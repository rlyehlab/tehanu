package be.rlab.tehanu.media

import be.rlab.tehanu.media.model.MediaFile
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.io.InputStream

class TestMediaManager {
    val instance: MediaManager = mock()

    fun findById(fileId: String, result: MediaFile): TestMediaManager = apply {
        whenever(instance.findById(fileId)).thenReturn(result)
    }

    fun readContent(mediaFile: MediaFile, result: InputStream): TestMediaManager = apply {
        whenever(instance.readContent(mediaFile)).thenReturn(result)
    }

    fun store(mediaFile: MediaFile, content: InputStream, result: MediaFile): TestMediaManager = apply {
        whenever(instance.store(mediaFile, content)).thenReturn(result)
    }

    fun update(mediaFile: MediaFile): TestMediaManager = apply {
        whenever(instance.update(mediaFile)).thenReturn(mediaFile)
    }
}
