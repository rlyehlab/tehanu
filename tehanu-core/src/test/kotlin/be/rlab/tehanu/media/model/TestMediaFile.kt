package be.rlab.tehanu.media.model

import java.util.*

data class TestMediaFile(
    val id: UUID = UUID.randomUUID(),
    val clientId: String = System.currentTimeMillis().toString(),
    val size: Long = 1234,
    val mimeType: String = "application/json",
    val metadata: Map<String, Any> = emptyMap()
) {
    fun new(): MediaFile = MediaFile(
        id = id,
        clientId = clientId,
        size = size,
        mimeType = mimeType,
        metadata = metadata
    )
}
