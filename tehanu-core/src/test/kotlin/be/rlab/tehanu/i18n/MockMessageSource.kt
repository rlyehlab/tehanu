package be.rlab.tehanu.i18n

import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class MockMessageSource {
    val instance: MessageSource = mock()

    fun get(name: String, args: List<Any>, result: String): MockMessageSource = apply {
        whenever(instance.get(name, *args.toTypedArray())).thenReturn(result)
    }

    fun find(name: String, args: List<Any>, result: String): MockMessageSource = apply {
        whenever(instance.find(name, *args.toTypedArray())).thenReturn(result)
    }

    fun expand(message: String, args: List<Any>, result: String): MockMessageSource = apply {
        whenever(instance.expand(message, *args.toTypedArray())).thenReturn(result)
    }
}
