package be.rlab.tehanu.clients.model

import be.rlab.tehanu.clients.Update
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.messages.model.TestMessage

data class MockUpdate(
    private val updateId: Long = System.currentTimeMillis(),
    private val chat: Chat = MockChat().new(),
    private val user: User? = MockUser().new(),
    private val message: Message? = TestMessage().instance,
    private val editedMessage: Message? = null,
    private val action: Action? = null
) {
    fun new(): Update = Update.new(
        updateId = updateId,
        chat = chat,
        user = user,
        message = message,
        action = action
    )
}