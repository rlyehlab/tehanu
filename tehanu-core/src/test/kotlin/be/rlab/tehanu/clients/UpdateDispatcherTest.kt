package be.rlab.tehanu.clients

import be.rlab.tehanu.clients.model.MockUpdate
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.mock.MockUserInput
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class UpdateDispatcherTest {
    @Test
    fun dispatch_initialState() {
        // arrange
        val update = MockUpdate().new()
        val state = MockState(handlerName = "/test")
            .findUserInput(update, null)
            .waitingForInput(false)
            .hasMoreInputs(false)
        val handler = MockUpdateHandler(name = "/test")
            .applies(update, true)
            .exec()

        val stateManager = MockStateManager()
            .addInitialState(update, handler.instance, state.instance)
            .processTransitions(state.instance)
        val dispatcher = UpdateDispatcher(stateManager.instance).addHandlers(listOf(handler.instance))

        // act
        val client = MockClient()
        val nextState = dispatcher.dispatch(client.instance, update)!!

        assertThat(nextState).isEqualTo(state.instance)

        // assert
        stateManager.verifyAll()
        stateManager.resolveNextState(state.instance, state.instance)

        val processTransitionsCallback: () -> State = stateManager.capturedValue("processTransitions", 3)
        assertThat(processTransitionsCallback()).isEqualTo(state.instance)
        stateManager.verifyAll()

        handler.verifyAll()
        state.verifyAll()
        client.verifyAll()
    }

    @Test
    fun dispatch_waitingForInput_success() {
        // arrange
        val update = MockUpdate().new()
        val input = MockUserInput()
            .resolveControl(update)
            .processInput()
        val state = MockState(handlerName = "/test")
            .findUserInput(update, input.instance as UserInput, times = 2)
            .waitingForInput()
            .hasMoreInputs(false)
        val handler = MockUpdateHandler(name = "/test")
            .applies(update, false)

        val stateManager = MockStateManager()
            .findStateIfApplies(update, state.instance)
            .processTransitions(state.instance)
        val dispatcher = UpdateDispatcher(stateManager.instance).addHandlers(listOf(handler.instance))

        // act
        val client = MockClient()
        val nextState = dispatcher.dispatch(client.instance, update)!!

        assertThat(nextState).isEqualTo(state.instance)

        // assert
        input.verifyAll()
        val capturedContext: UpdateContext = input.capturedValue("processInput")
        assertThat(capturedContext.state).isEqualTo(state.instance)
        assertThat(capturedContext.update).isEqualTo(update)

        stateManager.verifyAll()
        stateManager.resolveNextState(state.instance, state.instance)

        val processTransitionsCallback: () -> State = stateManager.capturedValue("processTransitions", 3)
        assertThat(processTransitionsCallback()).isEqualTo(state.instance)
        stateManager.verifyAll()

        handler.verifyAll()
        state.verifyAll()
        client.verifyAll()
    }
}
