package be.rlab.tehanu.clients

import be.rlab.tehanu.BotServiceProvider
import be.rlab.tehanu.MockBotServiceProvider
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.support.VerifySupport
import be.rlab.tehanu.view.ui.Control
import org.mockito.kotlin.*

class MockClient(
    private val serviceProvider: BotServiceProvider = MockBotServiceProvider().new()
) : VerifySupport<Client>() {
    override val instance: Client = mock {
        on { serviceProvider } doReturn serviceProvider
    }

    override fun verifyAllInternal() {
        verify(instance, atLeast(0)).serviceProvider
    }

    fun sendMessage(
        context: UpdateContext? = null,
        message: MessageBuilder? = null,
        result: List<Message> = emptyList()
    ): MockClient = apply {
        message?.let {
            whenever(instance.sendMessage(context!!, message)).thenReturn(result)
        }
        verify("sendMessage") {
            val capturedContext = argumentCaptor<UpdateContext>()
            val capturedMessage = argumentCaptor<MessageBuilder>()
            verify(instance).sendMessage(capturedContext.capture(), capturedMessage.capture())
            listOf(capturedContext.firstValue, capturedMessage.firstValue)
        }
    }

    fun renderControl(
        context: UpdateContext,
        control: Control,
        messageId: Long? = null
    ): MockClient = apply {
        verify("renderControl") {
            verify(instance).renderControl(context, control, messageId)
        }
    }
}
