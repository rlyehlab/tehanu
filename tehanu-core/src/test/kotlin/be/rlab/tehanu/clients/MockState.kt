package be.rlab.tehanu.clients

import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.MockChat
import be.rlab.tehanu.clients.model.MockUser
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.i18n.MockMessageSource
import be.rlab.tehanu.support.VerifySupport
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.mock.MockUserInput
import org.mockito.kotlin.*
import java.util.*

class MockState(
    private val id: UUID = UUID.randomUUID(),
    private val handlerName: String = "listener:handler",
    private val continueCallback: (() -> Unit) = {},
    private val previous: State? = null,
    private val chat: Chat = MockChat().new(),
    private val user: User = MockUser().new(),
    private val input: UserInput = MockUserInput().instance,
    private val translations: MessageSource = MockMessageSource().instance
) : VerifySupport<State>() {
    override val instance: State = mock {
        on { id } doReturn id
        on { chat } doReturn chat
        on { user } doReturn user
        on { handlerName } doReturn handlerName
        on { previous } doReturn previous
        on { continueCallback } doReturn continueCallback
        on { currentInput } doReturn input
        on { translations } doReturn translations
    }

    override fun verifyAllInternal() {
        verify(instance, atLeast(0)).id
        verify(instance, atLeast(0)).chat
        verify(instance, atLeast(0)).user
        verify(instance, atLeast(0)).handlerName
        verify(instance, atLeast(0)).previous
        verify(instance, atLeast(0)).continueCallback
        verify(instance, atLeast(0)).currentInput
        verify(instance, atLeast(0)).translations
    }

    fun waitingForInput(result: Boolean = true): MockState = apply {
        whenever(instance.waitingForInput()).thenReturn(result)
        verify("waitingForInput") {
            verify(instance).waitingForInput()
        }
    }

    fun applies(
        update: Update,
        result: Boolean
    ): MockState = apply {
        whenever(instance.applies(update)).thenReturn(result)
        verify("applies") {
            verify(instance).applies(update)
        }
    }

    fun hasMoreInputs(
        result: Boolean = true
    ): MockState = apply {
        whenever(instance.hasMoreInputs()).thenReturn(result)
        verify("hasMoreInputs") {
            verify(instance).hasMoreInputs()
        }
    }

    fun next(
        handlerName: String,
        continueCallback: () -> Unit,
        result: State
    ): MockState = apply {
        whenever(instance.next(handlerName, continueCallback)).thenReturn(result)
    }

    fun isActive(
        result: Boolean = true
    ): MockState = apply {
//        whenever(instance.isActive()).thenReturn(result)
    }

    fun updateIfRequired(
        chat: Chat?,
        user: User?
    ): MockState = apply {
        whenever(instance.updateIfRequired(chat, user)).thenReturn(instance)
    }

    fun findUserInput(
        update: Update,
        result: UserInput?,
        times: Int = 1
    ): MockState = apply {
        whenever(instance.findUserInput(update)).thenReturn(result)

        verify("findUserInput") {
            verify(instance, times(times)).findUserInput(update)
            emptyList<Any>()
        }
    }
}
