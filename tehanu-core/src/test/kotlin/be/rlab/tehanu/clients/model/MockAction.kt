package be.rlab.tehanu.clients.model

import java.util.*

data class MockAction(
    val id: String = UUID.randomUUID().toString(),
    val data: String = "action specific data"
) {
    fun new(): Action = Action(
        id = id,
        data = data
    )
}
