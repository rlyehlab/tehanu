package be.rlab.tehanu.clients.model

import kotlin.random.Random

data class MockUser(
    val id: Long = Random(9999).nextLong(),
    val userName: String? = "cthulhu",
    val firstName: String? = "H.P.",
    val lastName: String? = "Lovecraft"
) {
    fun new(): User = User(
        id = id,
        userName = userName,
        firstName = firstName,
        lastName = lastName
    )
}