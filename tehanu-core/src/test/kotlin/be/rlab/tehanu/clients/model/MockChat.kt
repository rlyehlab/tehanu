package be.rlab.tehanu.clients.model

import java.util.*
import kotlin.random.Random

data class MockChat(
    val id: UUID = UUID.randomUUID(),
    val clientName: String = "TelegramClient",
    val clientId: Long = Random(9999).nextLong(),
    val type: ChatType = ChatType.GROUP,
    val title: String? = "Test Chat Title",
    val description: String? = "Test Chat Description"
) {
    fun new(): Chat = Chat(
        id = id,
        clientName = clientName,
        clientId = clientId,
        type = type,
        title = title,
        description = description
    )
}
