package be.rlab.tehanu.clients

import be.rlab.tehanu.BotServiceProvider
import be.rlab.tehanu.clients.model.*
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.i18n.MockMessageSource
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.messages.model.TestMessage
import be.rlab.tehanu.support.VerifySupport
import org.mockito.kotlin.*

data class MockUpdateContext(
    val client: Client = MockClient().instance,
    val serviceProvider: BotServiceProvider = client.serviceProvider,
    val state: State = MockState().instance,
    val update: Update = MockUpdate().new(),
    val chat: Chat = MockChat().new(),
    val user: User = MockUser().new(),
    val translations: MessageSource = MockMessageSource().instance,
    val incomingMessage: Message = TestMessage().instance,
    val params: Map<String, Any> = emptyMap()
): VerifySupport<UpdateContext>() {
    override val instance: UpdateContext = mock {
        on { client } doReturn client
        on { state } doReturn state
        on { translations } doReturn translations
        on { chat } doReturn chat
        on { user } doReturn user
        on { update } doReturn update
        on { incomingMessage } doReturn incomingMessage
        on { serviceProvider } doReturn serviceProvider
        on { params } doReturn params
    }

    override fun verifyAllInternal() {
        verify(instance, atLeast(0)).client
        verify(instance, atLeast(0)).state
        verify(instance, atLeast(0)).translations
        verify(instance, atLeast(0)).chat
        verify(instance, atLeast(0)).user
        verify(instance, atLeast(0)).update
        verify(instance, atLeast(0)).incomingMessage
        verify(instance, atLeast(0)).serviceProvider
        verify(instance, atLeast(0)).params
    }

    fun talk(text: String): MockUpdateContext {
        verify("talk") {
            verify(instance).talk(text)
        }
        whenever(instance.talk(text)).thenReturn(instance)
        return this
    }

    fun answer(text: String): MockUpdateContext = apply {
        whenever(instance.answer(text)).thenReturn(instance)
        verify("answer") {
            verify(instance).answer(text)
        }
    }

    fun transitionTo(handlerName: String, params: Map<String, Any> = emptyMap()): MockUpdateContext = apply {
        whenever(instance.transitionTo(handlerName, params)).thenReturn(instance)
        verify("transitionTo") {
            verify(instance).transitionTo(handlerName, params)
        }
    }

    fun newMessage(messageBuilder: MessageBuilder): MockUpdateContext {
        verify("newMessage") {
            verify(instance).newMessage()
        }
        whenever(instance.newMessage()).thenReturn(messageBuilder)
        return this
    }

    fun sendMessage(
        messageBuilder: MessageBuilder,
        results: List<Message> = emptyList()
    ): MockUpdateContext = apply {
        verify("newMessage") {
            verify(instance).sendMessage(messageBuilder)
        }
        whenever(instance.sendMessage(messageBuilder)).thenReturn(results)
    }
}
