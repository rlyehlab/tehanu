package be.rlab.tehanu.clients

import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.i18n.MockMessageSource
import be.rlab.tehanu.support.VerifySupport
import org.mockito.kotlin.*

class MockUpdateHandler(
    private val name: String = "listener:handler",
    private val messageSource: MessageSource = MockMessageSource().instance,
    private val defaultLanguage: Language = Language.SPANISH
): VerifySupport<UpdateHandler>() {
    override val instance: UpdateHandler = mock {
        on { name } doReturn name
        on { translations } doReturn messageSource
        on { params } doReturn emptyList()
        on { defaultLanguage } doReturn defaultLanguage
    }

    override fun verifyAllInternal() {
        verify(instance, atLeast(0)).name
        verify(instance, atLeast(0)).translations
        verify(instance, atLeast(0)).params
        verify(instance, atLeast(0)).defaultLanguage
    }

    fun applies(
        update: Update,
        result: Boolean
    ): MockUpdateHandler {
        whenever(instance.applies(update)).thenReturn(result)
        verify("applies") {
            verify(instance).applies(update)
        }
        return this
    }

    fun exec(): MockUpdateHandler {
        verify("exec") {
            val capturedContext = argumentCaptor<UpdateContext>()
            verify(instance).exec(capturedContext.capture())
            capturedContext.firstValue
        }

        return this
    }
}