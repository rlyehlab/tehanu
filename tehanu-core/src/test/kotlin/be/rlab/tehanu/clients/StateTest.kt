package be.rlab.tehanu.clients

import be.rlab.tehanu.clients.model.Action
import be.rlab.tehanu.clients.model.MockAction
import be.rlab.tehanu.clients.model.MockUpdate
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.mock.MockUserInput
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class StateTest {

    private val handler: MockUpdateHandler = MockUpdateHandler()

    @Test
    fun initial() {
        val update = MockUpdate().new()
        val state = State.initial(
            update.chat,
            update.user,
            handler.instance.name,
            handler.instance.translations
        )

        assertThat(state.initial).isTrue()
        assertThat(state.handlerName).isEqualTo(handler.instance.name)
        assertThat(state.previous).isNull()
        assertThat(state.final).isTrue()
        assertThat(state.hasMoreInputs()).isFalse()
    }

    @Test
    fun addUserInput() {
        val update = MockUpdate().new()
        val state = State.initial(update.chat, update.user, handler.instance.name, handler.instance.translations)
        val input = MockUserInput()
        assertThat(state.hasMoreInputs()).isFalse()
        state.addUserInput(input.instance)
        state.addUserInput(input.instance)
        assertThat(state.hasMoreInputs()).isTrue()
        assertThat(state.nextUserInput()).isEqualTo(input.instance)
        assertThat(state.hasMoreInputs()).isFalse()

        input.verifyAll()
    }

    @Test
    fun findUserInput_action() {
        val action: Action = MockAction().new()
        val update = MockUpdate(action = action).new()
        val state = State.initial(update.chat, update.user, handler.instance.name, handler.instance.translations)
        val input = MockUserInput()
            .waitingForAction()
            .canHandle(update.action)

        state.addUserInput(input.instance as UserInput)
        assertThat(state.findUserInput(update)).isEqualTo(input.instance)

        input.verifyAll()
    }

    @Test
    fun findUserInput_message() {
        val action: Action = MockAction().new()
        val update = MockUpdate(action = action).new()
        val state = State.initial(update.chat, update.user, handler.instance.name, handler.instance.translations)
        val input = MockUserInput()
            .waitingForAction(false)
            .waitingForMessage(times = 2)
            .canHandle(update.action, false)

        state.addUserInput(input.instance as UserInput)
        assertThat(state.findUserInput(update)).isEqualTo(input.instance)

        input.verifyAll()
    }

    @Test
    fun updateIfRequired() {
        val update1 = MockUpdate().new()
        val update2 = MockUpdate().new()
        val state = State.initial(update1.chat, update1.user, handler.instance.name, handler.instance.translations)

        state.updateIfRequired(update2.chat, update2.user)

        assertThat(state.chat).isEqualTo(update2.chat)
        assertThat(state.user).isEqualTo(update2.user)
    }

    @Test
    fun next() {
        val update = MockUpdate().new()
        val nextHandler = MockUpdateHandler(name = "/nextHandler").instance
        var resolved = false
        val callback: () -> Unit = { resolved = true }
        val initial: State = State.initial(
            update.chat,
            update.user,
            handler.instance.name,
            handler.instance.translations
        )

        val nextState = initial.next("/nextHandler", callback)

        assert(!nextState.initial)
        assert(nextState.handlerName == nextHandler.name)
        assert(nextState.continueCallback == callback)
        assert(nextState.previous == initial)
        assert(!nextState.final)
        nextState.continueCallback()
        assert(resolved)
    }

    @Test
    fun applies_message() {
        val update = MockUpdate().new()
        val state = State.initial(update.chat, update.user, handler.instance.name, handler.instance.translations)
        val input = MockUserInput()
            .canHandle(null, false)
            .waitingForMessage()
        state.addUserInput(input.instance as UserInput)

        assertThat(state.applies(update)).isTrue()

        input.verifyAll()
    }

    @Test
    fun applies_action() {
        val action: Action = MockAction().new()
        val update = MockUpdate(action = action).new()
        val state = State.initial(update.chat, update.user, handler.instance.name, handler.instance.translations)
        val input = MockUserInput()
            .canHandle(action)
        state.addUserInput(input.instance as UserInput)

        assertThat(state.applies(update))

        input.verifyAll()
    }
}
