package be.rlab.tehanu.clients

import be.rlab.tehanu.clients.model.MockUpdate
import be.rlab.tehanu.view.Transition
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class StateManagerTest {
    @Test
    fun find() {
        val activeState: State = MockState().instance
        val manager = StateManager()
        manager.activeStates += activeState
        assert(manager.findStateById(activeState.id) == activeState)
    }

    @Test
    fun update() {
        val activeState: State = MockState().instance
        val nextState: State = MockState().instance
        val manager = StateManager()
        manager.activeStates += activeState

        val state: State = manager.updateState(activeState.id, nextState)
        assert(state == nextState)
        assert(manager.findStateById(nextState.id) == nextState)
        assert(manager.activeStates.size == 1)
    }

    @Test
    fun transitionTo() {
        // arrange
        var resolved = false
        val done = { resolved = true }
        val update = MockUpdate().new()
        val handler = MockUpdateHandler()
        val manager = StateManager().apply { addInitialState(update, handler.instance) }
        val currentState = manager.activeStates.first()
        val context = MockUpdateContext(state = currentState)

        // act
        var nextContext: UpdateContext = context.instance
        val nextState: State = manager.transitionTo(
            context.instance,
            Transition("/next", mapOf("hello" to "world")),
            { newContext ->
                nextContext = newContext
                manager.activeStates.first()
            },
            done
        )

        // assert
        assertThat(nextContext.params).isEqualTo(mapOf("hello" to "world"))
        nextState.continueCallback()
        assertThat(resolved).isTrue()

        context.verifyAll()
    }
}
