package be.rlab.tehanu.clients

import be.rlab.tehanu.support.VerifySupport
import be.rlab.tehanu.view.Transition
import org.mockito.kotlin.*

class MockUpdateDispatcher : VerifySupport<UpdateDispatcher>() {

    override val instance: UpdateDispatcher = mock()

    fun transitionTo(
        context: UpdateContext? = null,
        transition: Transition? = null,
        result: State
    ): MockUpdateDispatcher = apply {
        transition?.let {
            whenever(instance.transitionTo(eq(context!!), eq(transition), any())).thenReturn(result)
        } ?: let {
            whenever(instance.transitionTo(any(), any(), any())).thenReturn(result)
        }
        verify("transitionTo") {
            val capturedContext = argumentCaptor<UpdateContext>()
            val capturedTransition = argumentCaptor<Transition>()
            val capturedCallback = argumentCaptor<() -> Unit>()
            verify(instance).transitionTo(
                capturedContext.capture(),
                capturedTransition.capture(),
                capturedCallback.capture()
            )
            listOf(capturedContext.firstValue, capturedTransition.firstValue, capturedCallback.firstValue)
        }
    }
}
