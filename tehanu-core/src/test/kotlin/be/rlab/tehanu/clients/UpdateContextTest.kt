package be.rlab.tehanu.clients

import be.rlab.tehanu.clients.model.MockChat
import be.rlab.tehanu.clients.model.MockUpdate
import be.rlab.tehanu.clients.model.MockUser
import be.rlab.tehanu.i18n.MockMessageSource
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.TextResponse
import org.junit.Test

class UpdateContextTest {

    @Test
    fun talk() {
        val client = MockClient().sendMessage()
        val chat = MockChat().new()
        val update = MockUpdate(chat = chat).new()
        val context: UpdateContext = createUpdateContext(
            client = client.instance,
            state = MockState(
                translations = MockMessageSource().expand("hello world", emptyList(), "foobar").instance
            ).instance,
            update = update
        )

        // act
        context.talk("hello world")

        // assert
        client.verifyAll()
        val capturedMessageBuilder = client.capturedValue<MessageBuilder>("sendMessage", 1)
        assert(capturedMessageBuilder.chatId == update.chat!!.id)
        assert(capturedMessageBuilder.response is TextResponse)
        assert(capturedMessageBuilder.response.data == "hello world")
    }

    @Test
    fun talkTo_chat() {
        val client = MockClient().sendMessage()
        val chat = MockChat().new()
        val context: UpdateContext = createUpdateContext(
            client = client.instance,
            state = MockState(
                translations = MockMessageSource().expand("hello world", emptyList(), "foobar").instance
            ).instance
        )

        // act
        context.talkTo(chat.id, "hello world")

        // assert
        client.verifyAll()
        val capturedMessageBuilder = client.capturedValue<MessageBuilder>("sendMessage", 1)
        assert(capturedMessageBuilder.chatId == chat.id)
        assert(capturedMessageBuilder.response is TextResponse)
        assert(capturedMessageBuilder.response.data == "hello world")
    }

    @Test
    fun talkTo_user() {
        val client = MockClient().sendMessage()
        val user = MockUser().new()
        val context: UpdateContext = createUpdateContext(
            client = client.instance,
            state = MockState(
                translations = MockMessageSource().expand("hello world", emptyList(), "foobar").instance
            ).instance
        )

        // act
        context.talkTo(user, "hello world")

        // assert
        client.verifyAll()
        val capturedMessageBuilder = client.capturedValue<MessageBuilder>("sendMessage", 1)
        assert(capturedMessageBuilder.userId == user.id)
        assert(capturedMessageBuilder.response is TextResponse)
        assert(capturedMessageBuilder.response.data == "hello world")
    }

    @Test
    fun answer_public() {
        val client = MockClient().sendMessage()
        val context: UpdateContext = createUpdateContext(
            client = client.instance,
            state = MockState(
                translations = MockMessageSource().expand("hello world", emptyList(), "foobar").instance
            ).instance
        )

        // act
        context.answer("hello world")

        // assert
        client.verifyAll()
        val capturedMessageBuilder = client.capturedValue<MessageBuilder>("sendMessage", 1)
        assert(capturedMessageBuilder.replyTo == context.incomingMessage)
        assert(capturedMessageBuilder.response is TextResponse)
        assert(capturedMessageBuilder.response.data == "hello world")
    }

    private fun createUpdateContext(
        client: Client,
        state: State = MockState().instance,
        update: Update = MockUpdate().new()
    ): UpdateContext {
        return UpdateContext.new(
            client,
            state,
            update,
            emptyMap()
        )
    }
}
