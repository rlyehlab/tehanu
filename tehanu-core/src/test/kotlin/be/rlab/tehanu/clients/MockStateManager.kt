package be.rlab.tehanu.clients

import be.rlab.tehanu.support.VerifySupport
import be.rlab.tehanu.view.Transition
import org.mockito.kotlin.*
import java.util.*

class MockStateManager : VerifySupport<StateManager>() {

    override val instance: StateManager = mock()

    fun updateState(
        id: UUID = UUID.randomUUID(),
        nextState: State = MockState().instance
    ): MockStateManager {
        whenever(instance.updateState(eq(id), any())).thenReturn(nextState)

        verify("update") {
            val capturedState = argumentCaptor<State>()
            verify(instance).updateState(eq(id), capturedState.capture())
            capturedState.firstValue
        }
        return this
    }

    fun transitionTo(
        context: UpdateContext? = null,
        transition: Transition? = null,
        result: State
    ): MockStateManager = apply {
        transition?.let {
            whenever(instance.transitionTo(eq(context!!), eq(transition), any())).thenReturn(result)
        } ?: let {
            whenever(instance.transitionTo(any(), any(), any())).thenReturn(result)
        }
        verify("transitionTo") {
            val capturedContext = argumentCaptor<UpdateContext>()
            val capturedTransition = argumentCaptor<Transition>()
            val capturedExecCallback = argumentCaptor<(UpdateContext) -> State>()
            val capturedCallback = argumentCaptor<() -> Unit>()
            verify(instance).transitionTo(
                capturedContext.capture(),
                capturedTransition.capture(),
                capturedExecCallback.capture(),
                capturedCallback.capture()
            )
            listOf(
                capturedContext.firstValue,
                capturedTransition.firstValue,
                capturedExecCallback.firstValue,
                capturedCallback.firstValue
            )
        }
    }

    fun findStateIfApplies(
        update: Update,
        result: State
    ): MockStateManager = apply {
        whenever(instance.findStateIfApplies(update)).thenReturn(result)
        verify("findStateIfApplies") {
            verify(instance).findStateIfApplies(update)
            listOf(update)
        }
    }

    fun remove(id: UUID): MockStateManager = apply {
        verify("evict") {
            verify(instance).remove(id)
        }
    }

    fun addInitialState(
        update: Update,
        handler: UpdateHandler,
        result: State
    ): MockStateManager = apply {
        whenever(instance.addInitialState(update, handler)).thenReturn(result)
        verify("addInitialState") {
            verify(instance).addInitialState(update, handler)
        }
    }

    fun resolveNextState(
        state: State,
        result: State
    ): MockStateManager = apply {
        whenever(instance.resolveNextState(state)).thenReturn(result)
        verify("resolveNextState") {
            verify(instance).resolveNextState(state)
        }
    }

    fun processTransitions(
        result: State
    ): MockStateManager = apply {
        whenever(instance.processTransitions(any(), any(), any(), any())).thenReturn(result)
        verify("processTransitions") {
            val capturedContext = argumentCaptor<UpdateContext>()
            val capturedTransitions = argumentCaptor<Iterator<Transition>>()
            val capturedExecCallback = argumentCaptor<(UpdateContext) -> State>()
            val capturedCallback = argumentCaptor<() -> State>()
            verify(instance).processTransitions(
                capturedContext.capture(),
                capturedTransitions.capture(),
                capturedExecCallback.capture(),
                capturedCallback.capture()
            )
            listOf(
                capturedContext.firstValue,
                capturedTransitions.firstValue,
                capturedExecCallback.firstValue,
                capturedCallback.firstValue
            )
        }
    }
}
