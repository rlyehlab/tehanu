package be.rlab.tehanu.config

import be.rlab.tehanu.acl.model.Role

data class TestSecurityConfig(
    val admins: List<String> = emptyList(),
    val roles: List<Role> = emptyList()
) {
    fun new(): SecurityConfig =
        SecurityConfig(
            admins = admins,
            roles = roles
        )
}
