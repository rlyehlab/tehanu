package be.rlab.tehanu

import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.acl.TestAccessControl
import be.rlab.tehanu.clients.MockStateManager
import be.rlab.tehanu.clients.MockUpdateDispatcher
import be.rlab.tehanu.clients.StateManager
import be.rlab.tehanu.clients.UpdateDispatcher
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.i18n.MockMessageSource
import be.rlab.tehanu.media.MediaManager
import be.rlab.tehanu.media.TestMediaManager
import be.rlab.tehanu.messages.TestMemory
import be.rlab.tehanu.store.Memory
import kotlin.reflect.KClass

class MockBotServiceProvider(
    private val accessControl: AccessControl = TestAccessControl().instance,
    private val stateManager: StateManager = MockStateManager().instance,
    private val dispatcher: UpdateDispatcher = MockUpdateDispatcher().instance,
    private val memory: Memory = TestMemory(),
    private val mediaManager: MediaManager = TestMediaManager().instance,
    private val messageSource: MessageSource = MockMessageSource().instance,
) {
    private val configServiceMapping: Map<KClass<*>, () -> Any> = mapOf(
        AccessControl::class to { accessControl },
        StateManager::class to { stateManager },
        UpdateDispatcher::class to { dispatcher },
        MediaManager::class to { mediaManager },
        Memory::class to { memory },
        MessageSource::class to { messageSource },
    )

    fun new(): BotServiceProvider = object : BotServiceProvider() {
        @Suppress("UNCHECKED_CAST")
        override fun <T : Any> getService(type: KClass<T>): T {
            return configServiceMapping.getValue(type).invoke() as T
        }

        override fun <T : Any> getServices(type: KClass<T>): List<T> {
            return listOf(getService(type))
        }
    }
}
