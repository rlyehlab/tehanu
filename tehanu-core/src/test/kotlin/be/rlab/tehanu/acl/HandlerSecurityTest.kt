package be.rlab.tehanu.acl

import be.rlab.tehanu.AccessDeniedException
import be.rlab.tehanu.acl.model.AccessControlCondition
import be.rlab.tehanu.acl.model.Role
import be.rlab.tehanu.acl.model.UserRole
import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.ChatType
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.config.SecurityConfig
import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.i18n.MessageSourceFactory
import org.junit.Test

class HandlerSecurityTest {

    companion object {
        private const val CLIENT_NAME: String = "TelegramClient"
        private const val USER_ID: Long = 1234
        private const val CHAT_ID: Long = 5678
        private val ROLE_USER: Role = Role("USER", "Regular user",
            listOf("READ_CHAT", "READ_CALENDAR", "SEND_MESSAGE")
        )
        private val ROLE_ADMIN: Role = Role("ADMIN", "Administrator",
            listOf("READ_CHAT", "READ_CALENDAR", "WRITE_CHAT", "ADD_EVENT", "SEND_MESSAGE"),
            admin = true
        )
    }

    private val user: User = User.new(USER_ID, "test-user")
    private val chat: Chat = Chat.new(CLIENT_NAME, CHAT_ID, ChatType.PRIVATE, "title", "description")
    private val accessControl: TestAccessControl = TestAccessControl()
    private val securityConfig: SecurityConfig = SecurityConfig(emptyList(), listOf(ROLE_ADMIN, ROLE_USER))
    private val messageSource: MessageSource = MessageSourceFactory(Language.SPANISH).apply {
        initialize()
    }.resolve("default")

    @Test
    fun checkPermissions_accessGranted_all() {
        val security = HandlerSecurity(
            accessControl
                .findUserRole(chat, user, UserRole.new(chat.id, setOf("USER")))
                .instance,
            securityConfig,
            messageSource,
            listOf("READ_CHAT", "SEND_MESSAGE"),
            listOf(ROLE_USER),
            AccessControlCondition.ALL
        )
        security.checkPermissions(chat, user)
    }

    @Test
    fun checkPermissions_accessGranted_any() {
        val security = HandlerSecurity(
            accessControl
                .findUserRole(chat, user, UserRole.new(chat.id, setOf("USER")))
                .instance,
            securityConfig,
            messageSource,
            listOf("READ_CHAT", "SEND_MESSAGE", "ADD_EVENT"),
            listOf(ROLE_USER),
            AccessControlCondition.ANY
        )
        security.checkPermissions(chat, user)
    }

    @Test
    fun checkPermissions_accessGranted_default() {
        val security = HandlerSecurity(
            accessControl.instance,
            securityConfig,
            messageSource,
            listOf(),
            listOf(),
            AccessControlCondition.ALL
        )
        security.checkPermissions(chat, user)
    }

    @Test
    fun checkPermissions_accessGranted_admin() {
        val admin: User = User.new(1337, "admin-user")
        val security = HandlerSecurity(
            accessControl.instance,
            securityConfig.copy(admins = listOf("1337")),
            messageSource,
            listOf("READ_CHAT", "SEND_MESSAGE", "ADD_EVENT"),
            listOf(ROLE_USER),
            AccessControlCondition.ANY
        )
        security.checkPermissions(chat, admin)
    }

    @Test(expected = AccessDeniedException::class)
    fun checkPermissions_accessDenied_missingRole() {
        val security = HandlerSecurity(
            accessControl
                .findUserRole(chat, user, UserRole.new(chat.id, setOf("USER")))
                .instance,
            securityConfig,
            messageSource,
            listOf("READ_CHAT", "SEND_MESSAGE"),
            listOf(ROLE_USER, ROLE_ADMIN),
            AccessControlCondition.ALL
        )
        security.checkPermissions(chat, user)
    }

    @Test(expected = AccessDeniedException::class)
    fun checkPermissions_accessDenied_missingPermission() {
        val security = HandlerSecurity(
            accessControl
                .findUserRole(chat, user, UserRole.new(chat.id, setOf("USER")))
                .instance,
            securityConfig,
            messageSource,
            listOf("READ_CHAT", "SEND_MESSAGE", "ADD_EVENT"),
            listOf(ROLE_USER),
            AccessControlCondition.ALL
        )
        security.checkPermissions(chat, user)
    }
}
