package be.rlab.tehanu.acl

import be.rlab.tehanu.acl.model.Role
import be.rlab.tehanu.acl.model.UserRole
import be.rlab.tehanu.clients.model.*
import be.rlab.tehanu.config.TestSecurityConfig
import be.rlab.tehanu.messages.TestMemory
import be.rlab.tehanu.store.Memory
import org.junit.Test
import java.util.*

class AccessControlTest {

    companion object {
        private const val CLIENT_NAME: String = "TelegramClient"
        private const val USER_ID: Long = 1234
        private const val CHAT_ID: Long = 5678
    }

    private val memory: Memory = TestMemory()

    private var chats: List<Chat> by memory.slot("chats", listOf<Chat>())
    private var chatsMembers: Map<UUID, List<Long>> by memory.slot("chats_members", emptyMap<UUID, List<Long>>())
    private var users: List<User> by memory.slot("users", listOf<User>())
    private var userRoles: Map<Long, Set<UserRole>> by memory.slot("users_roles", emptyMap<Long, Set<UserRole>>())

    private val user: User = User.new(USER_ID, "test-user")
    private val chat: Chat = Chat.new(CLIENT_NAME, CHAT_ID, ChatType.PRIVATE, "title", "description")

    @Test
    fun assign_userToChat() {
        userRoles = userRoles + (user.id to setOf(
            UserRole.new(
            chatId = chat.id,
            rolesNames = setOf("OLD_ROLE")
        )))
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        accessControl.assign(
            chat = chat,
            user = user,
            rolesNames = setOf("TEST_ROLE")
        )
        assert(userRoles.containsKey(user.id))
        val roles: Set<UserRole> = userRoles.getValue(user.id)
        assert(roles.size == 1)
        assert(roles.first() == UserRole.new(chat.id, setOf("TEST_ROLE")))
    }

    @Test
    fun assign_userChatExists() {
        val userChat: Chat = Chat.new(
            clientName = CLIENT_NAME,
            clientId = user.id,
            type = ChatType.PRIVATE,
            title = "Chat with user",
            description = ""
        )
        chats = chats + userChat
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        accessControl.assign(
            clientName = CLIENT_NAME,
            user = user,
            rolesNames = setOf("TEST_ROLE")
        )
        assert(userRoles.containsKey(user.id))
        val roles: Set<UserRole> = userRoles.getValue(user.id)
        assert(roles.size == 1)
        assert(roles.first() == UserRole.new(userChat.id, setOf("TEST_ROLE")))
    }

    @Test
    fun assign_userChatMissing() {
        val userChat: Chat = MockChat(
            clientId = user.id,
            type = ChatType.PRIVATE,
            title = user.userName,
            description = "Chat with ${user.firstName} ${user.lastName}"
        ).new()
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        accessControl.assign(
            clientName = CLIENT_NAME,
            user = user,
            rolesNames = setOf("TEST_ROLE")
        )

        assert(userRoles.containsKey(user.id))

        val newUserChat: Chat = chats.first()
        assert(newUserChat == userChat.copy(id = newUserChat.id))

        val roles: Set<UserRole> = userRoles.getValue(user.id)
        assert(roles.size == 1)
        assert(roles.first() == UserRole.new(newUserChat.id, setOf("TEST_ROLE")))
        assert(chats.size == 1)
    }

    @Test
    fun drop_userFromChat() {
        val userRoleToRemove: UserRole = UserRole.new(
            chatId = chat.id,
            rolesNames = setOf("TEST_ROLE")
        )
        val userRoleToKeep: UserRole = UserRole.new(
            chatId = UUID.randomUUID(),
            rolesNames = setOf("TEST_ROLE")
        )
        userRoles = userRoles + (user.id to setOf(
            userRoleToRemove, userRoleToKeep
        ))
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        accessControl.drop(chat, user)

        assert(userRoles[user.id]?.size == 1)
        assert(userRoles[user.id]?.first() == userRoleToKeep)
    }

    @Test
    fun drop_userPermissions() {
        val userChat: Chat = Chat.new(
            clientName = CLIENT_NAME,
            clientId = user.id,
            type = ChatType.PRIVATE,
            title = "Chat with user",
            description = ""
        )
        chats = chats + userChat

        val userRoleToRemove: UserRole = UserRole.new(
            chatId = userChat.id,
            rolesNames = setOf("TEST_ROLE")
        )
        val userRoleToKeep: UserRole = UserRole.new(
            chatId = chat.id,
            rolesNames = setOf("TEST_ROLE")
        )
        userRoles = userRoles + (user.id to setOf(
            userRoleToRemove, userRoleToKeep
        ))
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        accessControl.drop(CLIENT_NAME, user)

        assert(userRoles[user.id]?.size == 1)
        assert(userRoles[user.id]?.first() == userRoleToKeep)
    }

    @Test
    fun drop_userChatDoesNotExist() {
        val userToDrop: User = MockUser().new()
        val userChat: Chat = Chat.new(
            clientName = CLIENT_NAME,
            clientId = userToDrop.id,
            type = ChatType.PRIVATE,
            title = userToDrop.userName,
            description = "Chat with ${userToDrop.firstName} ${userToDrop.lastName}"
        )

        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        accessControl.drop(CLIENT_NAME, userToDrop)

        val newChat: Chat = chats.first()
        assert(newChat == userChat.copy(id = newChat.id))
    }

    @Test
    fun addChatIfRequired_exists() {
        val newChat: Chat = MockChat(
            clientId = 1234
        ).new()
        chats = chats + newChat

        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        assert(accessControl.addChatIfRequired(newChat) == newChat)
    }

    @Test
    fun addChatIfRequired_create() {
        val newChat: Chat = MockChat(
            clientId = 1234,
            type = ChatType.GROUP,
            title = "test title",
            description = "test desc"
        ).new()

        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        val resolvedChat: Chat = accessControl.addChatIfRequired(newChat)
        assert(resolvedChat == newChat.copy(id = resolvedChat.id))
    }

    @Test
    fun addUserIfRequired_existingUserJoinsChat() {
        chats = chats + chat
        users = users + user

        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        val resolvedUser: User = accessControl.addUserIfRequired(chat, user)
        assert(resolvedUser == user)
        assert(chatsMembers[chat.id]?.contains(user.id)!!)
    }

    @Test
    fun addUserIfRequired_newUserJoinsChat() {
        chats = chats + chat

        val newUser = MockUser(
            id = 1234,
            userName = "cthulhu",
            firstName = "H.P.",
            lastName = "Lovecraft"
        ).new()

        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        val resolvedUser: User = accessControl.addUserIfRequired(chat, newUser)
        assert(resolvedUser == newUser)
        assert(users.contains(newUser))
        assert(chatsMembers[chat.id]?.contains(newUser.id)!!)
    }

    @Test
    fun listUsers_exists() {
        chatsMembers = chatsMembers + (chat.id to listOf(user.id))
        users = users + user
        chats = chats + chat

        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        val chatUsers: List<User> = accessControl.listUsers(chat.id)
        assert(chatUsers.size == 1)
        assert(chatUsers.containsAll(listOf(user)))
    }

    @Test
    fun findChatByclientId() {
        chats = chats + chat
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        assert(accessControl.findChat(chat.clientId) == chat)
    }

    @Test
    fun findChatById() {
        chats = chats + chat
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        assert(accessControl.findChatById(chat.id) == chat)
    }

    @Test
    fun listUsers_unknown() {
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        val chatUsers: List<User> = accessControl.listUsers(chat.id)
        assert(chatUsers.isEmpty())
    }

    @Test
    fun findUserById_exists() {
        users = users + user

        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        val chatUser: User? = accessControl.findUserById(user.id)
        assert(chatUser == user)
    }

    @Test
    fun findUserById_notExists() {
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        assert(accessControl.findUserById(user.id) == null)
    }

    @Test
    fun findUserByName_exists() {
        users = users + user

        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        val chatUser: User? = accessControl.findUserByName(user.userName!!)
        assert(chatUser == user)
    }

    @Test
    fun findUserByName_notExists() {
        val accessControl = AccessControl(TestSecurityConfig().new(), memory)
        assert(accessControl.findUserByName(user.userName!!) == null)
    }

    @Test
    fun getAdminsIds() {
        userRoles = userRoles + (user.id to setOf(
            UserRole.new(
            chatId = chat.id,
            rolesNames = setOf("TEST_ROLE")
        )))

        val applicationConfig = TestSecurityConfig(
            roles = listOf(
                Role(
                    "TEST_ROLE",
                    "Test",
                    listOf("MANAGE_ADMINS"),
                    admin = true
                )
            )
        ).new()

        val accessControl = AccessControl(applicationConfig, memory)
        val admins: List<Long> = accessControl.getAdminsIds(chat)
        assert(admins.size == 1)
        assert(admins.contains(user.id))
    }
}
