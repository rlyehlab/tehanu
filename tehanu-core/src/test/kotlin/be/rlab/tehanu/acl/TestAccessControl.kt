package be.rlab.tehanu.acl

import be.rlab.tehanu.acl.model.UserRole
import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.User
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.util.*

class TestAccessControl {
    val instance: AccessControl = mock()

    fun addChatIfRequired(
        chat: Chat
    ): TestAccessControl {
        whenever(instance.addChatIfRequired(chat)).thenReturn(chat)
        return this
    }

    fun addUserIfRequired(
        chat: Chat,
        user: User
    ): TestAccessControl {
        whenever(instance.addUserIfRequired(chat, user)).thenReturn(user)
        return this
    }

    fun findChatById(
        chatId: UUID,
        result: Chat
    ): TestAccessControl {
        whenever(instance.findChatById(chatId)).thenReturn(result)
        return this
    }

    fun listUsers(
        chatId: UUID,
        result: List<User>
    ): TestAccessControl {
        whenever(instance.listUsers(chatId)).thenReturn(result)
        return this
    }

    fun findUserById(
        userId: Long,
        result: User
    ): TestAccessControl {
        whenever(instance.findUserById(userId)).thenReturn(result)
        return this
    }

    fun findUserRole(
        chat: Chat,
        user: User,
        result: UserRole
    ): TestAccessControl {
        whenever(instance.findUserRole(chat, user)).thenReturn(result)
        return this
    }
}
