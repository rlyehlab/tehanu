package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.model.Action
import be.rlab.tehanu.messages.MockMessageBuilder
import be.rlab.tehanu.support.VerifySupport
import be.rlab.tehanu.view.Precondition
import be.rlab.tehanu.view.ValidationException
import org.mockito.kotlin.*
import java.util.*

open class MockControl(
    private val id: UUID = UUID.randomUUID(),
    private val description: MockMessageBuilder = MockMessageBuilder().text("test"),
    private val children: List<Control> = emptyList()
) : VerifySupport<Control>() {
    val preconditions: MutableList<Precondition> = mutableListOf()
    override val instance: Control = mock {
        on { id } doReturn id
        on { description } doReturn description.instance
        on { preconditions } doReturn preconditions
        on { children() } doReturn children
    }

    override fun verifyAllInternal() {
        verify(instance, atLeast(0)).id
        verify(instance, atLeast(0)).description
        verify(instance, atLeast(0)).preconditions
        verify(instance, atLeast(0)).children()
    }

    fun render(context: UpdateContext): MockControl = apply {
        whenever(instance.render(context)).thenReturn(instance)
        verify("render") {
            verify(instance).render(context)
        }
    }

    fun select(context: UpdateContext): MockControl = apply {
        whenever(instance.select(context)).thenReturn(instance)
        verify("select") {
            verify(instance).select(context)
        }
    }

    fun validate(
        context: UpdateContext,
        error: ValidationException? = null
    ): MockControl = apply {
        if (error == null) {
            whenever(instance.validate(context)).thenReturn(instance)
        } else {
            whenever(instance.validate(context)).thenThrow(error)
        }
        verify("validate") {
            verify(instance).validate(context)
        }
    }

    fun redrawIfRequired(): MockControl = apply {
        whenever(instance.redrawIfRequired(any())).thenReturn(instance)
        verify("redrawIfRequired") {
            val capturedCallback = argumentCaptor<Control.() -> Unit>()
            verify(instance).redrawIfRequired(capturedCallback.capture())
            capturedCallback.firstValue
        }
    }

    fun isCompleted(completed: Boolean = true): MockControl = apply {
        whenever(instance.isCompleted()).thenReturn(completed)
        verify("isCompleted") {
            verify(instance).isCompleted()
        }
    }

    fun isEnabled(enabled: Boolean = true): MockControl = apply {
        whenever(instance.isEnabled()).thenReturn(enabled)
        verify("isEnabled") {
            verify(instance).isEnabled()
        }
    }

    fun addPrecondition(precondition: Precondition): MockControl = apply {
        preconditions += precondition
    }

    fun disable(): MockControl = apply {
        whenever(instance.disable()).thenReturn(instance)
        verify("disable") {
            verify(instance).disable()
        }
    }

    fun waitingForMessage(waitingForMessage: Boolean = true, times: Int = 1): MockControl = apply {
        whenever(instance.waitingForMessage()).thenReturn(waitingForMessage)
        verify("waitingForMessage") {
            verify(instance, times(times)).waitingForMessage()
        }
    }

    fun waitingForAction(waitingForAction: Boolean = true): MockControl = apply {
        whenever(instance.waitingForAction()).thenReturn(waitingForAction)
        verify("waitingForAction") {
            verify(instance).waitingForAction()
        }
    }

    fun canHandle(action: Action?, result: Boolean = true): MockControl = apply {
        whenever(instance.canHandle(action)).thenReturn(result)
        verify("canHandle") {
            verify(instance).canHandle(action)
        }
    }
}
