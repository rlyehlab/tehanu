package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.MockUpdateContext
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.model.Action
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.util.UUID.randomUUID

class ControlTest {
    private val context: MockUpdateContext = MockUpdateContext()

    @Test
    fun render() {
        // arrange
        var rendered = 0
        val child = MockControl()
            .render(context.instance)
        val control = object : Control(id = randomUUID(), renderCallback = { rendered += 1 }) {
            override fun renderInternal(context: UpdateContext): Control = apply {
                rendered += 1
            }
        }.addChild(child.instance)

        // act
        control.render(context.instance)

        // assert
        assertThat(rendered).isEqualTo(2)
        child.verifyAll()
    }

    @Test
    fun validate() {
        // arrange
        var valid = 0
        val child = MockControl()
            .isEnabled()
            .isEnabled()
            .validate(context.instance)
        val control = object : Control(id = randomUUID()) {
            override fun validateInternal(context: UpdateContext): Control = apply {
                valid += 1
            }
        }.addChild(child.instance).validator { valid += 1 }

        // act
        control.validate(context.instance)

        // assert
        assertThat(valid).isEqualTo(2)
        child.verifyAll()
    }

    @Test
    fun select() {
        // arrange
        var value = 0
        val parent = MockControl()
            .select(context.instance)
        val control = object : Control(id = randomUUID(), parent = parent.instance) {
            override fun selectInternal(context: UpdateContext): Control = apply {
                value += 1
            }
        }.resolveValue { value += 1 }

        // act
        control.select(context.instance)

        // assert
        assertThat(value).isEqualTo(2)
        parent.verifyAll()
    }

    @Test
    fun find() {
        // arrange
        val child1 = Control.new()
        val child2 = Control.new().addChild(child1)
        val control = Control.new().addChild(child2)

        // act
        val found = control.find(child1.id)

        // assert
        assertThat(found).isEqualTo(child1)
    }

    @Test
    fun disable() {
        val control = Control.new()
        assertThat(control.isEnabled()).isTrue()
        control.disable()
        assertThat(control.isEnabled()).isFalse()
    }

    @Test
    fun setValue_any() {
        val control = Control.new()
        assertThat(control.isCompleted()).isFalse()
        assertThat(control.hasValue()).isFalse()
        control.setValue("foo")
        assertThat(control.isCompleted()).isTrue()
        assertThat(control.hasValue()).isTrue()
        assertThat(control.getValue<String>()).isEqualTo("foo")
    }

    @Test
    fun setValue_null() {
        val control = Control.new()
        assertThat(control.isCompleted()).isFalse()
        control.setValue(null)
        assertThat(control.isCompleted()).isTrue()
    }

    @Test
    fun redrawIfRequired_notRequired() {
        var required = false
        Control.new()
            .markForRedraw(false)
            .redrawIfRequired { required = true }
        assertThat(required).isFalse()
    }

    @Test
    fun redrawIfRequired_required() {
        var required = 0
        Control.new()
            .markForRedraw()
            .redrawIfRequired { required += 1 }
            .redrawIfRequired { required += 1 }
        assertThat(required).isEqualTo(1)
    }

    @Test
    fun findByType() {
        val keyboard = Keyboard.new()
        val control = Control.new().addChild(keyboard)
        assertThat(control.findByType(Keyboard::class)).contains(keyboard)
    }

    @Test
    fun canHandle_disabled() {
        val id = randomUUID()
        val action = Action(id = id.toString(), data = id.toString())
        val control = Control.new().disable()
        assertThat(control.canHandle(action)).isFalse()
    }

    @Test
    fun canHandle_isTarget() {
        val id = randomUUID()
        val action = Action(id = id.toString(), data = id.toString())
        val control = Control.new()
        val targetAction = action.copy(id = control.id.toString(), data = control.id.toString())
        assertThat(control.canHandle(targetAction)).isTrue()
    }

    @Test
    fun canHandle_child() {
        val id = randomUUID()
        val action = Action(id = id.toString(), data = id.toString())
        val child = Control.new()
        val control = Control.new().addChild(child)
        val targetAction = action.copy(id = child.id.toString(), data = child.id.toString())
        assertThat(control.canHandle(targetAction)).isTrue()
    }

    @Test
    fun waitingForMessage_disabled() {
        val child = Control.new()
        val control = Control.new().addChild(child).disable()
        assertThat(control.waitingForMessage()).isFalse()
    }

    @Test
    fun waitingForMessage_validInputMode() {
        val child = Control.new()
        val control = Control.new().addChild(child)
        assertThat(control.waitingForMessage()).isTrue()
    }

    @Test
    fun waitingForMessage_resolveValue() {
        val control = object : Control(id = randomUUID()) {
            override fun inputMode(): InputMode {
                return InputMode.Action
            }
        }.resolveValue { println("resolved!") }
        assertThat(control.waitingForMessage()).isTrue()
    }

    @Test
    fun waitingForMessage_isCompleted() {
        val child = Control.new()
        val control = Control.new().addChild(child).setValue("test value")
        assertThat(control.waitingForMessage()).isFalse()
    }

    @Test
    fun waitingForMessage_child() {
        val child = Control.new()
        val control = object : Control(id = randomUUID()) {
            override fun inputMode(): InputMode {
                return InputMode.Action
            }
        }.addChild(child)
        assertThat(control.waitingForMessage()).isTrue()
    }

    @Test
    fun waitingForAction_disabled() {
        val child = Control.new()
        val control = object : Control(id = randomUUID()) {
            override fun inputMode(): InputMode {
                return InputMode.Action
            }
        }.addChild(child).disable()
        assertThat(control.waitingForAction()).isFalse()
    }

    @Test
    fun waitingForAction_inputMode() {
        val child = Control.new()
        val control = object : Control(id = randomUUID()) {
            override fun inputMode(): InputMode {
                return InputMode.Action
            }
        }.addChild(child)
        assertThat(control.waitingForAction()).isTrue()
    }

    @Test
    fun waitingForAction_child() {
        val child = object : Control(id = randomUUID()) {
            override fun inputMode(): InputMode {
                return InputMode.Action
            }
        }
        val control = Control.new().addChild(child)
        assertThat(control.waitingForAction()).isTrue()
    }
}
