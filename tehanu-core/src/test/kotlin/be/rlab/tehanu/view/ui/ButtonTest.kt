package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.MockUpdateContext
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class ButtonTest {

    companion object {
        const val TEST_ICON_UNCHECKED: String = "○"
        const val TEST_ICON_CHECKED: String = "\uD83D\uDD18"
    }

    @Test
    fun selectDefault() {
        val button: Button = Button.new("test title", "some value")
        var redraw = false

        assertThat(button.isCompleted()).isTrue()
        button.select(MockUpdateContext().instance)
        button.redrawIfRequired { redraw = true }

        assertThat(button.title()).isEqualTo("test title")
        assertThat(button.getValue<String>()).isEqualTo("some value")
        assertThat(button.isChecked()).isFalse()
        assertThat(redraw).isTrue()
    }

    @Test
    fun selectOnClick() {
        val button: Button = Button.new("test title", "some value").apply {
            onClick {
                setValue("another value")
            }
        }
        assertThat(button.getValue<String>()).isEqualTo("some value")

        button.select(MockUpdateContext().instance)
        assertThat(button.getValue<String>()).isEqualTo("another value")
        assertThat(button.isChecked()).isFalse()
    }

    @Test
    fun states_radio() {
        val radio: Button = Button.radio("test title", "some value")
        assertThat(radio.isChecked()).isFalse()
        assertThat(radio.title()).isEqualTo("$TEST_ICON_UNCHECKED test title")
        radio.toggle()
        assertThat(radio.isChecked()).isTrue()
        assertThat(radio.title()).isEqualTo("$TEST_ICON_CHECKED test title")
        radio.uncheck()
        assertThat(radio.isChecked()).isFalse()
    }

    @Test
    fun states_checkbox() {
        val checkbox: Button = Button.checkbox("test title", "some value")
        assertThat(checkbox.isChecked()).isFalse()
        assertThat(checkbox.title()).isEqualTo("□ test title")
        checkbox.check()
        assertThat(checkbox.isChecked()).isTrue()
        assertThat(checkbox.title()).isEqualTo("✅ test title")
        checkbox.uncheck()
        assertThat(checkbox.isChecked()).isFalse()
    }

    @Test
    fun changeTitle() {
        val button: Button = Button.checkbox("test title", "some value")
        assertThat(button.title()).isEqualTo("□ test title")
        button.changeTitle("new title")
        assertThat(button.title()).isEqualTo("□ new title")
    }

    @Test
    fun updateIcon() {
        val button: Button = Button.new("test title", "some value")
        assertThat(button.title()).isEqualTo("test title")
        button.updateIcons("⚑", "⚐")
        assertThat(button.title()).isEqualTo("⚐ test title")
        button.toggle()
        assertThat(button.title()).isEqualTo("⚑ test title")
    }
}
