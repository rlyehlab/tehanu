package be.rlab.tehanu.view

import be.rlab.tehanu.MockBotServiceProvider
import be.rlab.tehanu.clients.MockClient
import be.rlab.tehanu.clients.MockUpdateContext
import be.rlab.tehanu.clients.MockUpdateDispatcher
import be.rlab.tehanu.clients.model.Action
import be.rlab.tehanu.clients.model.MockUpdate
import be.rlab.tehanu.messages.MockMessageBuilder
import be.rlab.tehanu.view.mock.MockUserInput
import be.rlab.tehanu.view.ui.Control
import be.rlab.tehanu.view.ui.MockControl
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

class UserInputTest {
    private val dispatcher: MockUpdateDispatcher = MockUpdateDispatcher()
    private val client: MockClient = MockClient(
        serviceProvider = MockBotServiceProvider(dispatcher = dispatcher.instance).new()
    )
    private val context: MockUpdateContext = MockUpdateContext(
        client = client.instance
    )

    @Test
    fun render_control() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")
        val control = MockControl()
            .render(context.instance)
        context.sendMessage(description.instance)
        client.renderControl(context.instance, control.instance)

        // act
        UserInput.new(description.instance)
            .addChild(control.instance)
            .render(context.instance)

        // assert
        context.verifyAll()
        control.verifyAll()
        client.verifyAll()
    }

    @Test
    fun render_controlSet() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")
        val control = MockControl()
            .render(context.instance)
        val controlSet = MockUserInput()
            .nextControl(control.instance)
            .hasMoreControls(times = 2)
            .render(context.instance)
        context.sendMessage(description.instance)
        client
            .renderControl(context.instance, control.instance)

        // act
        UserInput.new(description.instance)
            .addChild(controlSet.instance)
            .render(context.instance)

        // assert
        context.verifyAll()
        control.verifyAll()
        controlSet.verifyAll()
        client.verifyAll()
    }

    @Test
    fun render_preconditionFailed_notifyUser() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")
        val control = MockControl()
            .addPrecondition(Precondition.new { reject("assertion failed") })
        context
            .sendMessage(description.instance)
            .answer("assertion failed")

        // act
        UserInput.new(description.instance)
            .addChild(control.instance)
            .render(context.instance)

        // assert
        context.verifyAll()
        control.verifyAll()
        client.verifyAll()
    }

    @Test
    fun render_preconditionFailed_skipAction() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")
        val control1 = MockControl()
            .disable()
            .addPrecondition(Precondition.new { reject("assertion failed", RejectAction.SKIP_CONTEXT_ACTION) })
        val control2 = MockControl()
            .render(context.instance)
        context.sendMessage(description.instance)
        client.renderControl(context.instance, control2.instance)

        // act
        UserInput.new(description.instance)
            .addChild(control1.instance)
            .addChild(control2.instance)
            .render(context.instance)

        // assert
        context.verifyAll()
        control1.verifyAll()
        control2.verifyAll()
        client.verifyAll()
    }

    @Test
    fun render_preconditionFailed_throwUnhandledError() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")
        val control = MockControl()
            .addPrecondition(Precondition.new { reject("assertion failed", RejectAction.THROW_UNHANDLED_ERROR) })
        context.sendMessage(description.instance)

        // act
        try {
            UserInput.new(description.instance)
                .addChild(control.instance)
                .render(context.instance)
            throw RuntimeException("should not get here")
        } catch(cause: PreconditionEvaluationException) {
            assertThat(cause.message).isEqualTo("assertion failed")
            assertThat(cause.action).isEqualTo(RejectAction.THROW_UNHANDLED_ERROR)
        }

        // assert
        context.verifyAll()
        control.verifyAll()
        client.verifyAll()
    }

    @Test
    fun processInput_completed() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")
        val id = UUID.randomUUID()
        val context = MockUpdateContext(
            client = client.instance,
            update = MockUpdate(action = Action("action1", id.toString())).new()
        ).sendMessage(description.instance)
        val control1 = MockControl(id = id)
            .render(context.instance)
            .validate(context.instance)
            .select(context.instance)
            .redrawIfRequired()
            .isCompleted()
        val control2 = MockControl()
            .render(context.instance)
        client
            .renderControl(context.instance, control1.instance)
            .renderControl(context.instance, control1.instance, context.incomingMessage.messageId)
            .renderControl(context.instance, control2.instance)

        // act
        val userInput: UserInput = UserInput.new(description.instance)
            .addChild(control1.instance)
            .addChild(control2.instance)
            .render(context.instance) as UserInput
        userInput.processInput(context.instance)

        // assert
        context.verifyAll()
        control1.verifyAll()
        control2.verifyAll()
        val redraw: Control.() -> Unit = control1.capturedValue("redrawIfRequired")
        redraw(control1.instance)
        client.verifyAll()
    }

    @Test
    fun processInput_notCompleted() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")
        val id = UUID.randomUUID()
        val context = MockUpdateContext(
            client = client.instance,
            update = MockUpdate(action = Action("action1", id.toString())).new()
        ).sendMessage(description.instance)
        val control1 = MockControl(id = id)
            .validate(context.instance)
            .render(context.instance)
            .select(context.instance)
            .redrawIfRequired()
            .isCompleted(completed = false)
        val control2 = MockControl()
        client
            .renderControl(context.instance, control1.instance)
            .renderControl(context.instance, control1.instance, context.incomingMessage.messageId)

        // act
        val userInput: UserInput = UserInput.new(description.instance)
            .addChild(control1.instance)
            .addChild(control2.instance)
            .render(context.instance) as UserInput
        userInput.processInput(context.instance)

        // assert
        context.verifyAll()
        control1.verifyAll()
        control2.verifyAll()

        val redraw: Control.() -> Unit = control1.capturedValue("redrawIfRequired")
        redraw(control1.instance)
        client.verifyAll()
    }

    @Test
    fun processInput_validationError_message() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")
        val id = UUID.randomUUID()
        val context = MockUpdateContext(client = client.instance)
            .sendMessage(description.instance)
            .answer("validation error")
        val control1 = MockControl(id = id)
            .render(context.instance)
            .validate(context.instance, ValidationException(ValidationErrorResult.message("validation error")))
        client.renderControl(context.instance, control1.instance)

        // act
        val userInput: UserInput = UserInput.new(description.instance)
            .addChild(control1.instance)
            .render(context.instance) as UserInput
        userInput.processInput(context.instance)

        // assert
        context.verifyAll()
        control1.verifyAll()
        client.verifyAll()
    }

    @Test
    fun processInput_validationError_transition() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")
        val id = UUID.randomUUID()
        val context = MockUpdateContext(client = client.instance)
            .sendMessage(description.instance)
            .transitionTo("/next")
        val control1 = MockControl(id = id)
            .render(context.instance)
            .validate(
                context.instance,
                ValidationException(ValidationErrorResult.transition(Transition("/next", emptyMap())))
            )
        client.renderControl(context.instance, control1.instance)

        // act
        val userInput: UserInput = UserInput.new(description.instance)
            .addChild(control1.instance)
            .render(context.instance) as UserInput
        userInput.processInput(context.instance)

        // assert
        context.verifyAll()
        control1.verifyAll()
        client.verifyAll()
    }

    @Test
    fun submit() {
        // arrange
        val description: MockMessageBuilder = MockMessageBuilder()
            .text("hello world")

        // act
        val userInput: UserInput = UserInput.new(description.instance)
        var submitted = 0
        userInput
            .onSubmit { submitted++ }
            .select(context.instance)
            .select(context.instance)

        // assert
        assertThat(submitted).isEqualTo(1)
        assertThat(userInput.isCompleted()).isTrue()
        context.verifyAll()
        client.verifyAll()
    }

//    @Test
//    fun render_failed() {
//        // arrange
//        val context = MockUpdateContext()
//        val field = MockControl()
//            .render(context.instance)
//            .waitingForMessage(waitingForMessage = false)
//
//        // act
//        val userInput = UserInput.new()
////        userInput.attachMessage(context.instance.incomingMessage.messageId, field.instance)
//
//        // assert
//        context.verifyAll()
//        field.verifyAll()
//    }
//
//    @Test
//    fun addChild() {
//        // arrange
//        val field = MockControl()
//
//        // act
//        val userInput = UserInput.new()
//            .addChild(field.instance) as UserInput
//
//        // assert
//        assertThat(userInput.hasMoreControls()).isTrue()
//        assertThat(userInput.nextControl()).isEqualTo(field.instance)
//        assertThat(userInput.hasMoreControls()).isFalse()
//    }
//
//    @Test
//    fun isActive() {
//        // arrange
//        val field1 = MockControl().waitingForMessage(true)
//        val field2 = MockControl().waitingForMessage(false)
//
//        // act
//        val userInput1 = UserInput.new().addChild(field1.instance) as UserInput
//        val userInput2 = UserInput.new().addChild(field2.instance) as UserInput
//
//        // assert
////        assertThat(userInput1.waitingMessage()).isTrue()
////        assertThat(userInput2.waitingMessage()).isFalse()
//    }
//
//    @Test
//    fun keepAlive() {
//        // arrange
//        val field1 = MockControl().keepAlive(true)
//        val field2 = MockControl().keepAlive(false)
//
//        // act
//        val userInput1 = UserInput.new().addChild(field1.instance) as UserInput
//        val userInput2 = UserInput.new().addChild(field2.instance) as UserInput
//
//        // assert
////        assertThat(userInput1.keepAlive()).isTrue()
////        assertThat(userInput2.keepAlive()).isFalse()
//    }
//
//    @Test
//    fun resolveField_fromCache() {
//        // arrange
//        val client = MockClient()
//        val message = TestMessage().instance
//        val context = MockUpdateContext(client = client.instance, incomingMessage = message)
//        val field = MockControl()
//            .render(context.instance)
//            .waitingForMessage()
//        client.renderField(context.instance, field.instance)
//
//        // act
//        val userInput = UserInput.new()
////        userInput.attachMessage(context.instance.incomingMessage.messageId, field.instance)
//        val resolvedField = userInput.resolveControl(context.instance.update)
//
//        // assert
//        assertThat(resolvedField).isEqualTo(field.instance)
//    }
//
//    @Test
//    fun resolveField_lastField() {
//        // arrange
//        val context = MockUpdateContext()
//        val field = MockControl()
//            .render(context.instance)
//            .waitingForMessage()
//
//        // act
//        val userInput = UserInput.new()
//            .addChild(field.instance) as UserInput
//        val otherField = userInput.field("otherField")
//
//        assertThat(userInput.nextControl()).isEqualTo(field.instance)
//        assertThat(userInput.nextControl()).isEqualTo(otherField)
//        val resolvedField = userInput.resolveControl(context.instance.update)
//
//        // assert
//        assertThat(resolvedField).isEqualTo(otherField)
//        context.verifyAll()
//    }
//
//    @Test
//    fun submitIfRequired() {
//        // arrange
//        val message = TestMessage().instance
//        val context = MockUpdateContext(incomingMessage = message)
//        val field = MockControl()
//            .render(context.instance)
//            .waitingForMessage()
//
//        // act
//        var submitted = 0
//        val userInput = UserInput.new()
//            .addChild(field.instance).apply {
//                require(this is UserInput)
//                onSubmit {
//                    submitted += 1
//                }
//            } as UserInput
//
//        // assert
//        userInput.submit(context.instance)
//        assertThat(submitted).isEqualTo(0)
//        userInput.nextControl()
//        userInput.submit(context.instance)
//        assertThat(submitted).isEqualTo(1)
//        userInput.submit(context.instance)
//        assertThat(submitted).isEqualTo(1)
//    }
//
//    @Test
//    fun transitionTo() {
//        // act
//        val userInput = UserInput.new()
//
//        // assert
//        assertThat(userInput.transitionTo("/test")).isEqualTo(Transition("/test", emptyMap()))
//    }
//
//    @Test
//    fun addPrecondition() {
//        // act
//        val userInput = UserInput.new()
//
//        userInput.precondition {
////            evaluate { PreconditionEvaluationResult.FULFILLED }
////            resolve { "hello" }
//        }
//
//        // assert
//        assertThat(userInput.preconditions.size).isEqualTo(1)
////        assertThat(userInput.preconditions[0].evaluate()).isTrue()
////        assertThat(userInput.preconditions[0].process()).isEqualTo("hello")
//    }
}
