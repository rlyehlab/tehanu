package be.rlab.tehanu.view.mock

import be.rlab.tehanu.clients.Update
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.model.Action
import be.rlab.tehanu.messages.MockMessageBuilder
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.ui.Control
import be.rlab.tehanu.view.ui.Field
import be.rlab.tehanu.view.ui.MockControl
import org.mockito.kotlin.*
import java.util.*

class MockUserInput(
    id: UUID = UUID.randomUUID(),
    description: MockMessageBuilder = MockMessageBuilder().text("test")
) : MockControl(id, description) {
    override val instance: UserInput = mock()

    fun addChild(control: Control): MockUserInput = apply {
        whenever(instance.addChild(control)).thenReturn(instance)
        verify("addChild") {
            verify(instance).addChild(control)
        }
    }

    fun hasMoreControls(hasMoreControls: Boolean = true, times: Int = 1): MockUserInput = apply {
        whenever(instance.hasMoreControls()).thenReturn(hasMoreControls)
        verify("hasMoreControls") {
            verify(instance, times(times)).hasMoreControls()
        }
    }

    fun nextControl(next: Control): MockUserInput = apply {
        whenever(instance.nextControl()).thenReturn(next)
        verify("nextControl") {
            verify(instance).nextControl()
        }
    }

    fun isActive(result: Boolean): MockUserInput = apply {
        whenever(instance.waitingForMessage()).thenReturn(result)
    }

    fun applies(
        action: Action,
        result: Boolean
    ): MockUserInput = apply {
//        whenever(instance.canHandle(action)).thenReturn(result)
    }

    fun hasMoreFields(
        result: Boolean
    ): MockUserInput = apply {
        whenever(instance.hasMoreControls()).thenReturn(result)
    }

    fun nextField(
        result: Field
    ): MockUserInput = apply {
        whenever(instance.nextControl()).thenReturn(result)
    }

    fun submitIfRequired(
        context: UpdateContext? = null
    ): MockUserInput = apply {
        verify("submitIfRequired") {
            context?.let {
                verify(instance).submit(context)
            } ?: let {
                val capturedContext = argumentCaptor<UpdateContext>()
                verify(instance).submit(capturedContext.capture())
                capturedContext.firstValue
            }
        }
    }

    fun resolveControl(
        update: Update,
        result: Control? = null
    ): MockUserInput = apply {
        whenever(instance.resolveControl(update)).thenReturn(result)
        verify("resolveControl") {
            verify(instance).resolveControl(update)
        }
    }

    fun processInput(context: UpdateContext? = null): MockControl = apply {
        if (context != null) {
            whenever(instance.processInput(context)).thenReturn(instance)
        } else {
            whenever(instance.processInput(any())).thenReturn(instance)
        }

        verify("processInput") {
            val capturedContext = argumentCaptor<UpdateContext>()
            verify(instance).processInput(capturedContext.capture())
            capturedContext.firstValue
        }
    }
}
