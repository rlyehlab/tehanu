package be.rlab.tehanu.view

import be.rlab.tehanu.MockBotServiceProvider
import be.rlab.tehanu.clients.MockClient
import be.rlab.tehanu.clients.MockUpdateContext
import be.rlab.tehanu.clients.MockUpdateDispatcher
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.kotlin.*

class PreconditionTest {
    private val dispatcher: MockUpdateDispatcher = MockUpdateDispatcher()
    private val context: MockUpdateContext = MockUpdateContext(
        client = MockClient(
            serviceProvider = MockBotServiceProvider(dispatcher = dispatcher.instance).new()
        ).instance
    )

    @Test
    fun processAll_alreadyResolved() {
        // arrange
        val precondition: Precondition = mock()
        whenever(precondition.isSatisfied()).thenReturn(true)
        val preconditions = listOf(precondition)

        // act
        var resolved = false
        Precondition.processAll(context.instance, preconditions.iterator()) {
            resolved = true
        }

        // assert
        assertThat(resolved).isTrue()
        verify(precondition).isSatisfied()
        verifyNoMoreInteractions(precondition)
    }

    @Test
    fun processAll_resolve() {
        // arrange
        val precondition = Precondition.new { resolve() }
        val preconditions = listOf(precondition)

        // act
        var resolved = false
        Precondition.processAll(context.instance, preconditions.iterator()) {
            resolved = true
        }

        // assert
        assertThat(resolved).isTrue()
        assertThat(precondition.isSatisfied()).isTrue()
    }

    @Test
    fun processAll_reject() {
        // arrange
        val precondition = Precondition.new { reject("assertion failed", RejectAction.SKIP_CONTEXT_ACTION) }
        val preconditions = listOf(precondition)

        // act
        var resolved = false
        try {
            Precondition.processAll(context.instance, preconditions.iterator()) {
                resolved = true
            }
            throw RuntimeException("process should fail")
        } catch (cause: PreconditionEvaluationException) {
            assertThat(cause.message).isEqualTo("assertion failed")
            assertThat(cause.action).isEqualTo(RejectAction.SKIP_CONTEXT_ACTION)
        }

        // assert
        assertThat(resolved).isFalse()
        assertThat(precondition.isSatisfied()).isFalse()
    }

    @Test
    fun processAll_transition() {
        // arrange
        val transition = Transition("/next", mapOf("foo" to "bar"))
        val precondition = Precondition.new { transitionTo(transition.handlerName, transition.params) }
        val preconditions = listOf(precondition)

        // act
        var resolved = false
        Precondition.processAll(context.instance, preconditions.iterator()) {
            resolved = true
        }

        // assert
        assertThat(resolved).isFalse()
        assertThat(precondition.isSatisfied()).isFalse()
        verify(dispatcher.instance).transitionTo(eq(context.instance), eq(transition), any())
        verifyNoMoreInteractions(dispatcher.instance)
    }
}
