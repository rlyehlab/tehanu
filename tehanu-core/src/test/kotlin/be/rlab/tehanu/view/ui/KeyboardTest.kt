package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.MockUpdateContext
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class KeyboardTest {

    @Test
    fun render() {
        var buttonRendered = false
        val keyboard = Keyboard.new {
            button("Foo", "Bar") {
                buttonRendered = true
            }
        }
        val context = MockUpdateContext()

        assertThat(keyboard.buttons()).isEmpty()
        keyboard.render(context.instance)
        assertThat(keyboard.buttons()).isNotEmpty
        assertThat(buttonRendered).isTrue()
        context.verifyAll()
    }

    @Test
    fun render_missingSubmit() {
        val keyboard = Keyboard.new(mode = KeyboardMode.MULTIPLE_SELECTION) {
            button("Foo", "Bar")
        }
        val context = MockUpdateContext()

        try {
            assertThat(keyboard.buttons()).isEmpty()
            keyboard.render(context.instance)
            throw RuntimeException("should fail in the previous line")
        } catch (cause: IllegalArgumentException) {
            assertThat(cause.message).isEqualTo("submitButton is required in MULTIPLE_SELECTION mode")
        }
        context.verifyAll()
    }

    @Test
    fun singleSelection() {
        val keyboard = Keyboard.new(mode = KeyboardMode.SINGLE_SELECTION).apply {
            button("Hello!", "foo")
            button("World!", "bar")
        }
        val context = MockUpdateContext()
        keyboard.render(context.instance)

        val button = keyboard.buttons().last()
        assertThat(keyboard.waitingForAction()).isTrue()
        assertThat(keyboard.isCompleted()).isFalse()
        button.select(context.instance)
        assertThat(keyboard.isCompleted()).isTrue()
        assert(keyboard.getValue<String>() == "bar")
    }

    @Test
    fun singleSelection_submit() {
        val keyboard = Keyboard.new(mode = KeyboardMode.SINGLE_SELECTION).apply {
            button("Hello!", "foo")
            button("World!", "bar")
            submitButton("submit")
        }
        val context = MockUpdateContext()
        keyboard.render(context.instance)

        val button = keyboard.buttons()[1]
        val submit = keyboard.buttons().last()
        assertThat(keyboard.waitingForAction()).isTrue()
        assertThat(keyboard.isCompleted()).isFalse()
        button.select(context.instance)
        assertThat(keyboard.isCompleted()).isFalse()
        submit.select(context.instance)
        assertThat(keyboard.isCompleted()).isTrue()
        assert(keyboard.getValue<String>() == "bar")
    }

    @Test
    fun multipleSelection() {
        val keyboard = Keyboard.new(mode = KeyboardMode.MULTIPLE_SELECTION).apply {
            button("Hello!", "foo")
            button("World!", "bar")
            submitButton("submit")
        }
        val context = MockUpdateContext()
        keyboard.render(context.instance)

        keyboard.buttons()[0].select(context.instance)
        keyboard.buttons()[1].select(context.instance)

        val submit = keyboard.buttons().last()
        assertThat(keyboard.waitingForAction()).isTrue()
        assertThat(keyboard.isCompleted()).isFalse()
        submit.select(context.instance)
        assertThat(keyboard.isCompleted()).isTrue()
        assertThat(keyboard.getValue<List<String>>()).containsExactly("foo", "bar")
    }
}
