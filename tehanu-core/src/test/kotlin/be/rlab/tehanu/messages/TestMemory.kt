package be.rlab.tehanu.messages

import be.rlab.tehanu.store.Memory

class TestMemory : Memory() {
    private val memory: MutableMap<String, String> = mutableMapOf()

    override fun retrieve(slotName: String): String? {
        return memory[slotName]
    }

    override fun store(slotName: String, jsonData: String) {
        memory[slotName] = jsonData
    }

    override fun clear() {
        memory.clear()
    }
}
