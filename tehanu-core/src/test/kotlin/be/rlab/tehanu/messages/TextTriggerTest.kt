package be.rlab.tehanu.messages

import be.rlab.tehanu.clients.Update
import be.rlab.tehanu.clients.model.MockChat
import be.rlab.tehanu.clients.model.MockUser
import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.messages.model.TestMessage
import org.junit.Test

class TextTriggerTest {

    private val language: Language = Language.SPANISH

    @Test
    fun startsWith() {
        val trigger = TextTrigger.new("default", startsWith = "hello", ignoreCase = false)
        assert(trigger.applies(message("hello world"), language))
        assert(!trigger.applies(message("Hello world"), language))
        assert(!trigger.applies(message("hey, hello world!"), language))
    }

    @Test
    fun endsWith() {
        val trigger = TextTrigger.new("default", endsWith = "world", ignoreCase = true)
        assert(trigger.applies(message("hello World"), language))
        assert(!trigger.applies(message("hey, hello world!"), language))
    }

    @Test
    fun contains() {
        val trigger = TextTrigger.new("default", contains = listOf("hello", "world"), ignoreCase = true)
        assert(trigger.applies(message("hi there, hello World!"), language))
        assert(trigger.applies(message("hey, hello there!"), language))
        assert(!trigger.applies(message("hi there!"), language))
    }

    @Test
    fun contains_distance() {
        val trigger = TextTrigger.new(
            "default",
            contains = listOf("hello", "world"),
            ignoreCase = true,
            distance = 0.75F
        )
        assert(trigger.applies(message("hi there, hello World!"), language))
        assert(trigger.applies(message("hey, hello there!"), language))
        assert(!trigger.applies(message("hi there!"), language))
    }

    @Test
    fun matches() {
        val trigger = TextTrigger.new("default", regex = Regex(".*hell.\\sworld$"), ignoreCase = true)
        assert(trigger.applies(message("hi there, hello world"), language))
        assert(!trigger.applies(message("hi, hello there!"), language))
    }

    private fun message(text: String): Update {
        return Update.new(
            System.currentTimeMillis(), MockChat().new(), MockUser().new(), TestMessage(text).hasText(true).instance
        )
    }
}