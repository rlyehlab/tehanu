package be.rlab.tehanu.messages.model

import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

data class TestMessageEntity(
    private val type: EntityType = EntityType.MENTION,
    private val value: String = "",
    private val data: Any = ""
) {
    fun new(): MessageEntity = mock {
        on { type } doReturn type
        on { value } doReturn value
        on { data } doReturn data
    }
}
