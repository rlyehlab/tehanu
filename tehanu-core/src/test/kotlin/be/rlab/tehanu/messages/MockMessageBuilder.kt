package be.rlab.tehanu.messages

import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.messages.model.Response
import be.rlab.tehanu.messages.model.text
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class MockMessageBuilder {
    val instance: MessageBuilder = mock()

    fun replyTo(message: Message): MockMessageBuilder = apply {
        whenever(instance.replyTo(message)).thenReturn(instance)
    }

    fun text(
        message: String,
        vararg args: String
    ): MockMessageBuilder = apply {
        whenever(instance.text(message, *args)).thenReturn(instance)
    }

    fun<T> custom(response: Response<T>): MockMessageBuilder = apply {
        whenever(instance.custom(response)).thenReturn(instance)
    }

    fun options(vararg newOptions: Pair<String, Any?>): MockMessageBuilder = apply {
        whenever(instance.options(*newOptions)).thenReturn(instance)
    }

    fun<T> option(name: String, result: T?): MockMessageBuilder = apply {
        whenever(instance.option<T>(name)).thenReturn(result)
    }
}
