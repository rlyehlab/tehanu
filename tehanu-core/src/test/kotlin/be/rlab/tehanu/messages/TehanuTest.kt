package be.rlab.tehanu.messages

import be.rlab.tehanu.BotAware
import be.rlab.tehanu.Tehanu
import be.rlab.tehanu.clients.Client
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class TehanuTest {

    private val client: Client = mock()

    @Test
    fun initialize() {
        val listener: BotAware = mock()
        val tehanu = Tehanu(
            botListeners = listOf(listener),
            clients = emptyList(),
            serviceProvider = mock()
        )
        verify(listener).tehanu = tehanu
    }

    @Test
    fun start() {
        val tehanu = createTehanu(
            client = client
        )
        tehanu.start()
        verify(client).start()
    }

    private fun createTehanu(client: Client? = null): Tehanu {
        return Tehanu(
            botListeners = emptyList(),
            clients = client?.let { listOf(client) } ?: emptyList(),
            serviceProvider = mock()
        )
    }
}
