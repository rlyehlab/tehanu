package be.rlab.tehanu.messages.model

import be.rlab.tehanu.media.model.MediaFile
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

data class TestMessage(
    val text: String = "",
    val files: List<MediaFile> = emptyList(),
    val entities: List<MessageEntity> = emptyList(),
    val messageId: Long = System.currentTimeMillis()
) {
    val instance: Message = mock {
        on { text } doReturn text
        on { files } doReturn files
        on { entities } doReturn entities
        on { messageId } doReturn messageId

        entities.forEach { entity ->
            on { hasEntity(entity.type) } doReturn true
            on { getEntity(entity.type) } doReturn entity
        }
    }

    fun hasText(value: Boolean): TestMessage = apply {
        whenever(instance.hasText()).thenReturn(value)
    }

    fun hasMedia(value: Boolean): TestMessage = apply {
        whenever(instance.hasMedia()).thenReturn(value)
    }
}
