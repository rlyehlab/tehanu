package be.rlab.tehanu.view

import be.rlab.tehanu.clients.UpdateContext

/** Validation utilities for Telegram input.
 */
object Validators {

    private val NUMBER_FORMAT: Regex = Regex("^\\d+\\.?\\d*$")

    fun UpdateContext.assertNumber(
        number: String,
        message: String? = null,
        callback: (() -> Unit)? = null
    ) {
        assert(
            number.matches(NUMBER_FORMAT),
            message ?: translations["view-fields-validation-invalidNumber"],
            callback = callback
        )
    }

    fun UpdateContext.assertNumber(
        number: String,
        transition: Transition
    ) {
        assert(
            number.matches(NUMBER_FORMAT),
            transition = transition
        )
    }

    fun UpdateContext.assertRequired(
        expression: Boolean,
        message: String? = null,
        callback: (() -> Unit)? = null
    ) {
        assert(
            expression,
            message ?: translations["view-fields-validation-required"],
            callback = callback
        )
    }

    /** Runs a test function and calls the fail callback if the test function throws an IllegalArgumentException.
     * If the test function throws, it re-throws the same IllegalArgumentException at the end.
     *
     * @param testCallback Test function.
     * @param failCallback Callback to execute if the test function throws an error.
     */
    fun UpdateContext.assertThrows(
        testCallback: UpdateContext.() -> Unit,
        failCallback: UpdateContext.() -> Unit
    ) {
        try {
            testCallback(this)
        } catch (cause: IllegalArgumentException) {
            failCallback(this)
            throw cause
        }
    }

    /** Asserts that a value is true, and invokes the fail callback if the assertion failed.
     * It throws an IllegalArgumentException if the assertion fails.
     *
     * @param assertion Test function.
     * @param message Optional message to send back to the chat.
     * @param failCallback Callback to execute if the test function throws an error.
     */
    fun UpdateContext.assertTrue(
        assertion: Boolean,
        message: String? = null,
        failCallback: (() -> Unit)? = null
    ) {
        assert(assertion = assertion, message = message, callback = failCallback)
    }

    /** Asserts that a value is true, and invokes the fail callback if the assertion failed.
     * Optionally it follows a transition to another state.
     * It throws an IllegalArgumentException if the assertion fails.
     *
     * @param assertion Test function.
     * @param transition Optional transition.
     * @param failCallback Callback to execute if the test function throws an error.
     */
    fun UpdateContext.assertTrue(
        assertion: Boolean,
        transition: Transition,
        failCallback: (() -> Unit)? = null
    ) {
        assert(assertion = assertion, transition = transition, callback = failCallback)
    }

    /** Asserts a condition and calls the fail callback if the result is false.
     * If the assertion fails, it throws an IllegalArgumentException at the end.
     *
     * @param context Current
     * @param result Condition result.
     * @param failCallback Callback to execute if the result is false.
     */
    inline fun assertTrue(context: UpdateContext, result: Boolean, failCallback: UpdateContext.() -> Unit) {
        if (!result) {
            failCallback(context)
            throw IllegalArgumentException()
        }
    }

    private fun UpdateContext.assert(
        assertion: Boolean,
        message: String? = null,
        transition: Transition? = null,
        callback: (() -> Unit)? = null
    ) {
        if (assertion) {
            return
        }
        callback?.invoke()
        transition?.let { transitionTo(transition.handlerName) }
        message?.let { talk(message) }
        throw IllegalArgumentException()
    }
}
