package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.model.Action
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.Precondition
import be.rlab.tehanu.view.ValidationException
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

/** Represents an input control attached to a message.
 *
 * A Control may contain __children__ controls, which can be used to build a graph of components. It also
 * has a reference to the [parent] control.
 *
 * The Control has three phases: rendering, validation and selection.
 *
 * The rendering phase is triggered by the [render] method. It will call the [renderInternal] template
 * method, and then it will render all the children controls.
 *
 * The validation phase occurs just before the selection phase, when there is a user interaction with the Control,
 * and it's triggered by the [validate] method. First it validates all the children controls, then it runs all the
 * registered validators, and finally it calls the [validateInternal] to delegate the validation if required.
 *
 * The selection phase occurs when there is a user input to report to the control, and it's triggered by the
 * [select] method. In this phase, the control must resolve a value based on the user input. It will call
 * the registered [resolveValue] callback to resolve the control's value, then it will call the [selectInternal]
 * template method, and finally it will call the [parent]'s [select]. The event bubbles up from the control to
 * all its parents. Once the value is resolved, the Control status will change to _completed_.
 */
open class Control(
    val id: UUID,
    val parent: Control? = null,
    /** Description to display to the user. */
    description: MessageBuilder? = null,
    private val renderCallback: Control.() -> Unit = {}
) {
    companion object {
        fun new(
            description: MessageBuilder? = null,
            parent: Control? = null,
            renderCallback: Control.() -> Unit = {}
        ): Control = Control(
            id = UUID.randomUUID(),
            description = description,
            parent = parent,
            renderCallback = renderCallback
        )

        /** Returns all controls, including the descendants, for the specified control.
         * @param control Control to read descendants from.
         * @return the list of all controls.
         */
        fun descendantsOf(control: Control): List<Control> {
            return if (control.children().isEmpty()) {
                emptyList()
            } else {
                control.children().flatMap { child ->
                    descendantsOf(child) + child
                }
            }
        }
    }

    /** Control value. */
    private var value: Any? = null
    /** Property bag to store custom attributes. */
    val attributes: MutableMap<String, Any> = mutableMapOf()
    /** Indicates whether the control is disabled.
     * When a Control is disabled no more user interaction will be accepted.
     */
    private var disabled: Boolean = false
    /** Indicates whether this control requires redrawing. */
    private var redraw: Boolean = false
    /** Indicates whether the Control's value is already set.
     * We need this flag since null is a valid value, so we cannot infer whether the Control's value
     * is set looking at the [value] field.
     */
    private var hasValue: Boolean = false
    /** List of immediate child controls. */
    private val children: MutableList<Control> = mutableListOf()
    /** Preconditions required to render this control. */
    internal val preconditions: MutableList<Precondition> = mutableListOf()
    /** List of registered validators. */
    private val validators: MutableList<UpdateContext.() -> Unit> = mutableListOf()
    /** Optional function to resolve the Control's value. */
    private var resolveValue: (UpdateContext.() -> Unit)? = null
    private val descriptionInternal = description
    val description: MessageBuilder? get() {
        var resolved: MessageBuilder? = null
        var target: Control? = this
        while (resolved == null && target != null) {
            resolved = target.descriptionInternal
            target = target.parent
        }
        return resolved
    }

    /** Renders this control.
     * @param context Current update context.
     */
    protected open fun renderInternal(context: UpdateContext): Control = this

    /** Validates the user input.
     * If there is a validation error, the user needs to provide a new input.
     *
     * @param context Current context.
     * @throws IllegalArgumentException if there's a validation error.
     * @throws ValidationException if there's a validation error.
     */
    protected open fun validateInternal(context: UpdateContext): Control = this

    /** Reports user input for this control.
     *
     * @param context Current context.
     */
    protected open fun selectInternal(context: UpdateContext): Control = this

    /** Renders this control and all its children.
     * @param context Current update context.
     */
    fun render(context: UpdateContext): Control = apply {
        renderCallback()
        if (this !is ControlSet) {
            children.forEach { child -> child.render(context) }
        }
        renderInternal(context)
    }

    /** Validates the user input.
     * If there is a validation error, the user needs to provide a new input.
     *
     * @param context Current context.
     * @throws IllegalArgumentException if there's a validation error.
     * @throws ValidationException if there's a validation error.
     */
    fun validate(context: UpdateContext): Control = apply {
        try {
            if (waitingForMessage()) {
                children.forEach { child -> child.validate(context) }
                validators.forEach { validator -> validator(context) }
                validateInternal(context)
            }
        } catch (cause: IllegalArgumentException) {
            ValidationException.throwWithMessage(cause.message ?: "validation error")
        }
    }

    /** Reports user input for this control.
     *
     * @param context Current context.
     */
    fun select(context: UpdateContext): Control = apply {
        if (isEnabled()) {
            resolveValue?.invoke(context)
            selectInternal(context)
            parent?.select(context)
        }
    }

    /** Recursively finds a control by id starting at this control.
     *
     * If the id matches this control's id, it returns this control. Otherwise
     * it recursively searches through the children.
     *
     * @param controlId Id of the control to verify.
     * @return true if the control id exists or is its the own identifier.
     */
    fun find(controlId: UUID): Control? {
        return descendants().find { child ->
            child.id == controlId
        }
    }

    /** Returns the Control [InputMode].
     * By default, the input mode is [InputMode.Message].
     */
    open fun inputMode(): InputMode {
        return InputMode.Message
    }

    /** Indicates whether this control is enabled.
     * If enabled, the control accepts user interaction.
     */
    fun isEnabled(): Boolean {
        return !disabled
    }

    /** Disables this control to reject new user interactions.
     */
    fun disable(): Control = apply {
        disabled = true
    }

    /** Returns the list of immediate child controls.
     * @return a list of child controls.
     */
    fun children(): List<Control> {
        return children.toList()
    }

    /** Returns all controls, including the descendants.
     * @return the list of all controls.
     */
    fun descendants(): List<Control> {
        return descendantsOf(this)
    }

    /** Adds a child Control.
     * @param child Control to add.
     */
    fun addChild(child: Control): Control = apply {
        children += child
    }

    /** Sets the control value.
     * @param resolvedValue Control value.
     */
    fun setValue(resolvedValue: Any?): Control = apply {
        value = resolvedValue
        hasValue = true
    }

    /** Returns the control value.
     * @return the control value, or null if not set.
     */
    @Suppress("UNCHECKED_CAST")
    fun<T : Any> getValue(): T? {
        return value as T?
    }

    operator fun<T : Any> getValue(
        thisRef: Any?,
        property: KProperty<*>
    ): T {
        return getValue<T>() as T
    }

    /** Indicates whether this control has a value.
     * @return true if the control has a value, false otherwise.
     */
    fun hasValue(): Boolean {
        return hasValue
    }

    /** Marks this control for redrawing.
     * @param mustRedraw True to redraw, false otherwise.
     */
    fun markForRedraw(mustRedraw: Boolean = true): Control = apply {
        redraw = mustRedraw
    }

    /** Configuration block to group and register [Precondition]s.
     * @param callback Configuration callback.
     */
    fun preconditions(callback: () -> Unit): Control = apply {
        callback()
    }

    /** Defines a [Precondition] that must be fulfilled to render this control.
     *
     * @param callback Callback to build the precondition.
     */
    fun precondition(callback: Precondition.() -> Unit): Control = apply {
        addPrecondition(Precondition.new(callback))
    }

    /** Adds a [Precondition] that must be fulfilled to render this control.
     * @param precondition Precondition to add.
     */
    fun addPrecondition(precondition: Precondition): Control = apply {
        preconditions.add(precondition)
    }

    /** Adds a new validator.
     * All validators are called in the order they are added.
     *
     * The validator function must throw an [IllegalArgumentException] to indicate the validation didn't pass.
     * If throws, the exception message is send to the current channel.
     *
     * @param newValidator Validator function.
     */
    fun validator(newValidator: UpdateContext.() -> Unit): Control = apply {
        validators.add(newValidator)
    }

    /** Adds a callback to build the field value.
     * @param callback Callback to build a value based on the current input.
     */
    fun resolveValue(callback: UpdateContext.() -> Unit): Control = apply {
        resolveValue = callback
    }

    /** Returns all the children controls of the required type.
     * @param type Required controls type.
     * @return the list of controls of the required type.
     */
    @Suppress("UNCHECKED_CAST")
    fun<T : Control> findByType(type: KClass<T>): List<T> {
        return children().filter { control -> control::class == type } as List<T>
    }

    /** Determines whether an action is targeted to this control or any of the children.
     * @param action Action to verify.
     * @return true if this control is enabled and can handle the action, false otherwise.
     */
    fun canHandle(action: Action?): Boolean {
        val isTarget = action != null
                && isEnabled()
                && id == UUID.fromString(action.data)
        return isTarget || children.any { child -> child.canHandle(action) }
    }

    /** Determines whether this control is expecting more user interactions.
     *
     * By default, if the control is disabled or its value is not null, it's considered done.
     *
     * @return true if the control does not expect more user interaction, false otherwise.
     */
    open fun isCompleted(): Boolean {
        return !isEnabled() || hasValue()
    }

    /** Determines whether this control or any of its children is waiting for a Message input.
     *
     * It is true if this control is enabled; there is no resolved value yet; it has a value resolver
     * function provided via [resolveValue]; or there is a child control that's also waiting for a Message.
     *
     * @return true if this control is waiting for a Message, false otherwise.
     */
    fun waitingForMessage(): Boolean {
        if (!isEnabled()) {
            return false
        }
        val enabledControls = children().filter(Control::isEnabled)
        val selfWaitingForMessage = inputMode() == InputMode.Message || resolveValue != null

        return !isCompleted() && (
            selfWaitingForMessage || enabledControls.any { control -> control.waitingForMessage() }
        )
    }

    /** Determines whether this control is waiting for actions.
     *
     * It is true if this control is enabled and there is an enabled control with [InputMode.Action].
     *
     * @return true if this control is waiting for an Action, false otherwise.
     */
    fun waitingForAction(): Boolean {
        if (!isEnabled()) {
            return false
        }
        return inputMode() == InputMode.Action || children().any { control ->
            control.waitingForAction()
        }
    }

    /** Calls a redraw callback only if this field requires redraw.
     * @param callback Redraw callback.
     */
    fun redrawIfRequired(callback: Control.() -> Unit): Control = apply {
        if (redraw) {
            callback(this)
            markForRedraw(false)
            children().forEach { control -> control.markForRedraw(false) }
        }
    }
}
