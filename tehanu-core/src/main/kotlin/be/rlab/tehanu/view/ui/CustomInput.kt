package be.rlab.tehanu.view.ui

import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.UserInput

fun UserInput.custom(
    description: MessageBuilder? = null,
    renderCallback: Control.() -> Unit = {}
): Control = Control.new(description, this, renderCallback).apply {
    this@custom.addChild(this)
}

fun Field.custom(
    description: MessageBuilder? = null,
    renderCallback: Control.() -> Unit = {}
): Control = Control.new(description, this, renderCallback).apply {
    this@custom.addChild(this)
}
