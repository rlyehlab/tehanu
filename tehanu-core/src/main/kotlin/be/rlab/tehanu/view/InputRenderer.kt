package be.rlab.tehanu.view

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.view.ui.Control

interface InputRenderer {
    fun supports(
        context: UpdateContext,
        control: Control
    ): Boolean
}