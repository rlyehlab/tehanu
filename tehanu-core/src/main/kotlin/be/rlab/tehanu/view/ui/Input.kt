package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.UserInput
import java.util.*

fun UserInput.input(
    description: MessageBuilder? = null,
    renderCallback: Input.() -> Unit = {}
): Input = Input.new(description, this, renderCallback).apply {
    this@input.addChild(this)
}

fun Field.input(
    description: MessageBuilder? = null,
    renderCallback: Input.() -> Unit = {}
): Input = Input.new(description, this, renderCallback).apply {
    this@input.addChild(this)
}

/** General purpose input field for text-based messages.
 */
@Suppress("UNCHECKED_CAST")
open class Input(
    id: UUID,
    description: MessageBuilder? = null,
    parent: Control? = null,
    renderCallback: Input.() -> Unit
) : Control(id, parent, description, renderCallback as Control.() -> Unit) {

    companion object {
        fun new(
            description: MessageBuilder? = null,
            parent: Control? = null,
            renderCallback: Input.() -> Unit
        ): Input = Input(
            id = UUID.randomUUID(),
            description = description,
            parent = parent,
            renderCallback = renderCallback
        )
    }

    private var parser: (UpdateContext).() -> Any = { incomingMessage.text }

    override fun selectInternal(context: UpdateContext): Input = apply {
        setValue(parser(context))
        disable()
    }

    fun<T : Any> parser(callback: (UpdateContext).() -> T): Input = apply {
        parser = callback
    }
}
