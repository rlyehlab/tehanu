package be.rlab.tehanu.view

/** Must be thrown by validators to report validation errors.
 * @param result Validation error result.
 */
class ValidationException(val result: ValidationErrorResult) : RuntimeException() {
    companion object {
        fun throwWithMessage(message: String) {
            throw ValidationException(ValidationErrorResult.message(message))
        }

        fun throwWithTransition(transition: Transition) {
            throw ValidationException(ValidationErrorResult.transition(transition))
        }
    }
}
