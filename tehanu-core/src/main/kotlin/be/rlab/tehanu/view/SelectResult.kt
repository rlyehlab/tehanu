package be.rlab.tehanu.view

enum class SelectResult {
    Skipped,
    InProgress,
    Completed
}
