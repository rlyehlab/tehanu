package be.rlab.tehanu.view

import be.rlab.tehanu.clients.State

/** Represents a transition from one [State] to another.
 */
data class Transition(
    /** Name of the handler to transition to. */
    val handlerName: String,
    /** Parameters passed to the target handler. */
    val params: Map<String, Any>
) {
    companion object {
        /** Defines a [Transition] to another [State].
         *
         * @param handlerName Name of the handler to transition to.
         * @param params Params to pass to the target handler.
         */
        fun transitionTo(handlerName: String, params: Map<String, Any> = emptyMap()): Transition {
            return Transition(handlerName, params)
        }
    }
}
