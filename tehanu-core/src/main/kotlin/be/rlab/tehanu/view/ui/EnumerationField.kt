package be.rlab.tehanu.view.ui

import be.rlab.tehanu.view.UserInput

/** Field to ask for [Enum] values.
 *
 * It displays buttons with all values in the enumeration.
 */
inline fun<reified T : Enum<T>> UserInput.enumeration(
    description: String,
    mode: KeyboardMode = KeyboardMode.SINGLE_SELECTION,
    crossinline callback: (Field.() -> Unit) = {}
): Field = field(description) {
    keyboard(mode = mode) {
        enumValues<T>().forEach { value ->
            button(value.name, value)
        }
    }

    callback(this)
}
