package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.messages.MessageBuilder
import java.util.*
import kotlin.collections.ArrayDeque

/** A ControlSet represents a group of [Control]s.
 *
 * A ControlSet works as an Iterator, it will provide a [Control] to render as long
 * as it has available [Control]s. If any of the [Control]s is also a ControlSet, it
 * will go through each Control inside the child ControlSet.
 *
 * A ControlSet has no visual representation, so it will not be rendered in the Client.
 * Yet, [Control.render] will be called to render its children.
 */
abstract class ControlSet(
    id: UUID,
    /** Field description to display to the user. */
    description: MessageBuilder?,
    parent: Control? = null,
    /** Callback invoked to render this field. */
    renderCallback: Control.() -> Unit
) : Control(id, parent, description, renderCallback) {
    private val controls: ArrayDeque<Control> = ArrayDeque()
    private var currentControl: Control? = null
    private var lastControl: Control? = null

    /** Last control retrieve from the iterator. */
    val current: Control get() = requireNotNull(currentControl) { "there is no active control" }

    /** Determines whether there are more Controls to render.
     * @return true if there are more Controls to render, false otherwise.
     */
    fun hasMoreControls(): Boolean {
        return lastControl
            ?.let { lastControl ->
                lastControl is ControlSet && lastControl.hasMoreControls()
                || controls.isNotEmpty()
            } ?: controls.isNotEmpty()
    }

    /** Returns the next Control to render.
     * It throws an exception if there is no Control to render.
     * @return the next control.
     */
    fun nextControl(): Control {
        if (lastControl is ControlSet && (lastControl as ControlSet).hasMoreControls()) {
            currentControl = (lastControl as ControlSet).nextControl()
        } else {
            lastControl = controls.removeFirst()
            currentControl = lastControl
        }
        return currentControl!!
    }

    override fun renderInternal(context: UpdateContext): Control = apply {
        controls += children()
    }
}
