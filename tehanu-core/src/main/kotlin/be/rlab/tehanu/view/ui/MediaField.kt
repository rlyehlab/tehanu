package be.rlab.tehanu.view.ui

import be.rlab.tehanu.media.model.MediaFile
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators.assertRequired

/** Field to ask user for media files.
 */
fun UserInput.media(
    /** Field description. */
    description: String,
    /** List of supported mime types, by default all mime types are supported. */
    mimeTypes: List<String> = emptyList(),
    callback: (Field.() -> Unit) = {}
): Field = field(description) {

    fun validateMimeTypes(values: List<MediaFile>): Boolean {
        return values.all { mediaFile -> mimeTypes.contains(mediaFile.mimeType) }
    }

    validator {
        assertRequired(
            incomingMessage.hasMedia(),
            translations["view-fields-validation-invalidMedia"]
        )
        assertRequired(
            mimeTypes.isEmpty() || validateMimeTypes(incomingMessage.files),
            translations["view-fields-validation-invalidMediaType"]
        )
    }

    resolveValue {
        incomingMessage.files
    }

    callback(this)
}
