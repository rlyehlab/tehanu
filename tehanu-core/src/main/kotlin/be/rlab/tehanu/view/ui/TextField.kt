package be.rlab.tehanu.view.ui

import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators.assertTrue

fun UserInput.text(
    description: MessageBuilder,
    pattern: Regex? = null,
    callback: (Input.() -> Unit) = {}
) = input(description) {
    validator {
        assertTrue(
            pattern?.matches(incomingMessage.text) ?: true,
            translations.get("view-fields-text-invalidFormat", pattern.toString())
        )
    }
    callback(this)
}
