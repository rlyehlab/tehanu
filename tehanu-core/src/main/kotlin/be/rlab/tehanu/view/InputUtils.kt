package be.rlab.tehanu.view

import be.rlab.tehanu.support.ObjectMapperFactory

object InputUtils {
    /** Converts a String value to another type.
     * It uses Jackson's ObjectMapper configured with SNAKE_CASE naming strategy.
     * @param value Value to convert.
     * @return the parsed value.
     */
    inline fun<reified T> parseValue(value: String): T {
        return ObjectMapperFactory.snakeCaseMapper.readValue("\"$value\"", T::class.java)
    }
}
