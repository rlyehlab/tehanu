package be.rlab.tehanu.view

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.UpdateDispatcher
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/** Represents a precondition that must be satisfied before processing another action.
 *
 * If the precondition is unsatisfied, the resolver function will be invoked to process this precondition.
 * The resolver function must either call [resolve] to mark this precondition as satisfied; [reject] to
 * fail if the precondition is not satisfied; or [transitionTo] to force a transition to another State.
 *
 * If the resolver function forces a transition to another State, the Precondition will wait until the next
 * State returns to mark it as satisfied.
 */
class Precondition(
    /** Resolver function. */
    private val resolver: Precondition.() -> Unit,
    /** Caches the result to indicate when this precondition is satisfied.
     * This will avoid calling the resolver function more than once if the condition is already satisfied.
     */
    private var satisfied: Boolean = false,
    /** Transition to another State. */
    var transition: Transition? = null
) {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(Precondition::class.java)

        /** Creates a precondition with a resolver function.
         *
         * @param resolver Resolver function.
         * @return the new precondition.
         */
        fun new(resolver: Precondition.() -> Unit): Precondition = Precondition(
            resolver = resolver
        )

        /** Resolves all unsatisfied preconditions.
         *
         * If a Precondition is satisfied, it is skipped. Otherwise, it calls the resolver function
         * to fulfill the Precondition.
         *
         * If the result is a transition, it forces the transition to the next state and waits
         * until the next state returns in order to continue checking more preconditions.
         *
         * If the result is undetermined (neither [Precondition.resolve] or [Precondition.reject] were called from the
         * resolver function), it rejects the Precondition with [RejectAction.THROW_UNHANDLED_ERROR].
         *
         * Once all preconditions are satisfied, it invokes the [done] callback.
         *
         * @param context Current context.
         * @param preconditions Preconditions to resolve.
         * @param done Callback invoked once all preconditions are satisfied.
         */
        fun processAll(
            context: UpdateContext,
            preconditions: Iterator<Precondition>,
            done: () -> Unit
        ) {
            if (!preconditions.hasNext()) {
                logger.debug("all preconditions have been resolved")
                done()
            } else {
                logger.info("resolving precondition")
                val precondition = preconditions.next()
                if (precondition.isSatisfied()) {
                    processAll(context, preconditions, done)
                } else {
                    precondition.process()
                    val transition = precondition.transition
                    if (transition != null) {
                        logger.info(
                            "precondition forced a transition: handlerName=${transition.handlerName},params=handlerName=${transition.params}"
                        )
                        val dispatcher = context.serviceProvider.getService(UpdateDispatcher::class)
                        dispatcher.transitionTo(context, transition, done)
                    } else if (precondition.isSatisfied()) {
                        logger.info("precondition is satisfied, processing next")
                        processAll(context, preconditions, done)
                    } else {
                        throw PreconditionEvaluationException(
                            "the precondition is not satisfied and it wasn't rejected either",
                            RejectAction.THROW_UNHANDLED_ERROR
                        )
                    }
                }
            }
        }
    }

    /** Resolves this precondition and returns the result.
     * @return the precondition resolved value.
     */
    fun resolve(): Precondition = apply {
        satisfied = true
    }

    /** Rejects this precondition if the evaluation fails.
     * @param message Rejection message.
     * @param action Action to take.
     */
    fun reject(message: String, action: RejectAction = RejectAction.NOTIFY_USER) {
        throw PreconditionEvaluationException(message, action)
    }

    /** Forces a transition to another State to fulfill this precondition.
     * @param handlerName Handler to transition to.
     * @param params Additional parameters passed to the handler.
     */
    fun transitionTo(handlerName: String, params: Map<String, Any> = emptyMap()): Precondition = apply {
        transition = Transition(handlerName, params)
    }

    /** Indicates whether this precondition is satisfied.
     * @return true if satisfied, false otherwise.
     */
    fun isSatisfied(): Boolean {
        return satisfied
    }

    /** Processes this precondition.
     */
    private fun process(): Precondition = apply {
        resolver.invoke(this)
    }
}
