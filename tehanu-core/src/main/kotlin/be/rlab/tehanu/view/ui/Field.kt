package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.text
import be.rlab.tehanu.view.UserInput
import java.util.*

/** A field group is a container field to group another fields.
 * @param callback Callback to initialize the field group.
 */
fun UserInput.fieldGroup(callback: Field.() -> Unit): Field {
    return Field.group(this, callback).apply(::addChild)
}

/** Defines a field.
 *
 * @param description Field's message.
 * @param args Message arguments to expand with the underlying [be.rlab.tehanu.i18n.MessageSource].
 * @param callback Callback to initialize the field.
 */
fun UserInput.field(
    description: String,
    vararg args: String,
    callback: (Field.() -> Unit) = {}
): Field {
    return field(MessageBuilder().text(description, *args), callback)
}

/** Defines a field.
 *
 * @param description Field's message.
 * @param callback Callback to initialize the field.
 */
fun UserInput.field(
    description: MessageBuilder? = null,
    callback: (Field.() -> Unit) = {}
): Field {
    return Field.new(description, this, callback).apply(::addChild)
}

/** This class represents a field in a [UserInput].
 *
 * A field is the smaller user input unit. It supports [Control]s, visual components that depend on the client.
 *
 * A field is active as long there is at least one active [Control], or if the value is not yet resolved.
 *
 * A field can be extended to implement custom fields. Custom fields can define the [resolveValue] callback to
 * build the value based on the user input.
 *
 * The field defines three flows in the following order:
 *      1. render flow
 *      2. validation flow
 *      3. select flow
 *
 * The render flow translates the field model to the actual visual representation in the Client. It calls the
 * render() method from the controls and delegates the drawing to the Client.
 *
 * The validation flow allows to validate an Update before the field's value is resolved. If the validation fails, the
 * user might need to provide a new value. It is possible to add multiple validators using the validator() method.
 * Validators are invoked in the order they were added. All controls' validate() methods are called before the
 * Field validators.
 *
 * The select flow resolves the field value from the Update.
 */
@Suppress("UNCHECKED_CAST")
class Field private constructor(
    id: UUID,
    /** Field description to display to the user. */
    description: MessageBuilder?,
    val isFieldGroup: Boolean,
    parent: Control? = null,
    /** Callback invoked to render this field. */
    renderCallback: Control.() -> Unit
) : ControlSet(id, description, parent, renderCallback) {
    companion object {
        fun new(
            description: MessageBuilder? = null,
            parent: Control? = null,
            renderCallback: Field.() -> Unit = {}
        ) = Field(
            id = UUID.randomUUID(),
            description = description,
            isFieldGroup = false,
            parent = parent,
            renderCallback = renderCallback as Control.() -> Unit
        )

        fun group(
            parent: Control? = null,
            renderCallback: Field.() -> Unit
        ) = Field(
            id = UUID.randomUUID(),
            description = null,
            isFieldGroup = true,
            parent = parent,
            renderCallback = renderCallback as Control.() -> Unit
        )
    }

    private val values: MutableList<Any?> = mutableListOf()

    override fun selectInternal(context: UpdateContext): Field = apply {
        if (hasValue() || isFieldGroup) {
            return this
        }

        if (current.hasValue()) {
            values += current.getValue()
        }

        if (values.size == children().size) {
            setValue(values)
        }
    }
}
