package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.UserInput
import java.util.*

enum class KeyboardMode {
    SINGLE_SELECTION,
    MULTIPLE_SELECTION
}

/** Registers a new keyboard in this field.
 * @param callback Callback to build the keyboard.
 * @return this field.
 */
fun UserInput.keyboard(
    description: MessageBuilder? = null,
    mode: KeyboardMode = KeyboardMode.SINGLE_SELECTION,
    callback: Keyboard.() -> Unit
): Keyboard = Keyboard.new(mode, description, this, callback).apply {
    this@keyboard.addChild(this)
}

/** Registers a new keyboard in this field.
 * @param callback Callback to build the keyboard.
 * @return this field.
 */
fun Field.keyboard(
    description: MessageBuilder? = null,
    mode: KeyboardMode = KeyboardMode.SINGLE_SELECTION,
    callback: Keyboard.() -> Unit
): Keyboard = Keyboard.new(mode, description, this, callback).apply {
    this@keyboard.addChild(this)
}

/** Represents an input keyboard attached to a message.
 */
class Keyboard(
    id: UUID,
    private val mode: KeyboardMode,
    description: MessageBuilder? = null,
    parent: Control? = null,
    renderCallback: Control.() -> Unit
) : Control(id, parent, description, renderCallback) {

    companion object {
        @Suppress("UNCHECKED_CAST")
        fun new(
            mode: KeyboardMode = KeyboardMode.SINGLE_SELECTION,
            description: MessageBuilder? = null,
            parent: Control? = null,
            renderCallback: Keyboard.() -> Unit = {}
        ): Keyboard = Keyboard(
            id = UUID.randomUUID(),
            mode = mode,
            description = description,
            parent = parent,
            renderCallback = renderCallback as Control.() -> Unit
        )
    }

    private var submitButton: Button? = null
    var lineLength: Int = 1
        private set

    override fun renderInternal(context: UpdateContext): Control = apply {
        require(mode != KeyboardMode.MULTIPLE_SELECTION || submitButton != null) {
            "submitButton is required in MULTIPLE_SELECTION mode"
        }
    }

    override fun selectInternal(context: UpdateContext): Keyboard = apply {
        markForRedraw()
    }

    override fun inputMode(): InputMode {
        return InputMode.Action
    }

    /** Adds a new button to this keyboard.
     * @param title Text displayed in the button.
     * @param value Value reported by the inline query when user selects this button.
     */
    fun button(
        title: String,
        value: Any,
        type: ButtonType = ButtonType.BUTTON,
        callback: (Button.() -> Unit) = {}
    ): Keyboard = apply {
        val button = Button.new(title, value, null, type, parent = this, renderCallback = callback)
        addChild(
            button.onClick {
                toggle(button)
                button.markForRedraw()
            }
        )
    }

    /** Adds a new button of type checkbox to this keyboard.
     * @param title Text displayed in the button.
     * @param value Button value.
     */
    fun checkbox(
        title: String,
        value: Any,
        callback: Button.() -> Unit = {}
    ): Keyboard = apply {
        button(title, value, ButtonType.CHECKBOX, callback)
    }

    /** Adds a new button of type radio to this keyboard.
     * @param title Text displayed in the button.
     * @param value Button value.
     */
    fun radio(
        title: String,
        value: Any,
        callback: Button.() -> Unit = {}
    ): Keyboard = apply {
        button(title, value, ButtonType.RADIO, callback)
    }

    /** Configures the submit button for multiple selection mode.
     * @param title Text displayed in the button.
     */
    fun submitButton(
        title: String,
        callback: Button.() -> Unit = {}
    ): Keyboard = apply {
        submitButton = Button.new(title, "SUBMIT", parent = this, renderCallback = callback).apply {
            onClick {
                submit()
                submitButton?.markForRedraw()
            }
        }
        addChild(submitButton!!)
    }

    /** Indicates how many buttons are placed in the same line.
     * By default, all buttons are in the same line and Telegram automatically wraps them.
     *
     * @param length Number of buttons per line.
     */
    fun lineLength(length: Int) {
        require(length > 0)
        lineLength = length
    }

    fun buttons(): List<Button> {
        return children().filterIsInstance<Button>()
    }

    private fun submit() {
        val value: Any? = when (mode) {
            KeyboardMode.MULTIPLE_SELECTION ->
                buttons()
                    .filter(Button::isChecked)
                    .map { button -> button.getValue<Any>() }
            KeyboardMode.SINGLE_SELECTION ->
                buttons().first(Button::isChecked).getValue<Any>()
        }
        setValue(value)
    }

    private fun toggle(button: Button) {
        when (mode) {
            KeyboardMode.MULTIPLE_SELECTION -> button.toggle()
            KeyboardMode.SINGLE_SELECTION -> {
                if (submitButton == null) {
                    setValue(button.getValue())
                }
                buttons().forEach(Button::uncheck)
                button.check()
            }
        }
    }
}
