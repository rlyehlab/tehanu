package be.rlab.tehanu.view.ui

import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators.assertThrows
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/** Telegram field to ask for a [DateTime] type.
 */
fun UserInput.dateTime(
    description: String,
    format: String = "dd/MM/YYYY",
    callback: (Field.() -> Unit) = {}
): Field = field(description) {
    input {
        val dateTimeFormatter: DateTimeFormatter = DateTimeFormat.forPattern(format)

        parser {
            dateTimeFormatter.parseDateTime(incomingMessage.text)
        }

        validator {
            assertThrows({ dateTimeFormatter.parseDateTime(incomingMessage.text) }) {
                talk(translations.get("view-fields-dateTime-invalid", format))
            }
        }
    }
    callback(this)
}
