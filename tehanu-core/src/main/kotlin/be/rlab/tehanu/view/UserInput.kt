package be.rlab.tehanu.view

import be.rlab.tehanu.clients.Update
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.ui.Control
import be.rlab.tehanu.view.ui.ControlSet
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

/** This class represents the Structured User Input configuration for the [UpdateContext].
 */
class UserInput private constructor(
    id: UUID,
    description: MessageBuilder?,
    /** Callback invoked to render this field. */
    renderCallback: Control.() -> Unit
) : ControlSet(id, description = description, renderCallback = renderCallback) {
    companion object {
        /** Creates a new user input.
         */
        fun new(
            description: MessageBuilder? = null,
            renderCallback: Control.() -> Unit = {}
        ): UserInput = UserInput(
            id = UUID.randomUUID(),
            description = description,
            renderCallback = renderCallback
        )
    }

    private val logger: Logger = LoggerFactory.getLogger(UserInput::class.java)
    private var onSubmit: UpdateContext.() -> Unit = {}

    override fun renderInternal(context: UpdateContext): Control = apply {
        super.renderInternal(context)

        Precondition.processAll(context, preconditions.iterator()) {
            description?.let { description ->
                context.sendMessage(description)
            }
            renderNextControl(context)
        }
    }

    override fun selectInternal(context: UpdateContext): Control = apply {
        if (!hasMoreControls()) {
            submit(context)
        }
    }

    /** Resolves the control for the specified Update.
     * @param update Update to resolve the control for.
     * @return the required control.
     */
    fun resolveControl(update: Update): Control {
        return update.action?.let { action ->
            find(UUID.fromString(action.data))
        } ?: current
    }

    /** Handles the select flow.
     * The select flow performs the following actions:
     *     1. resolves the control that belongs to the current message.
     *     2. validates the control.
     *     3. resolves the control's value calling select().
     *     4. redraws the control, if required.
     *     5. if the current control is completed, it renders the next control in the UserInput.
     * @param context Current context.
     */
    fun processInput(context: UpdateContext): Control = apply {
        try {
            val control = resolveControl(context.update)
                .validate(context)
                .select(context)
                .redrawIfRequired {
                    context.client.renderControl(context, this, context.incomingMessage.messageId)
                }
            if (control.isCompleted()) {
                renderNextControl(context)
            }
        } catch (cause: ValidationException) {
            if (cause.result.isTransition()) {
                context.transitionTo(cause.result.transition!!.handlerName)
            } else {
                context.answer(cause.result.message!!)
            }
        }
    }

    private fun renderNextControl(context: UpdateContext) {
        if (!hasMoreControls()) {
            logger.debug("no more controls to render: context=$context")
            return
        }

        val nextControl: Control = nextControl()
        logger.debug("rendering control: controlId=${nextControl.id},context=$context")

        try {
            Precondition.processAll(context, nextControl.preconditions.iterator()) {
                logger.debug("preconditions satisfied, rendering control: id=${nextControl.id},context=$context")
                if (nextControl is ControlSet) {
                    nextControl.render(context)
                    renderNextControl(context)
                } else {
                    context.client.renderControl(context, nextControl.render(context), null)
                }
            }
        } catch (cause: PreconditionEvaluationException) {
            logger.info("precondition not satisfied: message=${cause.message},action=${cause.action}")
            when (cause.action) {
                RejectAction.NOTIFY_USER -> context.answer(cause.message)
                RejectAction.SKIP_CONTEXT_ACTION -> {
                    nextControl.disable()
                    renderNextControl(context)
                }
                RejectAction.THROW_UNHANDLED_ERROR -> throw cause
            }
        }
    }

    /** Calls the onSubmit callback and marks this UserInput as completed.
     * @param context Current context.
     */
    fun submit(context: UpdateContext) {
        if (!isCompleted()) {
            onSubmit(context)
            setValue(true)
        }
    }

    /** Registers a handler that's invoked once all fields are fulfilled by the user.
     * @param callback Callback to execute.
     */
    fun onSubmit(callback: UpdateContext.() -> Unit): UserInput = apply {
        onSubmit = callback
    }

    fun message(callback: MessageBuilder.() -> Unit): MessageBuilder {
        return MessageBuilder().apply(callback)
    }
}
