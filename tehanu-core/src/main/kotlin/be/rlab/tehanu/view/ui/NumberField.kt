package be.rlab.tehanu.view.ui

import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.InputUtils.parseValue
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators.assertNumber
import be.rlab.tehanu.view.Validators.assertRequired
import java.util.*

fun UserInput.number(
    description: MessageBuilder,
    minValue: Number? = null,
    maxValue: Number? = null,
    renderCallback: NumberInput.() -> Unit = {}
): NumberInput = NumberInput(description, minValue, maxValue, this, renderCallback).apply {
    this@number.addChild(this)
}

fun Field.number(
    description: MessageBuilder,
    minValue: Number? = null,
    maxValue: Number? = null,
    renderCallback: NumberInput.() -> Unit = {}
): NumberInput = NumberInput(description, minValue, maxValue, this, renderCallback).apply {
    this@number.addChild(this)
}

@Suppress("UNCHECKED_CAST")
class NumberInput(
    description: MessageBuilder,
    minValue: Number? = null,
    maxValue: Number? = null,
    parent: Control? = null,
    callback: NumberInput.() -> Unit = {}
) : Input(UUID.randomUUID(), description, parent, {
    parser {
        parseValue<Number>(incomingMessage.text)
    }

    validator {
        assertNumber(incomingMessage.text)
        val value: Number = parseValue(incomingMessage.text)
        minValue?.let {
            assertRequired(
                minValue.toDouble() > -1 && value.toDouble() >= minValue.toDouble(),
                translations.get("view-fields-number-validation-minValue", minValue)
            )
        }
        maxValue?.let {
            assertRequired(
                maxValue.toDouble() > -1 && value.toDouble() <= maxValue.toDouble(),
                translations.get("view-fields-number-validation-maxValue", maxValue)
            )
        }
    }

    callback(this as NumberInput)
})
