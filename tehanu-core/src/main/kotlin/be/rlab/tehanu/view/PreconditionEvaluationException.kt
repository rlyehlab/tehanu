package be.rlab.tehanu.view

enum class RejectAction {
    /** Sends the error message to the user. */
    NOTIFY_USER,
    /** Silently skips the action that depends on the precondition. */
    SKIP_CONTEXT_ACTION,
    /** Throws the exception without notifying the user. */
    THROW_UNHANDLED_ERROR
}

class PreconditionEvaluationException(
    override val message: String,
    val action: RejectAction
) : RuntimeException(message)
