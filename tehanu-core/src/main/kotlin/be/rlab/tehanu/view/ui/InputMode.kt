package be.rlab.tehanu.view.ui

/** Enumeration of the supported input modes for [Control]s.
 */
enum class InputMode {
    /** The control input is through a Client message. */
    Message,
    /** The control input is through an [Action]. */
    Action
}
