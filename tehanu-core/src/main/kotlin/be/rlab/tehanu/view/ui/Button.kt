package be.rlab.tehanu.view.ui

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.UserInput
import java.util.*

/** Registers a new button in this field.
 * @param callback Callback to build the keyboard.
 * @return this field.
 */
fun UserInput.button(
    title: String,
    value: Any,
    description: MessageBuilder? = null,
    type: ButtonType = ButtonType.BUTTON,
    callback: Button.() -> Unit = {}
): Button = Button.new(title, value, description, type, parent = this, renderCallback = callback).apply {
    this@button.addChild(this)
}

/** Registers a new button in this field.
 * @param callback Callback to build the keyboard.
 * @return this field.
 */
fun Field.button(
    title: String,
    value: Any,
    description: MessageBuilder? = null,
    type: ButtonType = ButtonType.BUTTON,
    callback: Button.() -> Unit = {}
): Button = Button.new(title, value, description, type, parent = this, renderCallback = callback).apply {
    this@button.addChild(this)
}

/** Positions to place the button icon, relative to the title.
 */
enum class IconPosition {
    TOP,
    RIGHT,
    BOTTOM,
    LEFT
}

enum class ButtonType(
    val iconChecked: String?,
    val iconUnchecked: String?
) {
    BUTTON(null, null),
    RADIO("\uD83D\uDD18", "○"),
    CHECKBOX("✅", "□")
}

/** Represents a button attached to a message.
 *
 * By default, a button allows a single click. Once clicked, its value is resolved and the
 * button is marked as inactive. If it has an [onClick] handler attached, it will not resolve
 * the value automatically on user interaction. The [onClick] handler is responsible of resolving the
 * button's value.
 *
 * The button supports two states: checked and unchecked. It is possible to define an icon for each state.
 */
@Suppress("UNCHECKED_CAST")
class Button(
    id: UUID,
    value: Any,
    description: MessageBuilder?,
    private var title: String,
    private var iconChecked: String?,
    private var iconUnchecked: String?,
    private var type: ButtonType,
    private val iconPosition: IconPosition,
    parent: Control? = null,
    renderCallback: Control.() -> Unit
) : Control(id, parent, description, renderCallback) {
    companion object {
        fun checkbox(
            title: String,
            value: Any,
            description: MessageBuilder? = null,
            parent: Control? = null,
            renderCallback: Button.() -> Unit = {}
        ): Button = new(
            title = title,
            value = value,
            description = description,
            type = ButtonType.CHECKBOX,
            parent = parent,
            renderCallback = renderCallback
        )

        fun radio(
            title: String,
            value: Any,
            description: MessageBuilder? = null,
            parent: Control? = null,
            renderCallback: Button.() -> Unit = {}
        ): Button = new(
            title = title,
            value = value,
            description = description,
            type = ButtonType.RADIO,
            parent = parent,
            renderCallback = renderCallback
        )

        fun new(
            title: String,
            value: Any,
            description: MessageBuilder? = null,
            type: ButtonType = ButtonType.BUTTON,
            iconChecked: String? = null,
            iconUnchecked: String? = null,
            iconPosition: IconPosition = IconPosition.LEFT,
            parent: Control? = null,
            renderCallback: Button.() -> Unit = {}
        ): Button = Button(
            id = UUID.randomUUID(),
            value = value,
            description = description,
            title = title,
            type = type,
            iconChecked = iconChecked ?: type.iconChecked,
            iconUnchecked = iconUnchecked ?: type.iconUnchecked,
            iconPosition = iconPosition,
            renderCallback = renderCallback as Control.() -> Unit,
            parent = parent
        )
    }

    init {
        setValue(value)
    }

    private var checked: Boolean = false

    /** Callback invoked when any user selects this control. */
    private var onClick: (UpdateContext.() -> Unit)? = null

    override fun selectInternal(
        context: UpdateContext
    ): Button = apply {
        if (onClick != null) {
            onClick?.invoke(context)
        } else {
            markForRedraw()
        }
    }

    override fun inputMode(): InputMode {
        return InputMode.Action
    }

    /** Registers an event handler that is called when a user clicks on this button.
     * @param callback Callback invoked when the user clicks on this button.
     */
    fun onClick(callback: UpdateContext.() -> Unit): Button = apply {
        onClick = callback
    }

    /** Returns the button title with the icon, if defined.
     * @return the composed title.
     */
    fun title(): String {
        val icon: String? = if (checked) iconChecked else iconUnchecked
        return icon?.let {
            when (iconPosition) {
                IconPosition.TOP -> "$icon\n$title"
                IconPosition.RIGHT -> "$title $icon"
                IconPosition.BOTTOM -> "$title\n$icon"
                IconPosition.LEFT -> "$icon $title"
            }
        } ?: title
    }

    /** Marks this button as checked.
     * @return this button.
     */
    fun check(): Button = apply {
        checked = true
    }

    /** Marks this button as unchecked.
     * @return this button.
     */
    fun uncheck(): Button = apply {
        checked = false
    }

    /** Indicates whether this button is checked.
     * @return true if the button is checked.
     */
    fun isChecked(): Boolean = checked

    override fun isCompleted(): Boolean {
        return parent?.isCompleted() ?: super.isCompleted()
    }

    /** Toggles the state of this button.
     * @return this button.
     */
    fun toggle(): Button = apply {
        if (isChecked()) {
            uncheck()
        } else {
            check()
        }
    }

    /** Changes this button title.
     * @param newTitle New title.
     * @return this button.
     */
    fun changeTitle(newTitle: String): Button = apply {
        title = newTitle
    }

    /** Sets the button type.
     * @param newType New button type.
     * @return this button.
     */
    fun type(newType: ButtonType): Button = apply {
        type = newType
        iconChecked = newType.iconChecked
        iconUnchecked = newType.iconUnchecked
    }

    /** Changes this button icon.
     * @param newIconChecked New icon for the checked state.
     * @param newIconUnchecked New icon for the unchecked state.
     * @return this button.
     */
    fun updateIcons(
        newIconChecked: String?,
        newIconUnchecked: String?,
    ): Button = apply {
        iconChecked = newIconChecked
        iconUnchecked = newIconUnchecked
    }
}
