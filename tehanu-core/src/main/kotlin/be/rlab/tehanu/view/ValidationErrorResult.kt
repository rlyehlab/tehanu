package be.rlab.tehanu.view

/** A validation error supports two actions: displaying a message to the user or
 * following a transition to another state. This class contains the information
 * about the action in case of validation errors.
 */
data class ValidationErrorResult(
    /** Error to show to the user. */
    val message: String?,
    /** Transition to another state. */
    val transition: Transition?
) {
    companion object {
        fun message(text: String): ValidationErrorResult = ValidationErrorResult(
            message = text,
            transition = null
        )

        fun transition(transition: Transition): ValidationErrorResult = ValidationErrorResult(
            message = null,
            transition = transition
        )
    }

    /** Indicates whether the result is a transition to another state.
     * @return true if the result is a transition.
     */
    fun isTransition(): Boolean = transition != null
}
