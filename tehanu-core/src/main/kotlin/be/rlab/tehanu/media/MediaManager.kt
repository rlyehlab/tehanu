package be.rlab.tehanu.media

import be.rlab.tehanu.media.model.MediaFile
import java.io.File
import java.io.InputStream

/** Provides access to [MediaFile]s and messages.
 */
abstract class MediaManager {
    companion object {

        /** Writes content to a system file.
         * If the file is omitted, it creates a temporary file and enables the delete on exit flag.
         */
        fun writeToFile(
            content: InputStream,
            file: File? = null
        ): File {
            val resolvedFile = file ?: File.createTempFile("tehanu-media-manager", ".tmp").apply {
                deleteOnExit()
            }
            content.use {
                val out = resolvedFile.outputStream()
                content.copyTo(out)
                out.close()
            }
            return resolvedFile
        }
    }

    /** Finds a file by id.
     * @param fileId Unique file id.
     * @return the required file, never null.
     * @throws IllegalArgumentException if the file does not exist.
     */
    abstract fun findById(fileId: String): MediaFile?

    /** Reads the content of a file.
     * @param mediaFile File to read.
     * @return returns an input stream to read the content.
     */
    abstract fun readContent(mediaFile: MediaFile): InputStream

    /** Stores the content of a media file.
     * @param mediaFile Media file to store.
     * @param content File content to store.
     * @return returns the media file.
     */
    abstract fun store(
        mediaFile: MediaFile,
        content: InputStream
    ): MediaFile

    /** Updates the information of a media file.
     * @param mediaFile File to update.
     * @return returns the updated file.
     */
    abstract fun update(mediaFile: MediaFile): MediaFile
}
