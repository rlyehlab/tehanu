package be.rlab.tehanu.media.model

import java.util.*

/** Represents a media file.
 */
data class MediaFile(
    val id: UUID,
    val clientId: String,
    val size: Long,
    val mimeType: String,
    val metadata: Map<String, Any>
) {
    companion object {
        @Suppress("UNCHECKED_CAST")
        fun new(
            clientId: String,
            size: Long,
            mimeType: String,
            metadata: Map<String, Any?>
        ): MediaFile = MediaFile(
            id = UUID.randomUUID(),
            clientId = clientId,
            size = size,
            mimeType = mimeType,
            metadata = metadata.filterValues { it != null } as Map<String, Any>
        )
    }
}
