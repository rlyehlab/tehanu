package be.rlab.tehanu

import be.rlab.tehanu.clients.Client
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/** Tehanu is a messaging client that supports to process messages from multiple [Client]s.
 *
 * The [start] method initializes all the registered [Client]s.
 *
 * Each client will have an exclusive channel that's alive within a coroutine. It uses the
 * thread pool provided by the [Dispatchers.Default] dispatcher.
 */
class Tehanu(
    private val clients: List<Client>,
    botListeners: List<BotAware> = emptyList(),
    val serviceProvider: BotServiceProvider
) {
    private val logger: Logger = LoggerFactory.getLogger(Tehanu::class.java)

    companion object;

    init {
        logger.info("Registering bot into BotAware listeners")
        botListeners.forEach { botListener ->
            botListener.tehanu = this
        }
    }

    fun start() = runBlocking {
        require(clients.isNotEmpty()) { "No clients found, at least one should be registered in the context." }

        logger.info("Initializing clients")
        clients.forEach { client ->
            launch {
                client.start()
            }
        }
        logger.info("Tehanu is ready")
    }
}
