package be.rlab.tehanu.config

import be.rlab.tehanu.DefaultServiceProvider
import be.rlab.tehanu.Tehanu
import be.rlab.tehanu.clients.Client
import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.i18n.MessageSourceFactory
import java.util.*
import kotlin.reflect.KClass

fun Tehanu.Companion.configure(callback: TehanuConfigBuilder.() -> Unit): Tehanu {
    val serviceProvider = DefaultServiceProvider()

    return TehanuConfigBuilder(serviceProvider).let { tehanuConfig ->
        callback(tehanuConfig)
        tehanuConfig.initialize(serviceProvider)
        Tehanu(
            clients = serviceProvider.getServices(Client::class),
            serviceProvider = serviceProvider
        )
    }
}

class TehanuConfigBuilder(
    val serviceProvider: DefaultServiceProvider
) : ConfigBuilder() {
    data class ConfigBuilderEntry(
        val order: Int,
        val factory: TehanuConfigBuilder.() -> ConfigBuilder
    )
    data class DefaultConfigKey(
        val type: KClass<*>,
        val order: Int
    ) : Comparable<DefaultConfigKey> {
        override fun compareTo(other: DefaultConfigKey): Int {
            return this.order.compareTo(other.order)
        }
    }
    private val configBuilderRegistry: MutableList<ConfigBuilderEntry> = mutableListOf()
    private val defaultConfigBuilders: SortedMap<DefaultConfigKey, () -> ConfigBuilder> = sortedMapOf(
        DefaultConfigKey(PersistenceConfigBuilder::class, 0) to { PersistenceConfigBuilder() },
        DefaultConfigKey(SecurityConfigBuilder::class, 1) to { SecurityConfigBuilder() },
        DefaultConfigKey(ServiceConfigBuilder::class, 2) to { ServiceConfigBuilder(serviceProvider) }
    )

    var language: Language = Language.ENGLISH

    fun addBuilder(order: Int = 0, factory: TehanuConfigBuilder.() -> ConfigBuilder) {
        configBuilderRegistry.add(ConfigBuilderEntry(order = order, factory = factory))
    }

    override fun initialize(serviceProvider: DefaultServiceProvider) {
        MessageSourceFactory(language).apply {
            initialize()
            serviceProvider.registerService(this)
        }

        val configBuilders: MutableList<ConfigBuilder> = configBuilderRegistry
            .sortedByDescending { it.order }
            .map { entry ->
                entry.factory(this).apply {
                    serviceProvider.registerService(this)
                }
            }
            .toMutableList()

        defaultConfigBuilders.forEach { (key, factory) ->
            val exists = configBuilders.any { builder -> key.type.isInstance(builder) }
            if (!exists) {
                val keyIndex = defaultConfigBuilders.keys.indexOf(key)
                val prevKeyIndex: Int = keyIndex.takeIf { keyIndex > 0 } ?: 1
                val prevKey = defaultConfigBuilders.keys.toList()[prevKeyIndex - 1]
                val insertIndex = configBuilders.indexOfFirst { builder -> prevKey.type.isInstance(builder) }
                configBuilders.add(insertIndex + 1, factory())
            }
        }

        configBuilders.forEach { configDsl -> configDsl.configure() }
        configBuilders.forEach { configDsl -> configDsl.initialize(serviceProvider) }
    }
}
