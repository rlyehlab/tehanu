package be.rlab.tehanu.config

import be.rlab.tehanu.DefaultServiceProvider
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

fun TehanuConfigBuilder.services(callback: ServiceConfigBuilder.() -> Unit) = addBuilder(order = -1) {
    ServiceConfigBuilder(serviceProvider).apply(callback)
}

class ServiceConfigBuilder(
    val serviceProvider: DefaultServiceProvider
) : ConfigBuilder() {
    data class ServiceDefinition(
        val factory: () -> Any,
        val type: KClass<*>
    ) {
        val dependencies: List<KClass<*>> = type.primaryConstructor
            ?.parameters?.map { param -> param.type.classifier as KClass<*> }
            ?: emptyList()
    }

    val serviceBuilders: MutableMap<KClass<*>, ServiceDefinition> = mutableMapOf()

    inline fun<reified T : Any> ref(): T {
        return serviceProvider.getService()
    }

    inline fun<reified T : Any> register(noinline factory: () -> T) = lazyConfig {
        serviceBuilders += T::class to ServiceDefinition(
            factory = factory,
            type = T::class
        )
    }

    override fun initialize(serviceProvider: DefaultServiceProvider) {
        serviceBuilders.forEach { (_, definition) ->
            serviceProvider.registerService(definition.factory())
        }
    }
}
