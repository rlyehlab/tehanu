package be.rlab.tehanu.config

import be.rlab.tehanu.DefaultServiceProvider
import be.rlab.tehanu.store.*
import be.rlab.tehanu.support.persistence.DataSourceInitializer
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import java.io.File
import javax.sql.DataSource

fun TehanuConfigBuilder.persistence(callback: PersistenceConfigBuilder.() -> Unit) = addBuilder(order = 1000) {
    PersistenceConfigBuilder().apply(callback)
}

/** Configures the persistence strategy to store bot state.
 *
 * It supports three type of persistence strategies:
 *
 * - Data source: a JDBC data source.
 * - File system: a file system directory.
 * - In memory: an in-memory store that will be lost when the application is stopped.
 *
 * Only one persistence strategy can be configured at the same time, they are mutually exclusive.
 *
 * By default, if no persistence strategy is explicitly configured, it uses an in-memory strategy.
 */
class PersistenceConfigBuilder : ConfigBuilder() {
    private var memory: Memory = DisposableMemory()
    private var dataSource: HikariDataSource? = null
    var logStatements: Boolean = false
    var tables: List<Table> = emptyList()

    fun dataSource(callback: HikariDataSource.() -> Unit) = lazyConfig {
        dataSource = HikariDataSource().apply(callback)
    }

    fun fileSystem(memoryDir: File) = lazyConfig {
        memory = FileSystemMemory(memoryDir)
    }

    fun inMemory() = lazyConfig {
        memory = DisposableMemory()
    }

    override fun initialize(serviceProvider: DefaultServiceProvider) = with(serviceProvider) {
        val resolvedMemory: Memory = dataSource?.let { dataSource ->
            val dataSourceConfig: DataSourceConfig = DataSourceConfig.new(
                url = dataSource.jdbcUrl,
                user = dataSource.username,
                password = dataSource.password,
                driver = dataSource.driverClassName,
                logStatements = logStatements
            )
            val database = Database.connect(dataSource)
            val dataSourceInitializer = DataSourceInitializer(
                tables = tables + MemorySlots
            ).apply {
                db = database
                config = dataSourceConfig
                initAuto()
            }
            registerService(DataSource::class) { dataSource }
            registerService(dataSourceConfig)
            registerService(dataSourceInitializer)
            registerService(database)

            DatabaseMemory(
                MemorySlotsDAO().apply {
                    db = database
                    config = dataSourceConfig
                }
            )
        } ?: memory
        registerService(Memory::class) { resolvedMemory }
    }
}
