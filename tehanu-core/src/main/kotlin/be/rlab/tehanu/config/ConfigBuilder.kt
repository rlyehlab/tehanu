package be.rlab.tehanu.config

import be.rlab.tehanu.DefaultServiceProvider

abstract class ConfigBuilder {
    private val configurationRegistry: MutableList<() -> Unit> = mutableListOf()

    abstract fun initialize(serviceProvider: DefaultServiceProvider)

    fun lazyConfig(callback: () -> Unit) {
        configurationRegistry += callback
    }

    fun configure() {
        configurationRegistry.forEach { callback -> callback() }
    }
}
