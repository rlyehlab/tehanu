package be.rlab.tehanu.config

import be.rlab.tehanu.acl.model.Role

/** Global access control and security configuration.
 */
data class SecurityConfig(
    /** List of administrators ids. Admins will have full access to the bot. */
    val admins: List<String>,
    /** List of defined roles. */
    val roles: List<Role>
) {
    fun isAdmin(userId: String): Boolean = admins.contains(userId)

    fun findRole(roleName: String): Role? {
        return roles.find { role ->
            role.name == roleName
        }
    }
}
