package be.rlab.tehanu.config

import be.rlab.tehanu.DefaultServiceProvider
import be.rlab.tehanu.clients.Client

fun TehanuConfigBuilder.clients(callback: ClientConfigBuilder.() -> Unit) = addBuilder {
    ClientConfigBuilder(serviceProvider).apply(callback)
}

class ClientConfigBuilder(
    val serviceProvider: DefaultServiceProvider
) : ConfigBuilder() {
    private val clientRegistry: MutableList<() -> Client> = mutableListOf()

    fun addClient(builder: () -> Client) = lazyConfig {
        clientRegistry.add(builder)
    }

    override fun initialize(serviceProvider: DefaultServiceProvider) {
        clientRegistry.forEach { factory ->
            serviceProvider.registerService(Client::class) { factory() }
        }
    }
}
