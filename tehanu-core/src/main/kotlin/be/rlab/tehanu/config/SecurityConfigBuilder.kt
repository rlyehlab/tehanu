package be.rlab.tehanu.config

import be.rlab.tehanu.DefaultServiceProvider
import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.acl.model.HandlerPermissions
import be.rlab.tehanu.acl.model.Role
import be.rlab.tehanu.command.UserManager
import be.rlab.tehanu.command.UserManager.Companion.MANAGE_USERS

fun TehanuConfigBuilder.security(callback: SecurityConfigBuilder.() -> Unit) = addBuilder(order = 100) {
    SecurityConfigBuilder().apply(callback)
}

class SecurityConfigBuilder : ConfigBuilder() {
    private val admins: MutableList<String> = mutableListOf()
    private val handlersPermissions: MutableList<HandlerPermissions> = mutableListOf()
    private val roles: MutableList<Role> = mutableListOf(
        role(UserManager.ROLE_ADMIN, listOf(MANAGE_USERS), admin = true)
    )

    fun roles(vararg newRoles: Role) = lazyConfig {
        newRoles.forEach { newRole ->
            val index: Int = roles.indexOfFirst { role -> role.name == newRole.name }
            if (index > -1) {
                roles[index] = roles[index].addPermissions(newRole.permissions)
            } else {
                roles.add(newRole)
            }
        }
    }

    fun admins(vararg adminIds: String) = lazyConfig {
        admins.addAll(adminIds.toList())
    }

    fun setPermissions(
        handlerName: String,
        requiredPermissions: List<String>
    ) = lazyConfig {
        handlersPermissions.add(HandlerPermissions(handlerName, requiredPermissions))
    }

    fun role(
        name: String,
        permissions: List<String>,
        title: String = name,
        admin: Boolean = false
    ): Role = Role(name, title, permissions, admin)

    override fun initialize(serviceProvider: DefaultServiceProvider) {
        serviceProvider.apply {
            val config = SecurityConfig(
                admins = admins,
                roles = roles
            )
            val accessControl = AccessControl(
                securityConfig = config,
                memory = serviceProvider.getService()
            )
            registerService(accessControl)
            registerService(config)
        }
    }
}
