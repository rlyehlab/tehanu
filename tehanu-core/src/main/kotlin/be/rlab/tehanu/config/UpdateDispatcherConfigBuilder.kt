package be.rlab.tehanu.config

import be.rlab.tehanu.DefaultServiceProvider
import be.rlab.tehanu.clients.StateManager
import be.rlab.tehanu.clients.UpdateDispatcher
import be.rlab.tehanu.command.Cancel
import be.rlab.tehanu.command.UserManager

fun TehanuConfigBuilder.handlers(callback: UpdateDispatcherConfigBuilder.() -> Unit) = addBuilder {
    UpdateDispatcherConfigBuilder().apply(callback)
}

class UpdateDispatcherConfigBuilder : ConfigBuilder() {
    internal val listeners: MutableList<Any> = mutableListOf()

    fun register(vararg newListeners: Any) = lazyConfig {
        listeners.addAll(newListeners)
    }

    override fun initialize(serviceProvider: DefaultServiceProvider) {
        serviceProvider.apply {
            // Adds built-in handlers.
            listeners += UserManager(serviceProvider.getService())
            listeners += Cancel
            val handlers = listeners.flatMap { listener ->
                HandlerAnnotationConfiguration(
                    listener,
                    serviceProvider.getService(),
                    serviceProvider.getService(),
                    serviceProvider.getService()
                ).getHandlers()
            }

            val stateManager = StateManager()
            val dispatcher = UpdateDispatcher(stateManager).apply {
                addHandlers(handlers)
            }
            registerService(dispatcher)
            registerService(stateManager)
        }
    }
}
