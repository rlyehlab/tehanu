package be.rlab.tehanu.config

import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.acl.HandlerSecurity
import be.rlab.tehanu.acl.model.AccessControlCondition
import be.rlab.tehanu.acl.model.Role
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.Params
import be.rlab.tehanu.annotations.Security
import be.rlab.tehanu.annotations.Triggers
import be.rlab.tehanu.clients.Trigger
import be.rlab.tehanu.clients.UpdateHandler
import be.rlab.tehanu.clients.model.TriggerCondition
import be.rlab.tehanu.clients.model.TriggerGroup
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.i18n.MessageSourceFactory
import be.rlab.tehanu.messages.TextTrigger
import be.rlab.tehanu.support.TriggerSupport
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KAnnotatedElement
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.companionObjectInstance
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.full.findAnnotation
import be.rlab.tehanu.annotations.Trigger as TriggerAnnotation

/** Reads an object or function and resolves the [UpdateHandler]s based on annotation configuration.
 */
class HandlerAnnotationConfiguration(
    /** Handler object or function. */
    private val target: Any,
    /** Factory to resolve the Handler's message source. */
    private val messageSourceFactory: MessageSourceFactory,
    /** Access control manager. */
    private val accessControl: AccessControl,
    /** Security configuration. */
    private val securityConfig: SecurityConfig
) {
    private val logger: Logger = LoggerFactory.getLogger(HandlerAnnotationConfiguration::class.java)

    init {
        val isClassFunction: Boolean = target is KFunction<*>
            && target.parameters.isNotEmpty()
            && target.parameters[0].kind == KParameter.Kind.INSTANCE
        require(!isClassFunction) {
            "Only top-level and instance functions are supported. Maybe you tried to specify a class-level function? " +
            "(invalid function $target)"
        }
    }

    fun getHandlers(): List<UpdateHandler> {
        val handlerFunctions: List<KFunction<*>> = handlerFunctions()

        require(handlerFunctions.isNotEmpty()) {
            "You must annotate at least one function with the @Handler annotations"
        }

        return handlerFunctions.map { handler ->
            resolveHandler(handler)
        }
    }

    private fun handlerFunctions(): List<KFunction<*>> {
        return if (target is KFunction<*>) {
            listOf(target)
        } else {
            target::class.declaredFunctions.filter { method ->
                method.findAnnotation<Handler>() != null
            }
        }
    }

    private fun resolveHandler(handler: KFunction<*>): UpdateHandler {
        logger.debug("creating message handler for function ${handler.name}")
        val handlerInfo: Handler = requireNotNull(handler.findAnnotation())
        val resolvedTriggerGroup: TriggerGroup = resolveTriggerGroup(handler)

        val targetParams: Params? = if (target is KFunction<*>) {
            null
        } else {
            target::class.findAnnotation()
        }
        val handlerParams: Params? = handler.findAnnotation()
        val messageSource: MessageSource = messageSourceFactory.resolve(handlerInfo.name.substringAfter("/"))

        return UpdateHandler(
            target = target,
            handler = handler,
            name = handlerInfo.name,
            triggerGroup = resolvedTriggerGroup,
            scope = handlerInfo.scope.toList(),
            defaultLanguage = messageSourceFactory.defaultLanguage,
            translations = messageSource,
            minParams = handlerInfo.minParams,
            maxParams = handlerInfo.maxParams,
            params = (handlerParams?.params?.toList() ?: emptyList()) + (targetParams?.params?.toList() ?: emptyList()),
            security = resolveSecurityInfo(handler, messageSource)
        )
    }

    private fun mapTriggerGroup(
        symbol: KAnnotatedElement
    ): TriggerGroup? {
        val triggersAnnotation: Triggers? = symbol.findAnnotation()
        val condition: TriggerCondition = triggersAnnotation?.condition ?: TriggerCondition.ALL
        val triggers: List<TriggerAnnotation> = (listOf(symbol.findAnnotation<TriggerAnnotation>()) +
            (triggersAnnotation?.triggers?.toList() ?: emptyList())).filterNotNull()
        val resolvedTriggers: List<Trigger> = triggers.mapIndexed { index, triggerAnnotation ->
            val type = triggerAnnotation.type

            type.companionObjectInstance?.let {
                require(type.companionObjectInstance is TriggerSupport) {
                    "The Trigger companion object must extend the TriggerSupport abstract class."
                }
                (type.companionObjectInstance as TriggerSupport).new(
                    "${symbol.javaClass.simpleName}::Trigger-${index}",
                    triggerAnnotation
                )
            } ?: throw RuntimeException("trigger type not supported: $type")
        }

        return resolvedTriggers.takeIf { it.isNotEmpty() }?.let {
            TriggerGroup(
                condition = condition,
                triggers = resolvedTriggers
            )
        }
    }

    private fun resolveTriggerGroup(handler: KFunction<*>): TriggerGroup {
        val targetTriggerGroup: TriggerGroup? = if (target is KFunction<*>) {
            null
        } else {
            mapTriggerGroup(target::class)
        }
        val handlerTriggerGroup: TriggerGroup? = mapTriggerGroup(handler)

        val resolvedTriggers: List<Trigger> = (handlerTriggerGroup?.triggers ?: emptyList()) +
            (targetTriggerGroup?.triggers ?: emptyList())
        val handlerName: String = requireNotNull(handler.findAnnotation<Handler>()).name

        return when {
            resolvedTriggers.isEmpty() -> {
                logger.debug("no triggers found, adding named handler $handlerName")

                TriggerGroup(
                    condition = TriggerCondition.ALL,
                    triggers = listOf(
                        TextTrigger.new("Named Handler trigger", startsWith = handlerName)
                    )
                )
            }
            else -> {
                logger.debug("triggers found, adding triggers for handler $handlerName")
                TriggerGroup(
                    condition = handlerTriggerGroup?.condition
                        ?: targetTriggerGroup?.condition
                        ?: TriggerCondition.ALL,
                    triggers = resolvedTriggers
                )
            }
        }
    }

    private fun resolveSecurityInfo(
        handler: KFunction<*>,
        messageSource: MessageSource
    ): HandlerSecurity {
        val handlerGroupSecurity: Security? = if (target is KFunction<*>) {
            null
        } else {
            target::class.findAnnotation()
        }
        val handlerSecurity: Security? = handler.findAnnotation()
        val handlerGroupPermissions: List<String> = (handlerGroupSecurity?.requiredPermissions ?: emptyArray()).toList()
        val handlerPermissions: List<String> = (handlerSecurity?.requiredPermissions ?: emptyArray()).toList()
        val handlerGroupRoles: List<String> = (handlerGroupSecurity?.requiredRoles ?: emptyArray()).toList()
        val handlerRoles: List<String> = (handlerSecurity?.requiredRoles ?: emptyArray()).toList()
        val requiredPermissions: List<String> = handlerPermissions + handlerGroupPermissions
        val requiredRoles: List<Role> = (handlerGroupRoles + handlerRoles).map { roleName ->
            securityConfig.roles.find { role -> role.name == roleName }
                ?: throw RuntimeException("Role not found: $roleName")
        }

        return HandlerSecurity(
            accessControl = accessControl,
            securityConfig = securityConfig,
            messageSource = messageSource,
            requiredPermissions = requiredPermissions,
            requiredRoles = requiredRoles,
            condition = handlerSecurity?.condition ?: handlerGroupSecurity?.condition ?: AccessControlCondition.ALL
        )
    }
}
