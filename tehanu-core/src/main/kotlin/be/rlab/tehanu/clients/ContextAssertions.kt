package be.rlab.tehanu.clients


/** Validates that the specified value is not null.
 * @param value Value to validate.
 * @param answerIfNull Text to answer if the value is null.
 */
fun<T : Any> UpdateContext.requireNotNull(
    value: T?,
    answerIfNull: String
): T = requireNotNull(value) {
    answer(answerIfNull)
}

/** Validates that the specified collection is not empty.
 * @param value Collection to validate.
 * @param answerIfEmpty Text to answer if the collection is empty.
 * @return the collection, only if it's not empty.
 */
fun<T> UpdateContext.requireNotEmpty(
    value: Collection<T>,
    answerIfEmpty: String
): Collection<T> {
    require(value.isNotEmpty()) {
        answer(answerIfEmpty)
    }
    return value
}

/** Validates that the specified list is not empty.
 * @param value List to validate.
 * @param answerIfEmpty Text to answer if the list is empty.
 * @return the list, only if it's not empty.
 */
fun<T> UpdateContext.requireNotEmpty(
    value: List<T>,
    answerIfEmpty: String
): List<T> {
    require(value.isNotEmpty()) {
        answer(answerIfEmpty)
    }
    return value
}
