package be.rlab.tehanu.clients

import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.view.UserInput
import java.util.*

/** Represents the state of a client [Update].
 *
 * The state machine moves forward until there is no transition, and then it returns backward to
 * the previous states. It allows to chain [UpdateHandler]s in order to build a graph of bot actions.
 *
 * The _initial state_ is created the first time Tehanu resolves an [UpdateHandler]
 * for an [Update]. The state will be alive until the state machine reaches a [final] state.
 *
 * A _final state_ is a State with the [previous] field set to null.
 */
class State(
    /** Unique identifier for the state. */
    val id: UUID,
    /** Chat this state belongs to, if any. */
    var chat: Chat?,
    /** User this state belongs to, if any. */
    var user: User?,
    /** Message handler that represents the state-transition function of this state. */
    val handlerName: String,
    /** Translations for this state. */
    val translations: MessageSource,
    /** Previous state. */
    val previous: State?,
    /** Callback invoked to go back to the previous state. */
    val continueCallback: () -> Unit,
    /** Indicates whether this is an initial state. */
    val initial: Boolean
) {
    companion object {
        /** Creates an initial state.
         *
         * @param chat Chat this state belongs to, if any.
         * @param user User this state belongs to, if any.
         * @param handlerName Name of the handler to execute the transition to this state.
         * @param translations Translations for this state.
         */
        fun initial(
            chat: Chat?,
            user: User?,
            handlerName: String,
            translations: MessageSource
        ): State = State(
            id = UUID.randomUUID(),
            chat,
            user,
            handlerName = handlerName,
            translations = translations,
            previous = null,
            continueCallback = {},
            initial = true
        )
    }

    /** List of all [UserInput]s in this state. */
    private val inputs: MutableList<UserInput> = mutableListOf()
    /** Client-defined attributes. */
    private val attributes: MutableMap<String, Any> = mutableMapOf()
    /** Active user input. */
    var currentInput: UserInput? = null
    /** Indicates whether this state is a _final state_ or not. */
    val final: Boolean get() = previous == null

    /** Adds a UserInput to this State.
     * @param userInput UserInput to add.
     */
    fun addUserInput(userInput: UserInput): State = apply {
        if (!inputs.contains(userInput)) {
            inputs += userInput
        }
    }

    /** Indicates whether this state has more [UserInput]s to render.
     * @return true if there are more [UserInput]s, false otherwise.
     */
    fun hasMoreInputs(): Boolean {
        return currentInput
            ?.let { inputs.indexOf(currentInput) < inputs.size - 1 }
            ?: inputs.isNotEmpty()
    }

    /** Returns the next [UserInput] to render.
     * @return the next [UserInput], if any.
     */
    fun nextUserInput(): UserInput {
        require(hasMoreInputs()) { "no more user inputs available" }
        currentInput = inputs[inputs.indexOf(currentInput) + 1]
        return currentInput!!
    }

    /** Finds the UserInput targeted by the specified Update.
     * @param update Update related to the required UserInput.
     * @return the UserInput, or null if the update is not intended for this State.
     */
    fun findUserInput(update: Update): UserInput? {
        return if (waitingForInput()) {
            return inputs.find { input ->
                input.canHandle(update.action) || input.waitingForMessage()
            } ?: currentInput
        } else {
            null
        }
    }

    /** Makes a transition to another state.
     *
     * @param handlerName Name of the handler to transition to.
     * @param continueCallback Callback to go back to this state.
     */
    fun next(
        handlerName: String,
        continueCallback: () -> Unit
    ): State {
        return State(
            id = UUID.randomUUID(),
            chat = chat,
            user = user,
            handlerName = handlerName,
            translations = translations,
            previous = this,
            continueCallback = continueCallback,
            initial = false
        )
    }

    /** Indicates whether this State is expecting some User interaction.
     * @return true if any of the [UserInput]s are waiting for an action or for a message.
     */
    fun waitingForInput(): Boolean {
        return inputs.any { input ->
            input.waitingForAction() || input.waitingForMessage()
        }
    }

    /** Updates the chat and user.
     * @param newChat The new chat.
     * @param newUser The new user.
     */
    fun updateIfRequired(
        newChat: Chat?,
        newUser: User?
    ): State = apply {
        chat = newChat ?: chat
        user = newUser ?: user
    }

    /** Goes back to the previous state.
     * It triggers the continue callback.
     */
    fun back(): State {
        require(!final) { "the state is not final" }
        continueCallback()
        return previous!!
    }

    /** Determines whether this State is targeted by the Update.
     *
     * If the Update has an Action, it checks if there is an active UserInput for the Action.
     *
     * Otherwise, if there is a UserInput in progress, it checks if the Update is targeted to
     * this State's user and chat.
     *
     * @param update Update to verify.
     * @return true if this State is targeted by the Update, false otherwise.
     */
    fun applies(update: Update): Boolean {
        val isTarget = update.chat == chat && update.user == user
        return isTarget && inputs.any { input ->
            input.canHandle(update.action) || input.waitingForMessage()
        }
    }

    fun setAttribute(name: String, value: Any): State = apply {
        attributes[name] = value
    }

    @Suppress("UNCHECKED_CAST")
    fun<T : Any> getAttribute(name: String): T? {
        return attributes[name] as T?
    }

    fun hasAttribute(name: String): Boolean {
        return attributes.containsKey(name)
    }
}
