package be.rlab.tehanu.clients

import be.rlab.tehanu.view.Transition
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

class StateManager {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(StateManager::class.java)
    }

    /** List of active states. */
    internal val activeStates: MutableList<State> = mutableListOf()

    /** Makes a transition from one [State] to another.
     * @param context Context that is triggering the transition to another state.
     * @param transition Transition to process.
     * @param execCallback Transition function, it makes the actual transition from the current state to the next state.
     * @param continueCallback Callback invoked when the new State finishes.
     */
    fun transitionTo(
        context: UpdateContext,
        transition: Transition,
        execCallback: (UpdateContext) -> State,
        continueCallback: () -> Unit = {}
    ): State {
        val currentState: State = findStateById(context.state.id)
        val nextState: State = currentState.next(transition.handlerName, continueCallback)
        activeStates.add(0, nextState)

        // We keep the parameters from previous handlers for convenience.
        val mergedParams: Map<String, Any> = context.params + transition.params
        return execCallback(UpdateContext.new(context.client, nextState, context.update, mergedParams))
    }

    /** Follows all transitions to a set of handlers in order.
     *
     * Once all transitions return, it invokes the [continueCallback] callback.
     *
     * @param context Current execution context.
     * @param transitions Transitions to process.
     * @param execCallback Transition function, it makes the actual transition from the current state to the next state.
     * @param continueCallback Callback invoked when the new State finishes.
     */
    fun processTransitions(
        context: UpdateContext,
        transitions: Iterator<Transition>,
        execCallback: (UpdateContext) -> State,
        continueCallback: () -> State
    ): State {
        return if (!transitions.hasNext()) {
            logger.debug("all transitions have been resolved")
            continueCallback()
        } else {
            transitionTo(context, transitions.next(), execCallback) {
                processTransitions(context, transitions, execCallback, continueCallback)
            }
        }
    }

    /** It determines which is the next state.
     *
     * This method will resolve the next state based on the following rules:
     *
     * - If the current state is waiting for user input, it keeps and returns the current state.
     * - If the current state is final (no transition required), it removes and returns the current state.
     * - If the current state is not final, it removes and returns the previous state.
     *
     * @param currentState Current state.
     */
    fun resolveNextState(currentState: State): State {
        val nextState: State = findStateById(currentState.id)

        return when {
            nextState.waitingForInput() -> nextState
            else -> {
                activeStates.remove(nextState)
                if (nextState.final) {
                    nextState
                } else {
                    nextState.back()
                }
            }
        }
    }

    /** Returns the [State] that is targeted by the specified [Update].
     * @param update Update to verify.
     * @return the targeted State, or null if no State applies.
     */
    fun findStateIfApplies(update: Update): State? {
        logger.debug("trying all states to check if applies for update: updateId=${update.updateId}")
        return activeStates.find { state -> state.applies(update) }
    }

    /** Finds a state by id.
     * @param stateId Id of the required state.
     * @return the required state.
     */
    fun findStateById(stateId: UUID): State {
        logger.debug("looking for state $stateId")
        return activeStates.single { state ->
            state.id == stateId
        }
    }

    /** Adds a new initial state for the specified update and handler.
     *
     * If there is a previous State, it is removed.
     *
     * @param update Update that triggered the new state.
     * @param handler Handler to manage this update.
     */
    fun addInitialState(
        update: Update,
        handler: UpdateHandler
    ): State {
        logger.debug("cancelling existing state if required, new handler matches: ${handler.name}")
        findStateIfApplies(update)?.let { state ->
            remove(state.id)
        }

        return State.initial(update.chat, update.user, handler.name, handler.translations).apply {
            activeStates.add(0, this)
        }
    }

    /** Adds or updates a state.
     *
     * @param id Id of the state to update.
     * @param nextState New state.
     */
    fun updateState(
        id: UUID,
        nextState: State
    ): State {
        logger.debug("updating state $id")

        activeStates.replaceAll { state ->
            if (state.id == id) {
                nextState
            } else {
                state
            }
        }

        return nextState
    }

    /** Removes a state by id.
     * @param id Id of the state to remove.
     */
    fun remove(id: UUID) {
        activeStates.removeIf { state ->
            state.id == id
        }
    }

    fun findByAttribute(name: String, value: Any): List<State> {
        return activeStates.filter { state ->
            state.getAttribute<Any>(name) == value
        }
    }
}
