package be.rlab.tehanu.clients.model

import java.util.*

/** Represents a chat in a messaging client.
 */
data class Chat(
    /** Unique identifier for this chat. */
    val id: UUID,
    /** Name of the messaging client this chat belongs to. */
    // TODO(seykron): this is hardcoded for backward compatibility. It should be
    // removed in the future when every project migrated to the latest version.
    val clientName: String = "TelegramClient",
    /** Client-specific identifier for this chat. */
    val clientId: Long,
    /** Chat type. */
    val type: ChatType,
    /** Chat title, if set. */
    val title: String?,
    /** Chat description, if set. */
    val description: String?
) {
    companion object {
        fun new(
            clientName: String,
            clientId: Long,
            type: ChatType,
            title: String?,
            description: String?
        ): Chat = Chat(
            id = UUID.randomUUID(),
            clientName = clientName,
            clientId = clientId,
            type = type,
            title = title,
            description = description
        )
    }

    override fun toString(): String =
        "$clientId/$title"
}
