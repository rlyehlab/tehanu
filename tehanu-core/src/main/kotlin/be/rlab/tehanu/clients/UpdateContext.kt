package be.rlab.tehanu.clients

import be.rlab.tehanu.BotServiceProvider
import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.media.MediaManager
import be.rlab.tehanu.media.model.MediaFile
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.messages.model.UserInputResponse
import be.rlab.tehanu.messages.model.text
import be.rlab.tehanu.messages.model.userInput
import be.rlab.tehanu.view.Transition
import be.rlab.tehanu.view.UserInput
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.InputStream
import java.util.*

/** Represents the current [State] for a specific [Update].
 * This is the public interface available in [be.rlab.tehanu.clients.UpdateHandler]s
 */
class UpdateContext(
    val client: Client,
    val state: State,
    val update: Update,
    val params: Map<String, Any>
) {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(UpdateContext::class.java)

        /** Creates a new context.
         *
         * @param client Client this context belongs to.
         * @param state Current state.
         * @param update Current update from client.
         * @param params Additional parameters passed to the underlying handler.
         */
        fun new(
            client: Client,
            state: State,
            update: Update,
            params: Map<String, Any>
        ): UpdateContext = UpdateContext(
            client = client,
            state = state,
            update = update,
            params = params
        )
    }

    val translations: MessageSource = state.translations
    val serviceProvider: BotServiceProvider = client.serviceProvider

    /** List of target handler names to transition after processing the Update. */
    internal val transitions: MutableList<Transition> = mutableListOf()

    fun hasChat(): Boolean {
        return state.chat != null
    }

    fun hasUser(): Boolean {
        return state.user != null
    }

    /** Context chat as lazy value.
     * If this context has no chat, it throws an [IllegalArgumentException].
     */
    val chat: Chat by lazy {
        requireNotNull(state.chat)
    }

    /** Context user as lazy value.
     * If this context has no user, it throws an [IllegalArgumentException].
     */
    val user: User by lazy {
        requireNotNull(state.user)
    }

    /** Current message as lazy value, if any.
     * If this context has no message, it throws an [IllegalArgumentException].
     */
    val incomingMessage: Message by lazy {
        requireNotNull(update.message)
    }

    /** UserInput related to the current Update, if any.
     */
    val currentInput: UserInput? get() = state.findUserInput(update)

    /** Talks to the channel.
     * @param text Text to send.
     * @param args Message arguments to expand with the underlying [MessageSource].
     * @param callback Callback to build the message to send.
     */
    fun talk(
        text: String,
        vararg args: String,
        callback: (MessageBuilder.() -> Unit)? = null
    ): UpdateContext = apply {
        sendMessage {
            text(text, *args)
            callback?.invoke(this)
        }
    }

    /** Talks to the user that sent the message on the channel.
     * @param text Message to send.
     * @param args Message arguments to expand with the underlying [MessageSource].
     * @param callback Callback to build the message to send.
     */
    fun answer(
        text: String,
        vararg args: String,
        callback: (MessageBuilder.() -> Unit)? = null
    ): UpdateContext = apply {
        sendMessage {
            text(text, *args)
            replyTo(incomingMessage)
            callback?.invoke(this)
        }
    }

    /** Talks to a specific chat.
     * @param chatId Id of the chat to send message to.
     * @param text Message to send.
     * @param args Message arguments to expand with the underlying [MessageSource].*
     * @param callback Callback to build the message to send.
     */
    fun talkTo(
        chatId: UUID,
        text: String,
        vararg args: String,
        callback: (MessageBuilder.() -> Unit)? = null
    ): UpdateContext = apply {
        sendMessage {
            text(text, *args)
            forChat(chatId)
            callback?.invoke(this)
        }
    }

    /** Talks in private to a specific user.
     * @param user User to talk to.
     * @param text Message to send.
     * @param args Message arguments to expand with the underlying [MessageSource].
     * @param callback Callback to build the message to send.
     */
    fun talkTo(
        user: User,
        text: String,
        vararg args: String,
        callback: (MessageBuilder.() -> Unit)? = null
    ): UpdateContext = apply {
        sendMessage {
            text(text, *args)
            forUser(user.id)
            callback?.invoke(this)
        }
    }

    /** Talks in private to a specific user.
     * @param user User to talk to.
     * @param callback Callback to build the message to send.
     */
    fun talkTo(
        user: User,
        callback: MessageBuilder.() -> Unit
    ): UpdateContext = apply {
        sendMessage {
            forUser(user.id)
            callback(this)
        }
    }

    /** Low-level send message operation.
     *
     * @param callback Callback to build the message to send.
     * @return the created message(s).
     */
    fun sendMessage(callback: MessageBuilder.() -> Unit): List<Message> {
        return sendMessage(newMessage().forChat(update.chat?.id).apply(callback))
    }

    /** Low-level send message operation.
     *
     * @param message Message to send.
     * @return the created message(s).
     */
    fun sendMessage(message: MessageBuilder): List<Message> {
        return if (message.response is UserInputResponse) {
            // UserInput processing is deferred until the end of the request.
            addUserInput(message.response.data as UserInput)
            emptyList()
        } else {
            client.sendMessage(this, message)
        }
    }

    /** Builds the interface to wait for user input.
     * @param description Message displayed to the user to describe the general purpose of this user input.
     */
    fun userInput(
        description: MessageBuilder? = null,
        callback: UserInput.() -> Unit
    ): UpdateContext = apply {
        logger.debug("creating user input response")
        sendMessage {
            userInput(description?.forChat(update.chat?.id), callback)
        }
    }

    /** Adds a UserInput to the underlying State.
     * @param userInput UserInput to add.
     */
    fun addUserInput(userInput: UserInput): UpdateContext = apply {
        state.addUserInput(userInput)
    }

    /** Reads the content of a media file.
     */
    fun readContent(mediaFile: MediaFile): InputStream {
        return client.serviceProvider.getService(MediaManager::class).readContent(mediaFile)
    }

    /** Low-level operation to create a new message builder.
     *
     * @param callback Callback to build the message.
     * @return the message builder.
     */
    fun newMessage(callback: MessageBuilder.() -> Unit): MessageBuilder {
        return newMessage().forChat(update.chat?.id).apply(callback)
    }

    fun newMessage(): MessageBuilder {
        return MessageBuilder()
    }

    /** Forces a [Transition] to another [State].
     * All transitions will be executed in order before proceeding with any user interaction.
     * @param handlerName Name of the handler to transition to.
     * @param params Parameters passed to the target handler.
     */
    fun transitionTo(handlerName: String, params: Map<String, Any> = emptyMap()): UpdateContext = apply {
        transitions += Transition(handlerName, params)
    }

    override fun toString(): String = "${state.user} on ${state.chat}"
}
