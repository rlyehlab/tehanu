package be.rlab.tehanu.clients

import be.rlab.tehanu.clients.model.Action
import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.messages.model.Message

/** Represents an update in a chat.
 *
 * The update is the root entity for any kind of activity notified by a [Client] in a chat.
 * The [Client] is responsible for building an update each time it receives an event.
 *
 * This update currently supports the following features depending on the client:
 *
 *   - All supported [Message]s.
 *   - Message edit events.
 *   - User interaction with a message ([Action]s)
 *
 * Though this update is used by all clients, the semantics are similar to Telegram API since
 * it was the first implementation.
 */
data class Update(
    /** Unique id for this update. */
    val updateId: Long,
    /** User that triggered the update. It is optional since some kind of chats like
     * Telegram channels do not provide user information.
     */
    val user: User?,
    /** Chat this update belongs to. */
    val chat: Chat?,
    /** Message related to the update, if any. */
    val message: Message?,
    /** Information about user input on the message. */
    val action: Action?,
    /** Client-specific data, if any. */
    val attributes: Map<String, Any>
) {
    companion object {
        fun new(
            updateId: Long,
            chat: Chat? = null,
            user: User? = null,
            message: Message? = null,
            action: Action? = null,
            attributes: Map<String, Any> = mapOf()
        ): Update = Update(
            updateId = updateId,
            chat = chat,
            user = user,
            message = message,
            action = action,
            attributes = attributes
        )
    }

    fun withAction(action: Action?): Update = copy(
        action = action
    )
}
