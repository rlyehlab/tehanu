package be.rlab.tehanu.clients.model

data class User(
    val id: Long,
    val userName: String?,
    val firstName: String?,
    val lastName: String?
) {
    companion object {
        fun new(
            id: Long,
            userName: String
        ): User = User(
            id = id,
            userName = userName,
            firstName = null,
            lastName = null
        )
    }

    override fun toString(): String = "$id/$userName"
}
