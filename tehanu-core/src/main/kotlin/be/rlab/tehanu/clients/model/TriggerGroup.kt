package be.rlab.tehanu.clients.model

import be.rlab.tehanu.clients.Trigger

/** Represents a list of [Trigger]s that must be evaluated with a [TriggerCondition] criteria.
 */
data class TriggerGroup(
    /** Condition to evaluate the list of triggers. */
    val condition: TriggerCondition,
    /** List of triggers to evaluate. */
    val triggers: List<Trigger>
)
