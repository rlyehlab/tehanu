package be.rlab.tehanu.clients.model

/** Conditions that indicate how to evaluate a [TriggerGroup].
 */
enum class TriggerCondition {
    /** All triggers in a [TriggerGroup] must match. */
    ALL,
    /** At least one trigger in a [TriggerGroup] must match. */
    ANY
}