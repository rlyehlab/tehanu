package be.rlab.tehanu.clients.model

import be.rlab.tehanu.clients.Client
import be.rlab.tehanu.view.UserInput

/** Represents a user action on a message.
 *
 * Actions are client-specific and they are represented by the [UserInput] class. Each [Client] must be able to define
 * custom data for specific actions in order to identify the action when the user interacts with a message.
 *
 * Some clients like Telegram requires to acknowledge an action. The [Client.replyAction] method is for that purpose.
 * Clients that don't need acknowledge must not throw an exception.
 */
data class Action(
    /** Unique id for this action assigned by a [Client]. */
    val id: String,
    /** Action-specific data assigned by a [Client]. */
    val data: String
)
