package be.rlab.tehanu.clients

import be.rlab.tehanu.AccessDeniedException
import be.rlab.tehanu.InvalidScopeException
import be.rlab.tehanu.clients.model.Action
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.text
import be.rlab.tehanu.view.Transition
import be.rlab.tehanu.view.UserInput
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/** The Update Dispatcher takes [Update]s from [Client]s and delegates the Updates to the resolved
 * [UpdateHandler].
 */
class UpdateDispatcher(
    private val stateManager: StateManager
) {

    private val handlers: MutableList<UpdateHandler> = mutableListOf()

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(UpdateDispatcher::class.java)
    }

    /** Delegates an [Update] to the resolved [UpdateHandler].
     *
     * It resolves the underlying state for the update, if any.
     *
     * @param client Client the update comes from.
     * @param update Update to handle.
     * @return the last state.
     */
    fun dispatch(
        client: Client,
        update: Update
    ): State? {
        logger.debug("resolving state for update ${update.updateId}")
        val handler: UpdateHandler? = handlers.find { handler -> handler.applies(update) }

        // if a new handler matches, it removes the existing state and starts
        // a new workflow for the matching handler.
        val state: State? = if (handler != null) {
            logger.debug("creating new state for handler ${handler.name}")
            stateManager.addInitialState(update, handler)
        } else {
            logger.debug("searching for existing state")
            stateManager.findStateIfApplies(update)
        }

        return state?.let {
            val resolvedUpdate = updateTargetActionIfRequired(state, update)
            val context = UpdateContext.new(client, state, resolvedUpdate, emptyMap())

            when {
                state.waitingForInput() -> processInput(context)
                else -> exec(context)
            }
        }
    }

    /** Adds handlers only if there is no handler with the same name in the context.
     * @param candidateHandlers List of handlers to add.
     */
    fun addHandlers(candidateHandlers: List<UpdateHandler>): UpdateDispatcher = apply {
        logger.debug("adding new handlers")
        candidateHandlers
            .filter { candidateHandler -> findHandlerByName(candidateHandler.name) == null  }
            .forEach { candidateHandler ->
                logger.debug("registering handler ${candidateHandler.name}")
                handlers += candidateHandler
            }
    }

    /** Gets a message handler.
     * @param handlerName Name of the required handler.
     * @return the required handler.
     */
    fun findHandlerByName(handlerName: String): UpdateHandler? {
        logger.debug("looking for handler $handlerName")
        return handlers.find { handlerDefinition ->
            handlerDefinition.name == handlerName
        }
    }

    /** Makes a transition to the specific [State].
     * @param transition Name of the target handler.
     * @param continueCallback Callback invoked when the new State finishes.
     */
    fun transitionTo(
        context: UpdateContext,
        transition: Transition,
        continueCallback: () -> Unit = {}
    ): State {
        logger.info("transitioning to another State: from=${context.state.handlerName},to=${transition.handlerName}")
        return stateManager.transitionTo(context, transition, this::exec, continueCallback)
    }

    /** Executes the transition to the specified state.
     *
     * It will follow transitions recursively until a handler result does not require a transition.
     *
     * @param context Current context.
     * @return the context from the last transition.
     */
    private fun exec(context: UpdateContext): State {
        val client: Client = context.client
        val update: Update = context.update
        val handler: UpdateHandler = findHandlerByName(context.state.handlerName)
            ?: throw RuntimeException("Handler '${context.state.handlerName}' not found")

        logger.debug("Executing handler ${handler.name} on chat ${update.chat} by user ${update.user}")

        try {
            handler.exec(context)
        } catch (cause: InvalidScopeException) {
            client.sendMessage(context, MessageBuilder().text(cause.message).forChat(update.chat?.id))
        } catch (cause: AccessDeniedException) {
            client.sendMessage(context, MessageBuilder().text(cause.message).forChat(update.chat?.id))
        }

        return resolveNextState(context)
    }

    /** If the state is waiting for user input, it reports a new update to
     * process it as user input.
     *
     * If the input triggers a transition, this method will follow the transition
     * recursively until the input processing does not require a new transition.
     *
     * @param context Current context.
     * @return returns the last transition state.
     */
    private fun processInput(context: UpdateContext): State {
        logger.debug("processing user input for update ${context.update.updateId}")
        val userInput = context.currentInput
            ?: throw RuntimeException("UserInput not found for update: update=${context.update}")
        userInput.processInput(context)
        return resolveNextState(context)
    }

    private fun updateTargetActionIfRequired(state: State, update: Update): Update {
        val input = state.findUserInput(update)
        return input?.resolveControl(update)?.let { targetControl ->
            update.withAction(
                Action(
                    id = update.action?.id ?: System.currentTimeMillis().toString(),
                    data = targetControl.id.toString()
                )
            )
        } ?: update
    }

    private fun resolveNextState(context: UpdateContext): State {
        return stateManager.processTransitions(context, context.transitions.iterator(), this::exec) {
            val current = context.state.currentInput
            val shouldRenderNext = context.state.hasMoreInputs() && (current == null || current.isCompleted())
            if (shouldRenderNext) {
                val userInput: UserInput = context.state.nextUserInput()
                logger.info("rendering user input: ${userInput.id}")
                userInput.render(context)
            }

            stateManager.resolveNextState(context.state)
        }
    }
}
