package be.rlab.tehanu.clients.model

enum class ChatType(val types: List<String>) {
    PRIVATE(listOf("private")),
    GROUP(listOf("group", "supergroup")),
    UNKNOWN(listOf("unknown"));

    companion object {
        fun from(type: String): ChatType {
            return values().find { chatType ->
                chatType.types.contains(type)
            } ?: UNKNOWN
        }
    }
}
