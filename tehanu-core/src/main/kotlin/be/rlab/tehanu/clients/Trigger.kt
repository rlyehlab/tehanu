package be.rlab.tehanu.clients

import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.support.TriggerSupport

/** Must be implemented in order to evaluate if a message can be handled by a
 * the underlying [be.rlab.tehanu.messages.UpdateHandler].
 *
 * Classes implementing this interface must define a companion object and it must extend the
 * [TriggerSupport] class in order to provide a static factory method. So each trigger class
 * is its own factory based on the [be.rlab.tehanu.annotations.Trigger] annotation configuration.
 *
 * A simple trigger class looks like this:
 *
 * ```
 * class MyTrigger(private val myParam: Int) : Trigger {
 *     companion object : TriggerSupport {
 *         override fun new(name: String, triggerConfig: TriggerAnnotation): Trigger {
 *             return MyTrigger(myParam = getParam(triggerConfig, "myParam")?.toInt() ?: 0)
 *         }
 *     }
 *
 *     override fun applies(
 *         chat: Chat,
 *         user: User?,
 *         message: Message,
 *         language: Language
 *    ): Boolean {
 *        // if this trigger matches for the specified chat, user, message and language,
 *        // based on the configuration.
 *        return if (myParam > 10) {
 *            true
 *        } else {
 *            false
 *        }
 *    }
 * }
 * ```
 */
interface Trigger {

    companion object {
        /** Name of the default trigger if no trigger is specified. */
        const val DEFAULT_TRIGGER: String = "default"
    }

    /** Trigger name used to bind triggers by name. */
    val name: String

    /** Determines whether this trigger applies for an update.
     *
     * @param update Update to evaluate.
     * @param language Language to evaluate.
     */
    fun applies(
        update: Update,
        language: Language
    ): Boolean
}
