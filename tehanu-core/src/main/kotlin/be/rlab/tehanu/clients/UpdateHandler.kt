package be.rlab.tehanu.clients

import be.rlab.tehanu.InvalidScopeException
import be.rlab.tehanu.acl.HandlerSecurity
import be.rlab.tehanu.annotations.Param
import be.rlab.tehanu.clients.model.*
import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.messages.model.Message
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KType
import kotlin.reflect.jvm.isAccessible

/** An update handler provides support to handle an [Update] from a [Client].
 *
 * It calls a function or a method and automatically binds know-types to parameters. Take a look
 * at the [exec] implementation to check supported parameter types.
 *
 * Handler functions must be annotated with the [be.rlab.tehanu.annotations.Handler] annotation.
 *
 * Handlers may have [Trigger]s to handle [Update]s conditionally. By default, if no [Trigger] is configured,
 * a Handler will be executed if a message starts with the Handler name.
 */
data class UpdateHandler(
    val target: Any,
    val handler: KFunction<*>,
    val name: String,
    val triggerGroup: TriggerGroup?,
    val scope: List<ChatType>,
    /** Default language, used as fallback if no other language is detected. */
    val defaultLanguage: Language,
    /** Message source to support messages internationalization. */
    val translations: MessageSource,
    val minParams: Int = 0,
    val maxParams: Int = 0,
    val params: List<Param>,
    val security: HandlerSecurity
) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(UpdateHandler::class.java)
    }

    init {
        handler.isAccessible = true
    }

    fun applies(update: Update): Boolean {
        logger.debug("evaluating triggers")

        val validTriggers: Boolean = triggerGroup?.let {
            val filter: ((Trigger) -> Boolean) -> Boolean = when(triggerGroup.condition) {
                TriggerCondition.ALL -> triggerGroup.triggers::all
                TriggerCondition.ANY -> triggerGroup.triggers::any
            }
            filter { trigger ->
                val applies: Boolean = trigger.applies(update, defaultLanguage)

                logger.debug("trigger '${trigger.name}' applies: $applies")

                applies
            }
        } ?: true

        return validTriggers
    }

    fun exec(context: UpdateContext) {
        checkPermissions(context)
        checkScope(context)

        val args: List<Any> = handler.parameters.map { parameter ->
            when {
                isAssignableFrom(parameter.type, target::class) -> target
                isAssignableFrom(parameter.type, UpdateContext::class) -> context
                isAssignableFrom(parameter.type, Message::class) -> context.incomingMessage
                isAssignableFrom(parameter.type, User::class) -> context.user
                isAssignableFrom(parameter.type, Chat::class) -> context.chat
                isAssignableFrom(parameter.type, Update::class) -> context.update
                isAssignableFrom(parameter.type, State::class) -> context.state
                isAssignableFrom(parameter.type, Client::class) -> context.client
                isAssignableFrom(parameter.type, MessageSource::class) -> context.translations
                else -> context.serviceProvider.getService(parameter.type.classifier as KClass<*>)
            }
        }
        handler.call(*args.toTypedArray())
    }

    private fun checkScope(context: UpdateContext) {
        if (!context.hasChat() || !context.hasUser()) {
            return
        }

        val chat: Chat = requireNotNull(context.chat)
        val validScope = scope.isEmpty() || scope.contains(chat.type)

        logger.debug("handler $name scopes: $scope")

        if (!validScope) {
            logger.debug("invalid scope for handler $name: ${chat.type}")

            when (chat.type) {
                ChatType.PRIVATE ->
                    throw InvalidScopeException(translations["handler-invalid-scope-private"])
                ChatType.GROUP ->
                    throw InvalidScopeException(translations["handler-invalid-scope-group"])
                else ->
                    throw InvalidScopeException(translations["handler-invalid-scope-any"])
            }
        }
    }

    private fun checkPermissions(context: UpdateContext) {
        if (context.hasChat() && context.hasUser()) {
            security.checkPermissions(requireNotNull(context.chat), context.user)
        }
    }

    private fun isAssignableFrom(
        type: KType,
        klass: KClass<*>
    ): Boolean {
        val paramType: KClass<*> = type.classifier as KClass<*>
        return klass.java.isAssignableFrom(paramType.java)
    }
}
