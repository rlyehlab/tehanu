package be.rlab.tehanu.clients

import be.rlab.tehanu.BotServiceProvider
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.*
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.ui.Control
import java.util.*

/** This interface represents a messaging client.
 *
 * Messaging clients are responsible for handling updates from the client's upstream.
 *
 * There are a couple of limited operations in favour of interoperability between clients. This client interface
 * supports the following features:
 *
 *   - Handling updates from the upstream.
 *   - Sending messages with client-specific parameters.
 *   - Editing messages with client-specific parameters.
 *   - Sending location messages.
 *   - Handling and replying [UserInput].
 */
interface Client {

    /** Client name. It must be unique in the context.
     */
    val name: String

    /** Provides access to bot internal components. */
    val serviceProvider: BotServiceProvider

    /** Initializes this client and injects the Update Dispatcher.
     * @param tehanu Bot instance.
     */
    fun start(): Any

    /** Sends a message to a chat.
     * @param context Current context.
     * @param message Builder with the information to create the message.
     * @return the created message(s).
     */
    fun sendMessage(context: UpdateContext, message: MessageBuilder): List<Message>

    /** Edits an existing message on a chat.
     *
     * The chat must exist in the [AccessControl] database.
     *
     * @param chatId Id of the chat the message belongs to.
     * @param messageId Id of the message to edit.
     * @param newMessage Builder with the information to create the message.
     * @return the edited message.
     */
    fun editMessage(
        chatId: UUID,
        messageId: Long,
        newMessage: MessageBuilder
    ): Message

    /** Renders a single control.
     *
     * If [messageId] is not specified, it renders the control in a new [Message],
     * otherwise it edits the existing message.
     *
     * @param context Current update context.
     * @param control Control to render.
     * @param messageId Message to attach this control to.
     */
    fun renderControl(
        context: UpdateContext,
        control: Control,
        messageId: Long?
    )
}
