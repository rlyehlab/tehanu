package be.rlab.tehanu

/** This interface provides the lifecycle for a standalone bot application.
 *
 * When [start] is called, it initializes the application context. In this phase the
 * application context is being created but it's still unavailable.
 *
 * After initialization, the [ready] method is called just before the bot starts
 * listening for messages.
 */
interface BotApplication {
    /** Invoked to initialize data and components.
     */
    fun initialize()

    /** Invoked once all data and components are available.
     */
    fun ready()

    /** Must be called to start the standalone application.
     */
    fun start()
}
