package be.rlab.tehanu.annotations

/** Marker annotation to indicate a class contains [Handler]s.
 */
@Target(AnnotationTarget.CLASS)
annotation class HandlerGroup
