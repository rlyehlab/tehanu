package be.rlab.tehanu.annotations

import be.rlab.tehanu.messages.TextTrigger
import kotlin.reflect.KClass
import be.rlab.tehanu.clients.Trigger as TriggerClass

/** This annotation configures a rule on how Tehanu will delegate a [be.rlab.tehanu.messages.model.Message]
 * to a [be.rlab.tehanu.messages.UpdateHandler].
 *
 * It is possible to define custom triggers by implementing the [be.rlab.tehanu.messages.Trigger] interface.
 * If no custom trigger is provided, by default it uses a convenient [TextTrigger] to match different parts
 * of the message.
 *
 * Triggers can be defined either at object or [Handler] level. [Handler]s inherit object-level
 * triggers but own triggers have precedence over object-level triggers.
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class Trigger(
    /** Trigger class. */
    val type: KClass<out TriggerClass> = TextTrigger::class,
    /** Parameters required to build this trigger instance. */
    vararg val params: TriggerParam
)
