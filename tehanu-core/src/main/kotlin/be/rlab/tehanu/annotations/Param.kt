package be.rlab.tehanu.annotations

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class Param(
    val name: String,
    val args: Int = 0,
    val required: Boolean = false
)
