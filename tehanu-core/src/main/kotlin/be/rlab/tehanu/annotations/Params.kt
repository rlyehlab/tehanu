package be.rlab.tehanu.annotations

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class Params(
    vararg val params: Param
)
