package be.rlab.tehanu.annotations

@Target(AnnotationTarget.CLASS)
annotation class TriggerParam(
    val name: String,
    vararg val value: String
)
