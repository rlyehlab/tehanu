package be.rlab.tehanu.annotations

import be.rlab.tehanu.acl.model.AccessControlCondition

/** Configures the access control to execute handlers.
 * If defined at object level, it applies to all handlers in cascade. Otherwise it applies to specific handlers.
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class Security(
    /** List of required permissions. */
    val requiredPermissions: Array<String> = [],
    /** List of required roles. */
    val requiredRoles: Array<String> = [],
    /** Indicates how to evaluate the required roles and permissions. */
    val condition: AccessControlCondition = AccessControlCondition.ALL
)
