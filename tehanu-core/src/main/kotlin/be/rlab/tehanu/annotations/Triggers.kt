package be.rlab.tehanu.annotations

import be.rlab.tehanu.clients.model.TriggerCondition

/** Must be used on a class or a function to define a group of [Trigger]s.
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class Triggers(
    vararg val triggers: Trigger,
    val condition: TriggerCondition = TriggerCondition.ALL
)
