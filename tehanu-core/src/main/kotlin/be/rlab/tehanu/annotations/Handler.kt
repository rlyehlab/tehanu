package be.rlab.tehanu.annotations

import be.rlab.tehanu.clients.model.ChatType

/** This annotation marks a function to handle Updates from Clients.
 *
 * By default, if no [Trigger] is configured, it will handle messages that starts with the handler name.
 *
 * The incoming message must match ALL conditions configured in this annotation in order to trigger
 * the handler. All custom triggers must also match in order to fulfill the condition.
 */
@Target(AnnotationTarget.FUNCTION)
annotation class Handler(
    /** Handler name. */
    val name: String,
    /** Required scope. By default it supports all scopes. */
    val scope: Array<ChatType> = [],
    /** Minimum number of parameters in the message, by default there is no min. */
    val minParams: Int = 0,
    /** Maximum number of parameters in the message, by default there is no max. */
    val maxParams: Int = 0
)
