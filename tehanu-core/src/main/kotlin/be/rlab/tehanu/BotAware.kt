package be.rlab.tehanu

/** Must be implemented in order to inject Tehanu into a component.
 */
interface BotAware {
    var tehanu: Tehanu
}