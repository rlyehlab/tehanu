package be.rlab.tehanu.messages.model

/** Operators to indicate how to match terms in a [be.rlab.tehanu.messages.TextTrigger].
 */
enum class TextTriggerOperator {
    /** Must match all terms. */
    ALL,
    /** Must match at least one term. */
    ANY,
    /** Must not match any term. */
    NONE
}
