package be.rlab.tehanu.messages.model

import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.UserInput

/** Builds the interface to wait for user input.
 * @param description Message displayed to the user to describe the general purpose of this user input.
 * @param callback Callback to customize the [UserInput].
 */
fun MessageBuilder.userInput(
    description: MessageBuilder? = null,
    callback: UserInput.() -> Unit
): MessageBuilder = apply {
    custom(UserInputResponse(
        data = UserInput.new(description).apply(callback)
    ))
}

/** Represents a plain text response.
 */
data class UserInputResponse(
    override val data: UserInput
) : Response<UserInput> {
    companion object {
        const val TYPE: String = "userInput"
    }
    override val type: String = TYPE
}
