package be.rlab.tehanu.messages.model

/** Represents a chat event, like when a user joined, left, or topic is changed.
 */
data class ChatEvent (
    /** Client-specific event name. */
    val name: String,
    /** Client-specific event data. */
    val data: Any? = null
) {
    /** Casts the event data to the required type.
     * @return the event data.
     */
    @Suppress("UNCHECKED_CAST")
    fun<T> data(): T {
        return data as T
    }
}
