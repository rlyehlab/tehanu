package be.rlab.tehanu.messages

import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.messages.model.Response
import java.util.*

/** Core component to build [be.rlab.tehanu.messages.model.Message]s.
 */
class MessageBuilder {
    var replyTo: Message? = null
        private set
    lateinit var response: Response<*>
        private set
    var chatId: UUID? = null
        private set
    var userId: Long? = null
        private set
    private val options: MutableMap<String, Any?> = mutableMapOf()

    fun forChat(id: UUID?): MessageBuilder = apply {
        chatId = id
    }

    fun forUser(id: Long?): MessageBuilder = apply {
        userId = id
    }

    fun replyTo(message: Message): MessageBuilder = apply {
        replyTo = message
    }

    /** Sends a custom response.
     * @param response Response to send.
     * @return this builder.
     */
    fun<T> custom(response: Response<T>): MessageBuilder {
        this.response = response
        return this
    }

    /** Adds custom options for the response.
     * @param newOptions Custom options.
     * @return this builder.
     */
    fun options(vararg newOptions: Pair<String, Any?>): MessageBuilder = apply {
        options.putAll(newOptions)
    }

    /** Returns the value of a custom option, if it does exist.
     * @param name Required option name.
     * @return the option's value, or null if it doesn't exist.
     */
    @Suppress("UNCHECKED_CAST")
    fun<T> option(name: String): T? {
        return options[name] as T
    }

    override fun toString(): String {
        return "replyTo=${replyTo?.messageId ?: "N/A"},chatId=${chatId ?: "N/A"},userId=${userId ?: "N/A"}" +
                ",response=${response}"
    }
}
