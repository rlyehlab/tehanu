package be.rlab.tehanu.messages.model

import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.messages.MessageBuilder

/** Sends a text message.
 * @param message Text message to send.
 * @param args Message arguments to expand with the underlying [MessageSource].
 */
fun MessageBuilder.text(
    message: String,
    vararg args: String
): MessageBuilder = apply {
    custom(TextResponse(message, args.toList()))
}

/** Represents a plain text response.
 */
data class TextResponse(
    override val data: String,
    val args: List<String>,
    val entities: List<MessageEntity> = emptyList()
) : Response<String> {
    companion object {
        const val TYPE: String = "text"
    }
    override val type: String = TYPE
}
