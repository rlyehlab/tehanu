package be.rlab.tehanu.messages

import be.rlab.tehanu.clients.Trigger
import be.rlab.tehanu.clients.Update
import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.support.TriggerSupport
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import be.rlab.tehanu.annotations.Trigger as TriggerAnnotation

/** Triggers handlers based on the message information.
 */
class MessageContentTrigger(
    override val name: String,
    /** The message has text. */
    private val hasText: Boolean,
    /** The message has media files. */
    private val hasMedia: Boolean,
    /** The message has a chat event. */
    private val hasChatEvent: Boolean
) : Trigger {

    companion object : TriggerSupport() {
        private val logger: Logger = LoggerFactory.getLogger(MessageContentTrigger::class.java)
        const val HAS_TEXT: String = "hasText"
        const val HAS_LOCATION: String = "hasLocation"
        const val HAS_MEDIA: String = "hasMedia"
        const val HAS_CHAT_EVENT: String = "hasChatEvent"

        override fun new(
            name: String,
            triggerConfig: TriggerAnnotation
        ): Trigger {
            return MessageContentTrigger(
                name = name,
                hasText = hasParam(triggerConfig, HAS_TEXT),
                hasMedia = hasParam(triggerConfig, HAS_MEDIA),
                hasChatEvent = hasParam(triggerConfig, HAS_CHAT_EVENT)
            )
        }
    }

    override fun applies(
        update: Update,
        language: Language
    ): Boolean {
        logger.debug("matching message content")
        return if (update.message != null) {
            return (hasText && update.message.hasText())
                || (hasMedia && update.message.hasMedia())
                || (hasChatEvent && update.message.hasChatEvent())
        } else {
            false
        }
    }
}
