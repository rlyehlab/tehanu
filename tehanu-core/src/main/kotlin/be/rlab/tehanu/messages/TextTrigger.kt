package be.rlab.tehanu.messages

import be.rlab.nlp.Distance
import be.rlab.nlp.Normalizer
import be.rlab.tehanu.clients.Trigger
import be.rlab.tehanu.clients.Update
import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.messages.model.EntityType
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.messages.model.MessageEntity
import be.rlab.tehanu.messages.model.TextTriggerOperator
import be.rlab.tehanu.support.TriggerSupport
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import be.rlab.tehanu.annotations.Trigger as TriggerAnnotation

/** Trigger that evaluates different parts of a message.
 */
class TextTrigger(
    override val name: String,
    /** The message must contain any of this elements. */
    private val contains: List<String> = emptyList(),
    /** The message must start with a string. */
    private val startsWith: String?,
    /** The message must end with a string. */
    private val endsWith: String?,
    /** The message must match a regex. */
    private val regex: Regex?,
    /** Distance between the input text and any of the terms in the [contains] field. */
    private val distance: Float = -1.0F,
    /** Ignore case on matching. */
    private val ignoreCase: Boolean,
    /** Indicates whether to normalize the input using a [Normalizer]. */
    private val normalize: Boolean,
    /** Indicates whether to stem the input if normalization is required. */
    private val stemming: Boolean,
    /** Indicates whether to remove all [be.rlab.tehanu.messages.model.MessageEntity]s before validation. */
    private val stripEntities: Boolean,
    /** Indicates how to match terms. */
    private val operator: TextTriggerOperator
) : Trigger {

    companion object : TriggerSupport() {
        private val logger: Logger = LoggerFactory.getLogger(TextTrigger::class.java)

        const val CONTAINS: String = "contains"
        const val STARTS_WITH: String = "starts-with"
        const val ENDS_WITH: String = "ends-with"
        const val REGEX: String = "regex"
        const val DISTANCE: String = "distance"
        const val IGNORE_CASE: String = "ignore-case"
        const val NORMALIZE: String = "normalize"
        const val STEMMING: String = "stemming"
        const val STRIP_ENTITIES: String = "strip-entities"
        const val OPERATOR: String = "operator"

        fun new(
            name: String,
            contains: List<String> = emptyList(),
            startsWith: String? = null,
            endsWith: String? = null,
            regex: Regex? = null,
            distance: Float = -1.0F,
            ignoreCase: Boolean = true,
            normalize: Boolean = false,
            stemming: Boolean = true,
            stripEntities: Boolean = false,
            operator: TextTriggerOperator = TextTriggerOperator.ANY
        ): TextTrigger =
            TextTrigger(
                name = name,
                contains = contains,
                startsWith = startsWith,
                endsWith = endsWith,
                regex = regex,
                distance = distance,
                ignoreCase = ignoreCase,
                normalize = normalize,
                stemming = stemming,
                stripEntities = stripEntities,
                operator = operator
            )

        override fun new(
            name: String,
            triggerConfig: TriggerAnnotation
        ): Trigger {
            val ignoreCase: Boolean = hasParam(triggerConfig, IGNORE_CASE)
            val regex: String? = getParam(triggerConfig, REGEX)

            return TextTrigger(
                name = name,
                startsWith = getParam(triggerConfig, STARTS_WITH),
                endsWith = getParam(triggerConfig, ENDS_WITH),
                contains = getParams(triggerConfig, CONTAINS).toList(),
                ignoreCase = ignoreCase,
                regex = if (ignoreCase) {
                    regex?.toRegex(RegexOption.IGNORE_CASE)
                } else {
                    regex?.toRegex()
                },
                distance = getParam(triggerConfig, DISTANCE)?.toFloat() ?: -1.0F,
                normalize = hasParam(triggerConfig, NORMALIZE),
                stemming = hasParam(triggerConfig, STEMMING),
                stripEntities = hasParam(triggerConfig, STRIP_ENTITIES),
                operator = getParam(triggerConfig, OPERATOR)?.let { operator ->
                    TextTriggerOperator.valueOf(operator.uppercase())
                } ?: TextTriggerOperator.ANY
            )
        }
    }

    override fun applies(
        update: Update,
        language: Language
    ): Boolean {
        return update.message != null
            && update.message.hasText()
            && applies(update.message, language)
    }

    private fun applies(
        message: Message,
        language: Language
    ): Boolean {
        logger.debug("evaluating text matches within the message")
        val transformPipeline: List<(String) -> String> = listOfNotNull(
            { value: String -> normalize(value, language) },
            { value: String -> stripEntities(value, message.entities) }
        )
        val validationPipeline: List<(String) -> Boolean> = listOf(
            { value: String -> startsWith(value) },
            { value: String -> endsWith(value) },
            { value: String -> matches(value) },
            { value: String -> contains(value, language) }
        )

        val text: String = transformPipeline.fold(message.text) { value, transformer ->
            transformer(value)
        }

        return validationPipeline.all { validator ->
            validator(text)
        }
    }

    private fun normalize(
        text: String,
        language: Language
    ): String {
        return if (normalize) {
            logger.debug("normalizing input (stemming: $stemming)")
            Normalizer(
                text = text,
                language = language.toLuceneLanguage(),
                stemming = stemming,
                removeStopWords = false
            ).normalize()
        } else {
            text
        }
    }

    private fun stripEntities(
        text: String,
        entities: List<MessageEntity>
    ): String {
        return if (stripEntities) {
            logger.debug("stripping message entities")

            entities.filter { entity ->
                entity.type != EntityType.RICH_TEXT
            }.fold(text) { cleanText, entity ->
                cleanText.replace(entity.value, "")
            }
        } else {
            text
        }
    }

    private fun startsWith(value: String): Boolean {
        return startsWith?.let {
            logger.debug("message must start with $startsWith (ignore case: $ignoreCase)")
            value.startsWith(startsWith, ignoreCase)
        } ?: true
    }

    private fun endsWith(value: String): Boolean {
        return endsWith?.let {
            logger.debug("message must end with $endsWith (ignore case: $ignoreCase)")
            value.endsWith(endsWith, ignoreCase)
        } ?: true
    }

    private fun matches(value: String): Boolean {
        return regex?.let {
            logger.debug("message must match the regex: $regex")
            regex.matches(value)
        } ?: true
    }

    private fun contains(
        value: String,
        language: Language
    ): Boolean {
        val expectedValues: List<String> = contains.map { expectedValue ->
            normalize(expectedValue, language)
        }
        val matcher: ((String) -> Boolean) -> Boolean = when(operator) {
            TextTriggerOperator.ALL -> expectedValues::all
            TextTriggerOperator.ANY -> expectedValues::any
            TextTriggerOperator.NONE -> expectedValues::none
        }
        return expectedValues.isEmpty() || matcher { other ->
            if (distance > 0) {
                Distance.jaroWinkler(value, other) >= distance
            } else {
                value.contains(other, ignoreCase)
            }
        }
    }
}
