package be.rlab.tehanu.messages.model

/** Represents a message response.
 *
 * Must be implemented by value objects in order to extend the [be.rlab.tehanu.messages.MessageBuilder] by using
 * the internal [be.rlab.tehanu.messages.MessageBuilder.custom] method.
 *
 * Message responses are client-specific, so each client is responsible of providing extensions and
 * sending this response.
 */
interface Response<T>{
    /** A client-specific response type. */
    val type: String
    /** Client-specific data. */
    val data: T
}
