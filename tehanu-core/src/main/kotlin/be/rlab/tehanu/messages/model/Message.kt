package be.rlab.tehanu.messages.model

import be.rlab.tehanu.media.model.MediaFile
import org.joda.time.DateTime

/** Represents a message sent to a [Chat].
 * A message can contain different type of contents like location, photos, videos, and rich text.
 */
data class Message(
    /** Client-specific identifier. */
    val messageId: Long,
    /*** Date and time when the message was sent. */
    val date: DateTime,
    /** Indicates whether the message is an edition. */
    val isEdit: Boolean,
    /** Indicates whether the message is a forward. */
    val isForward: Boolean,
    /** Message text, if any. */
    private val textInternal: String?,
    /** Message entities, if present. */
    val entities: List<MessageEntity>,
    /** If the message is a chat event, it contains the event information. */
    val event: ChatEvent?,
    /** List of media files included in this message. */
    val files: List<MediaFile>,
    /** Any other unclassified, client-specific content. */
    val content: Map<String, Any?>
) {

    companion object {
        fun new(
            messageId: Long,
            date: DateTime,
            isEdit: Boolean = false,
            isForward: Boolean = false,
            text: String? = null,
            entities: List<MessageEntity> = emptyList(),
            event: ChatEvent? = null,
            files: List<MediaFile> = emptyList(),
            content: Map<String, Any?> = emptyMap()
        ): Message = Message(
            messageId = messageId,
            date = date,
            isEdit = isEdit,
            isForward = isForward,
            textInternal = text,
            entities = entities,
            event = event,
            files = files,
            content = content
        )
    }

    /** Message text, if any.
     * Throws an exception if the message has no text.
     */
    val text: String by lazy {
        requireNotNull(textInternal)
    }

    fun hasText(): Boolean {
        return textInternal != null
    }

    fun hasMedia(): Boolean {
        return files.isNotEmpty()
    }

    fun hasChatEvent(): Boolean {
        return event != null
    }

    fun hasUnclassifiedContent(): Boolean {
        return content.isNotEmpty()
    }

    fun hasEntity(entityType: EntityType): Boolean {
        return entities.any { messageEntity ->
            messageEntity.type == entityType
        }
    }

    fun getEntity(entityType: EntityType): MessageEntity? {
        return entities.find { messageEntity ->
            messageEntity.type == entityType
        }
    }

    @Suppress("UNCHECKED_CAST")
    operator fun<T : Any> get(fieldName: String): T {
        require(content.containsKey(fieldName)) { "the content has no the required file: $fieldName" }
        return content.getValue(fieldName) as T
    }
}
