package be.rlab.tehanu.messages.model

data class MessageEntity(
    val type: EntityType,
    val value: String,
    val data: Any
)
