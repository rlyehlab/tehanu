package be.rlab.tehanu.messages.model

enum class EntityType(val type: String) {
    MENTION("mention"),
    HASHTAG("hashtag"),
    URL("url"),
    EMAIL("email"),
    PHONE_NUMBER("phone_number"),
    BOT_COMMAND("bot_command"),
    RICH_TEXT("text"),
    UNKNOWN("unknown");

    companion object {
        fun from(entityType: String): EntityType {
            return values().find { textEntityType ->
                textEntityType.type == entityType
            } ?: UNKNOWN
        }
    }
}
