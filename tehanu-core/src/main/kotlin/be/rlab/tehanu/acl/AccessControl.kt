package be.rlab.tehanu.acl

import be.rlab.tehanu.acl.model.Role
import be.rlab.tehanu.acl.model.UserRole
import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.ChatType
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.config.SecurityConfig
import be.rlab.tehanu.store.Memory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

class AccessControl(
    private val securityConfig: SecurityConfig,
    memory: Memory
) {

    companion object {
        const val USERS_SLOT: String = "users"
        private const val CHATS_SLOT: String = "chats"
        private const val CHATS_MEMBERS_SLOT: String = "chats_members"
        private const val USERS_ROLES_SLOT: String = "users_roles"
    }

    private val logger: Logger = LoggerFactory.getLogger(AccessControl::class.java)

    private var chats: List<Chat> by memory.slot(CHATS_SLOT, listOf<Chat>())
    private var chatsMembers: Map<UUID, List<Long>> by memory.slot(CHATS_MEMBERS_SLOT, emptyMap<UUID, List<Long>>())
    private var users: List<User> by memory.slot(USERS_SLOT, listOf<User>())
    private var userRoles: Map<Long, Set<UserRole>> by memory.slot(
        USERS_ROLES_SLOT, emptyMap<Long, Set<UserRole>>())

    fun findUserRole(
        chat: Chat,
        user: User
    ): UserRole? {
        return userRoles[user.id]?.find { userRole ->
            userRole.chatId == chat.id
        }
    }

    fun getAdminsIds(chat: Chat): List<Long> {
        return userRoles.filterValues { userRoles ->
            userRoles.any { userRole ->
                val isAdmin: Boolean = userRole.rolesNames.any { roleName ->
                    securityConfig.findRole(roleName)?.admin ?: false
                }

                userRole.chatId == chat.id && isAdmin
            }
        }.keys.toList()
    }

    fun assign(
        chat: Chat,
        user: User,
        rolesNames: Set<String>
    ) {
        logger.debug("Setting roles for user ${userInfo(chat, user)}: $rolesNames")

        val newUserRole: UserRole = UserRole.new(chat.id, rolesNames)
        val existingRoles: Set<UserRole> = userRoles[user.id]?.filter { userRole ->
            userRole.chatId != chat.id
        }?.toSet() ?: emptySet()

        userRoles = userRoles + (user.id to existingRoles + newUserRole)
    }

    fun assign(
        clientName: String,
        user: User,
        rolesNames: Set<String>
    ) {
        val chat: Chat = chats.find { chat ->
            chat.clientId == user.id
        } ?: addUserChat(clientName, user)

        assign(chat, user, rolesNames)
    }

    fun drop(
        chat: Chat,
        user: User
    ) {
        logger.debug("Removing roles from user ${userInfo(chat, user)}")

        val filteredRoles: Set<UserRole> = userRoles[user.id]?.filter { userRole ->
            userRole.chatId != chat.id
        }?.toSet() ?: emptySet()

        userRoles = userRoles + (
            user.id to filteredRoles
        )
    }

    fun drop(
        clientName: String,
        user: User
    ) {
        val chat: Chat = chats.find { chat ->
            chat.clientId == user.id
        } ?: addUserChat(clientName, user)

        drop(chat, user)
    }

    fun addChatIfRequired(chat: Chat): Chat {
        return findChat(chat.clientId) ?: addChat(chat)
    }

    fun addUserIfRequired(
        chat: Chat,
        user: User
    ): User {
        val resolvedUser: User = findUserById(user.id) ?: addUser(user)
        return addUserToChat(chat, resolvedUser)
    }

    fun addUserIfRequired(user: User): User {
        return findUserById(user.id) ?: addUser(user)
    }

    fun findChat(chatId: Long): Chat? {
        return chats.find { chat ->
            chat.clientId == chatId
        }
    }

    fun findChatById(id: UUID): Chat? {
        return chats.find { chat ->
            chat.id == id
        }
    }

    fun findUserByName(userName: String): User? {
        return users.find { user ->
            user.userName == userName
        }
    }

    fun findUserById(id: Long): User? {
        return users.find { user ->
            user.id == id
        }
    }

    fun listUsers(chatId: UUID): List<User> {
        val usersIds: List<Long> = chatsMembers[chatId] ?: emptyList()
        return usersIds.map { userId ->
            findUserById(userId)!!
        }
    }

    fun listChats(): List<Chat> =
        chats.toList()

    fun listRoles(): List<Role> =
        securityConfig.roles

    private fun addChat(newChat: Chat): Chat {
        logger.debug("Adding new chat $newChat")
        chats = chats + newChat
        return newChat
    }

    private fun addUser(newUser: User): User {
        logger.debug("Adding new user $newUser")
        users = users + newUser
        return newUser
    }

    private fun addUserChat(
        clientName: String,
        user: User
    ): Chat {
        val newChat = Chat.new(
            clientName = clientName,
            clientId = user.id,
            type = ChatType.PRIVATE,
            title = user.userName,
            description = "Chat with ${user.firstName} ${user.lastName}"
        )
        logger.debug("Adding new user chat $newChat")

        chats = chats + newChat

        return newChat
    }

    private fun addUserToChat(
        chat: Chat,
        user: User
    ): User {
        val members: List<Long> = chatsMembers.getOrDefault(chat.id, emptyList())

        if (!members.contains(user.id)) {
            chatsMembers = chatsMembers + (chat.id to members + user.id)
        }

        return user
    }

    private fun userInfo(
        chat: Chat,
        user: User
    ): String = "$user on chat $chat"
}
