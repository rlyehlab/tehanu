package be.rlab.tehanu.acl

import be.rlab.tehanu.AccessDeniedException
import be.rlab.tehanu.acl.model.AccessControlCondition
import be.rlab.tehanu.acl.model.Role
import be.rlab.tehanu.acl.model.UserRole
import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.config.SecurityConfig
import be.rlab.tehanu.i18n.MessageSource
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/** This class manages the security for a [be.rlab.tehanu.clients.UpdateHandler].
 *
 * It verifies user permissions and roles in a chat and fails if the user
 * does not fulfill the required conditions.
 *
 * The access control configuration is provided by the [be.rlab.tehanu.annotations.Security] annotation.
 * If used at object level, [be.rlab.tehanu.annotations.Handler]s inherit the security configuration from the
 * underlying object.
 */
class HandlerSecurity(
    private val accessControl: AccessControl,
    private val securityConfig: SecurityConfig,
    private val messageSource: MessageSource,
    private val requiredPermissions: List<String>,
    private val requiredRoles: List<Role>,
    private val condition: AccessControlCondition
) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(HandlerSecurity::class.java)
    }

    /** Checks that the user contains required permissions on the chat for this handler.
     * @param chat Current chat.
     * @param user User to verify.
     */
    fun checkPermissions(
        chat: Chat,
        user: User
    ) {
        val isGlobalAdmin: Boolean = securityConfig.isAdmin(user.id.toString())
        val defaultValid: Boolean = requiredPermissions.isEmpty() && requiredRoles.isEmpty()

        if (isGlobalAdmin || defaultValid) {
            logger.debug("admin user: $isGlobalAdmin")
            logger.debug("default permissions: $defaultValid")
            return
        }
        val userRoleInChat: UserRole? = accessControl.findUserRole(chat, user)
        val userRoles: List<Role> = userRoleInChat?.rolesNames?.mapNotNull { roleName ->
            securityConfig.findRole(roleName)
        } ?: emptyList()

        val currentPermissions: List<String> = userRoles.flatMap { role -> role.permissions }

        logger.debug("User $user on chat $chat has permissions: $currentPermissions")

        val valid: Boolean = when(condition) {
            AccessControlCondition.ALL ->
                currentPermissions.containsAll(requiredPermissions) && userRoles.containsAll(requiredRoles)
            AccessControlCondition.ANY -> {
                currentPermissions.any { permission -> requiredPermissions.contains(permission) } ||
                userRoles.any { role -> requiredRoles.contains(role) }
            }
        }

        if (!valid) {
            throw AccessDeniedException(messageSource["handler-security-error-accessDenied"])
        }
    }
}
