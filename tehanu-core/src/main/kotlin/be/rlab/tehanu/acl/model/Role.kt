package be.rlab.tehanu.acl.model

/** Represents a user role.
 *
 * User roles have permissions that must match Handlers required permissions.
 * A role can be marked as _admin_ for administrative tasks purposes.
 */
data class Role(
    val name: String,
    val title: String,
    val permissions: List<String>,
    val admin: Boolean = false
) {
    fun addPermissions(newPermissions: List<String>): Role = copy(
        permissions = permissions + newPermissions
    )
}
