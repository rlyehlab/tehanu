package be.rlab.tehanu.acl.view

import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.acl.model.Role
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.text
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators.assertTrue
import be.rlab.tehanu.view.ui.Control
import be.rlab.tehanu.view.ui.keyboard

/** Field to ask for a list of roles.
 */
fun UserInput.roles(
    accessControl: AccessControl,
    description: String
): Control = keyboard(description = MessageBuilder().text(description)) {
    val roles: List<Role> = accessControl.listRoles()
    roles.forEach { role ->
        button(role.title, role)
    }
//
//    keyboard(KeyboardMode.MULTIPLE_SELECTION) {
//        roles.forEach { role ->
//            button(role.title, role)
//        }
//    }

    validator {
        val roleName: String = incomingMessage.text
        assertTrue(roles.any { role ->
            role.name == roleName
        }, translations["handler-security-error-role-not-found"])
    }
}
