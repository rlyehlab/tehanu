package be.rlab.tehanu.acl.model

/** Defines the required permissions for a MessageHandler.
 */
data class HandlerPermissions(
    val handlerName: String,
    val requiredPermissions: List<String>
)
