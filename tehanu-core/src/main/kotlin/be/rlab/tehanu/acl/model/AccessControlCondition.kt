package be.rlab.tehanu.acl.model

/** Conditions that indicate how to evaluate a roles and permissions.
 */
enum class AccessControlCondition {
    /** All roles and permissions must match. */
    ALL,
    /** At least one role or permission must match. */
    ANY
}
