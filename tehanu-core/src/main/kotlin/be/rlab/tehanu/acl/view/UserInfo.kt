package be.rlab.tehanu.acl.view

data class UserInfo(
    val id: Long?,
    val userName: String?
) {
    override fun toString(): String = userName ?: id?.toString() ?: "<Unknown>"
}
