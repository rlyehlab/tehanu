package be.rlab.tehanu.acl.view

import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators.assertRequired
import be.rlab.tehanu.view.ui.Field
import be.rlab.tehanu.view.ui.field
import be.rlab.tehanu.view.ui.keyboard

/** Field to let the user pick an existing user from a list.
 */
fun UserInput.user(
    accessControl: AccessControl,
    description: String,
    chat: Chat? = null,
    buttons: Boolean = false
): Field = field(description) {
    fun resolveUser(userRef: String): User? {
        return if (userRef.toLongOrNull() != null) {
            accessControl.findUserById(userRef.toLong())
        } else {
            accessControl.findUserByName(userRef.substringAfter("@"))
        }
    }

    if (buttons && chat != null) {
        keyboard {
            accessControl.listUsers(chat.id).forEach { user ->
                button("@${user.userName}", UserInfo(user.id, user.userName))
            }
        }
    } else {
        validator {
            if (incomingMessage.hasText()) {
                val userRef: String = incomingMessage.text
                assertRequired(
                    resolveUser(userRef) != null,
                    translations.get("handler-security-error-user-not-found", userRef)
                )
            }
        }

        resolveValue {
            val user: User = resolveUser(incomingMessage.text)!!
            UserInfo(user.id, user.userName)
        }
    }
}
