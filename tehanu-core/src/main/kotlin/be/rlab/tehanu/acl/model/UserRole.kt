package be.rlab.tehanu.acl.model

import java.util.*

data class UserRole(
    val chatId: UUID,
    val rolesNames: Set<String>
) {
    companion object {
        fun new(
            chatId: UUID,
            rolesNames: Set<String> = emptySet()
        ): UserRole = UserRole(
            chatId = chatId,
            rolesNames = rolesNames
        )
    }
}
