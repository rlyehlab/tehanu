package be.rlab.tehanu.store

class DatabaseMemory(
    private val memorySlotsDAO: MemorySlotsDAO
) : Memory() {

    override fun retrieve(slotName: String): String? =
        memorySlotsDAO.retrieve(slotName)

    override fun store(
        slotName: String,
        jsonData: String
    ) {
        memorySlotsDAO.store(slotName, jsonData)
    }

    override fun clear() {
        memorySlotsDAO.deleteAll()
    }
}
