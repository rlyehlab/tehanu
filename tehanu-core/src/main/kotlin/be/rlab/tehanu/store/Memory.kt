package be.rlab.tehanu.store

import be.rlab.tehanu.support.ObjectMapperFactory
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KProperty

abstract class Memory {

    val logger: Logger = LoggerFactory.getLogger(Memory::class.java)
    private val subscribers: MutableMap<String, List<() -> Unit>> = mutableMapOf()

    abstract fun retrieve(slotName: String): String?

    abstract fun store(
        slotName: String,
        jsonData: String
    )

    abstract fun clear()

    fun <T> slot(
        slotName: String,
        defaultValue: T
    ) = SlotResolver(
        memory = this,
        slotName = slotName,
        defaultValue = defaultValue as Any
    )

    inline fun <reified T> read(slotName: String): T? {
        logger.debug("Reading slot '$slotName'")
        return retrieve(slotName)?.let { jsonData ->
            ObjectMapperFactory.snakeCaseMapper.readValue(jsonData)
        }
    }

    fun<T> save(
        slotName: String,
        element: T
    ): T {
        logger.debug("Serializing and saving slot '$slotName'")
        val jsonSlot = ObjectMapperFactory.snakeCaseMapper.writeValueAsString(element)

        store(slotName, jsonSlot)

        logger.debug("Invoking subscriptions for slot '$slotName'")
        subscribers.getOrDefault(slotName, listOf()).forEach { callback ->
            callback()
        }

        return element
    }

    fun subscribe(
        slotName: String,
        callback: () -> Unit
    ) {
        logger.debug("New subscription to slot '$slotName'")
        val callbacks = subscribers.getOrDefault(slotName, listOf())
        subscribers[slotName] = callbacks + callback
    }

    class SlotResolver(
        val memory: Memory,
        val slotName: String,
        val defaultValue: Any
    ) {
        inline operator fun <reified T> getValue(thisRef: Any?, property: KProperty<*>): T {
            return (memory.read<T>(slotName) ?: defaultValue) as T
        }

        inline operator fun <reified T> setValue(thisRef: Any?, property: KProperty<*>, value: T) {
            memory.save(slotName, value)
        }
    }
}
