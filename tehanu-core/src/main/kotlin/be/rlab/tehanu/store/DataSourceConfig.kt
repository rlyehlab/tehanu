package be.rlab.tehanu.store

data class DataSourceConfig(
    val url: String,
    val user: String,
    val password: String,
    val driver: String,
    val logStatements: Boolean,
    val drop: String
) {
    companion object {
        fun new(
            url: String,
            user: String,
            password: String,
            driver: String,
            logStatements: Boolean = false,
            drop: String = ""
        ): DataSourceConfig = DataSourceConfig(
            url = url,
            user = user,
            password = password,
            driver = driver,
            logStatements = logStatements,
            drop = drop
        )

        fun inMemory(): DataSourceConfig = DataSourceConfig(
            url = "jdbc:h2:mem:tehanu",
            user = "sa",
            password = "",
            driver = "org.h2.Driver",
            logStatements = true,
            drop = ""
        )
    }
    val isTest: Boolean = driver == "org.h2.Driver"
}
