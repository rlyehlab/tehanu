package be.rlab.tehanu.store

/** A memory implementation backed by a Map.
 * It keep state in memory and everything will be lost when the application is terminated.
 */
class DisposableMemory : Memory() {

    private val memory: MutableMap<String, String> = mutableMapOf()

    override fun retrieve(slotName: String): String? {
        return memory[slotName]
    }

    override fun store(slotName: String, jsonData: String) {
        memory[slotName] = jsonData
    }

    override fun clear() {
        memory.clear()
    }
}
