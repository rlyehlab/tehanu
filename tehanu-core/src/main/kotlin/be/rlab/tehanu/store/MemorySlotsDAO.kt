package be.rlab.tehanu.store

import be.rlab.tehanu.support.persistence.TransactionSupport
import org.jetbrains.exposed.sql.deleteAll
import java.util.*

class MemorySlotsDAO : TransactionSupport() {

    fun retrieve(slotName: String): String? = transaction {
        MemorySlotEntity.find {
            MemorySlots.slotName eq slotName
        }.takeIf { results ->
            !results.empty()
        }?.single()?.data
    }

    fun store(
        theSlotName: String,
        jsonData: String
    ): String = transaction {
        val results = MemorySlotEntity.find {
            MemorySlots.slotName eq theSlotName
        }
        if (results.empty()) {
            MemorySlotEntity.new(UUID.randomUUID()) {
                slotName = theSlotName
                data = jsonData
            }
            jsonData
        } else {
            results.single().let { entity ->
                entity.data = jsonData
                jsonData
            }
        }
    }

    fun deleteAll() {
        MemorySlots.deleteAll()
    }
}