package be.rlab.tehanu.store

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.*

object MemorySlots : UUIDTable(name = "memory_slots") {
    val slotName = varchar("slot_name", 50)
    val data = text("data")
}

class MemorySlotEntity(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<MemorySlotEntity>(MemorySlots)
    var slotName: String by MemorySlots.slotName
    var data: String by MemorySlots.data
}
