package be.rlab.tehanu.store

import java.io.File

class FileSystemMemory(
    private val memoryDir: File
) : Memory() {

    init {
        // Ensures the directory exists.
        memoryDir.mkdirs()
    }

    override fun retrieve(slotName: String): String? {
        val file = File(memoryDir, "$slotName.json")

        return file.takeIf {
            file.exists()
        }?.let {
            logger.debug("Loading memory slot '$slotName' from $file")

            return file.bufferedReader().readText()
        }
    }

    override fun store(slotName: String, jsonData: String) {
        val memoryFile = File(memoryDir, "$slotName.json")
        synchronized(this) {
            memoryFile.writeText(jsonData)
        }
    }

    override fun clear() {
        synchronized(this) {
            memoryDir.listFiles()?.forEach { file ->
                if (file.name.endsWith(".json")) {
                    file.delete()
                }
            }
        }
    }
}
