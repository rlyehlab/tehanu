package be.rlab.tehanu.support

import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL

/** Contains common validations.
 */
object Validations {
    fun isValidUrl(url: String): Boolean {
        return try {
            URL(url).toURI()
            true
        } catch (cause: MalformedURLException) {
            false
        } catch (cause: URISyntaxException) {
            false
        }
    }
}