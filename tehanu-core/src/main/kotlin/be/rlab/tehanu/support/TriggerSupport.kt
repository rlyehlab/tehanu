package be.rlab.tehanu.support

import be.rlab.tehanu.clients.Trigger
import be.rlab.tehanu.annotations.Trigger as TriggerAnnotation

/** Must be extended by triggers companion objects.
 * It provides methods to read [TriggerAnnotation] annotation configuration.
 */
abstract class TriggerSupport {

    /** Creates the trigger based on annotation configuration.
     * @param name A name for this trigger.
     * @param triggerConfig Trigger annotation.
     * @return the new created trigger.
     */
    abstract fun new(
        name: String,
        triggerConfig: TriggerAnnotation
    ): Trigger

    /** Checks if a parameter is provided.
     * @param triggerConfig Trigger annotation.
     * @param name Required parameter name.
     * @return true if the parameter exists, false otherwise.
     */
    fun hasParam(
        triggerConfig: TriggerAnnotation,
        name: String
    ): Boolean {
        return triggerConfig.params.any { param -> param.name == name }
    }

    /** Returns a trigger parameter value by name.
     * @param triggerConfig Trigger annotation.
     * @param name Required parameter name.
     * @return the parameter value, or null if it doesn't exist.
     */
    fun getParam(
        triggerConfig: TriggerAnnotation,
        name: String
    ): String? {
        return getParams(triggerConfig, name).firstOrNull()
    }

    /** Returns the values for all the parameters with the specified name.
     * @param triggerConfig Trigger annotation.
     * @param name Required parameter name.
     * @return the required values.
     */
    fun getParams(
        triggerConfig: TriggerAnnotation,
        name: String
    ): Array<out String> {
        return triggerConfig.params.find { param ->
            param.name == name
        }?.value ?: emptyArray()
    }
}
