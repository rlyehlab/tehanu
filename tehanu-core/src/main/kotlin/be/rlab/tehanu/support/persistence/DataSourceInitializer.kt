package be.rlab.tehanu.support.persistence

import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.URL

class DataSourceInitializer(
    private val tables: List<Table>
) : TransactionSupport() {

    companion object {
        private const val DROP_ACK: String = "Yes, please delete all my data forver."
    }

    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    val isTest: Boolean get() { return config.isTest }

    fun initAuto(dropSql: String? = null) = transaction {
        if (config.drop == DROP_ACK && dropSql != null) {
            dropIfRequired(dropSql)
        }

        SchemaUtils.create(*tables.toTypedArray())
        SchemaUtils.createMissingTablesAndColumns(*tables.toTypedArray())
    }

    fun dropIfRequired(dropSql: String) {
        if (config.drop == DROP_ACK) {
            logger.debug("Dropping existing database")
            exec(
                Thread.currentThread().contextClassLoader.getResource(dropSql) ?:
                    throw RuntimeException("Drop DDL file not found: $dropSql")
            )
        }
    }

    fun initIfRequired(
        scriptsPath: String
    ) {
        logger.debug("Initializing database")

        Thread.currentThread().contextClassLoader.getResources(
            scriptsPath
        ).toList().sortedBy { resource ->
            resource.file
        }.filter { resource ->
            resource.file.matches(Regex("^\\d+-.*"))
        }.forEach { resource ->
            logger.debug("Executing DDL: ${resource.file}")
            exec(resource)
        }
    }

    fun exec(resource: URL) {
        transaction {
            val sql: String = resource.openStream().bufferedReader().readText()
            sql.split(";").filter { line ->
                line.isNotBlank()
            }.forEach { line ->
                exec("$line;")
            }
        }
    }
}
