package be.rlab.tehanu.command

import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.acl.model.Role
import be.rlab.tehanu.acl.model.UserRole
import be.rlab.tehanu.acl.view.UserInfo
import be.rlab.tehanu.acl.view.roles
import be.rlab.tehanu.acl.view.user
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.Security
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.model.ChatType
import be.rlab.tehanu.clients.model.User
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/** Built-in handlers to manage users.
 */
class UserManager(private val accessControl: AccessControl) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(UserManager::class.java)
        const val ROLE_ADMIN: String = "ADMIN"
        const val MANAGE_USERS: String = "MANAGE_USERS"
    }

    /** Adds a new user and sets the roles.
     * If executed in a group, it adds roles only for the chat.
     * If executed in a private chat, it adds global roles for the current [be.rlab.tehanu.clients.Client].
     */
    @Handler(name = "/user_add")
    @Security(requiredPermissions = [MANAGE_USERS])
    fun add(
        context: UpdateContext,
        accessControl: AccessControl
    ) = context.userInput {
        logger.info("add user begins")

        val user: UserInfo by user(accessControl, "{handler-security-addUser-userName}")
        val roles: List<Role> by roles(accessControl, "{handler-security-addUser-roles}")

        onSubmit { addRoles(user, roles) }
    }

    /** Removes a user.
     */
    @Handler(name = "/user_remove")
    @Security(requiredPermissions = [MANAGE_USERS])
    fun remove(
        context: UpdateContext,
        accessControl: AccessControl
    ) = context.userInput {
        logger.info("remove user begins")
        val user: UserInfo by user(accessControl, "{handler-security-removeUser-userName}")
        onSubmit { removeUser(user) }
    }

    /** List all registered users in all chats.
     */
    @Handler(name = "/user_list")
    @Security(requiredPermissions = [MANAGE_USERS])
    fun list(context: UpdateContext) = context.apply {
        logger.info("listing all users, required by $user")
        val usersInfo: String = accessControl.listChats().joinToString("\n\n") { chat ->
            val userList: String = accessControl.listUsers(chat.id).takeIf {
                it.isNotEmpty()
            }?.joinToString("\n") { user ->
                val userRole: UserRole? = accessControl.findUserRole(chat, user)
                if (userRole != null) {
                    "$user " +
                    "; First Name: ${user.firstName} " +
                    "; Last Name: ${user.lastName} " +
                    "; Roles: ${userRole.rolesNames.joinToString()}"
                } else {
                    ""
                }
            } ?: ""
            if (userList.isNotBlank()) {
                "# ${chat.title}\n$userList"
            } else {
                ""
            }
        }
        talkTo(user, usersInfo)
    }

    /** Provides information about the user calling the command.
     * The information is provided in a private chat.
     */
    @Handler(name = "/me")
    fun me(context: UpdateContext) = context.apply {
        talkTo(user, "$user ; First name: ${user.firstName} ; Last name: ${user.lastName}")
    }

    private fun UpdateContext.addRoles(
        selectedUser: UserInfo,
        roles: List<Role>
    ): UpdateContext = apply {
        val resolvedRoles: Set<String> = roles.map(Role::name).toSet()
        val resolvedUser: User =
            selectedUser.id?.let { accessControl.findUserById(selectedUser.id) }
            ?: selectedUser.userName?.let { accessControl.findUserByName(selectedUser.userName) }
            ?: return answer(translations["handler-security-error-user-not-selected"])

        logger.info("adding roles (${resolvedRoles.joinToString()}) to user $resolvedUser ")
        when(chat.type) {
            ChatType.GROUP -> accessControl.assign(chat, resolvedUser, resolvedRoles)
            ChatType.PRIVATE -> accessControl.assign(chat.clientName, resolvedUser, resolvedRoles)
            else -> throw RuntimeException("Unsupported chat")
        }

        talk(
            "{handler-security-addUser-success}",
            resolvedUser.userName ?: translations["handler-security-addUser-success-defaultUser"],
            resolvedRoles.joinToString()
        )

        if (roles.any(Role::admin)) {
            talkTo(resolvedUser, "{handler-security-addUser-admin}")
        }
    }

    private fun UpdateContext.removeUser(selectedUser: UserInfo): UpdateContext = apply {
        val resolvedUser: User =
            selectedUser.id?.let { accessControl.findUserById(selectedUser.id) }
                ?: selectedUser.userName?.let { accessControl.findUserByName(selectedUser.userName) }
                ?: return answer(translations["handler-security-error-user-not-selected"])

        logger.info("removing user $resolvedUser ")
        when(chat.type) {
            ChatType.GROUP -> accessControl.drop(chat, resolvedUser)
            ChatType.PRIVATE -> accessControl.drop(chat.clientName, resolvedUser)
            else -> throw RuntimeException("Unsupported chat")
        }
        talk(
            "{handler-security-removeUser-success}",
            resolvedUser.userName ?: translations["handler-security-addUser-removeUser-defaultUser"]
        )
    }
}
