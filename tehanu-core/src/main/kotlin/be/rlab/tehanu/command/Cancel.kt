package be.rlab.tehanu.command

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext

object Cancel {
    @Handler(name = "/cancel")
    fun dummy(context: UpdateContext) = context.apply {
        return answer("cancelé el comando actual y no hice ningún cambio")
    }
}
