package be.rlab.tehanu

import kotlin.reflect.KClass

class DefaultServiceProvider : BotServiceProvider() {
    private val configServiceMapping: MutableMap<KClass<*>, MutableList<() -> Any>> = mutableMapOf()

    fun registerService(
        type: KClass<*>,
        resolver: () -> Any
    ) {
        if (!configServiceMapping.containsKey(type)) {
            configServiceMapping[type] = mutableListOf()
        }
        configServiceMapping.getValue(type) += resolver
    }

    fun registerService(instance: Any) {
        registerService(instance::class) { instance }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> getServices(type: KClass<T>): List<T> {
        return if (!configServiceMapping.containsKey(type)) {
            emptyList()
        } else {
            configServiceMapping.getValue(type).map { factory -> factory.invoke() } as List<T>
        }
    }

    override fun <T : Any> getService(type: KClass<T>): T {
        require(configServiceMapping.containsKey(type)) { "Service of type $type not found" }
        return getServices(type).first()
    }

    inline fun <reified T : Any> getService(): T {
        return getService(T::class)
    }
}
