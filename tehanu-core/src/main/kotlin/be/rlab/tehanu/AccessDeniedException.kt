package be.rlab.tehanu

class AccessDeniedException(override val message: String) : RuntimeException(message)
