package be.rlab.tehanu

class InvalidScopeException(override val message: String) : RuntimeException(message)
