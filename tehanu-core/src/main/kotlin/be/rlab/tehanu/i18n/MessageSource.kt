package be.rlab.tehanu.i18n

/** A message source resolves messages for different languages.
 *
 * It provides a hierarchical organization for internationalized messages. It is possible
 * to specify a [parent] message source and it will lookup for messages upward in the hierarchy.
 *
 * Each key supports multiple messages. If a key has multiple messages, this component will pick
 * a random value.
 *
 * Messages support placeholders for positional parameters and references to other messages.
 */
class MessageSource(
    private val parent: MessageSource?,
    private val messages: Map<Language, MessageMap>,
    private val defaultLanguage: Language
) {

    companion object {
        fun empty(language: Language = Language.SPANISH): MessageSource =
            MessageSource(null, emptyMap(), language)
    }

    /** Returns a new message source with the specified parent.
     * @param messageSource Parent message source.
     */
    fun withParent(messageSource: MessageSource): MessageSource {
        return MessageSource(
            parent = messageSource,
            messages = messages,
            defaultLanguage = defaultLanguage
        )
    }

    /** Gets the message with the specified name.
     * @param name Message name.
     * @return the required message, throws an exception if the message doesn't exist.
     */
    operator fun get(name: String): String {
        return lookup(defaultLanguage, name)?.let { messages ->
            resolve(defaultLanguage, messages.random())
        } ?: throw RuntimeException("message not found: $name")
    }

    /** Gets the message with the specified name.
     * @param name Message name.
     * @param args Arguments for the message.
     * @return the required message, throws an exception if the message doesn't exist.
     */
    fun get(
        name: String,
        vararg args: Any
    ): String {
        return lookup(defaultLanguage, name)?.let { messages ->
            resolve(defaultLanguage, messages.random(), *args)
        } ?: throw RuntimeException("message not found: $name")
    }

    /** Finds a message with the specified name.
     * @param name Message name.
     * @param args Arguments for the message.
     * @return the required message, or null if it doesn't exist.
     */
    fun find(
        name: String,
        vararg args: Any
    ): String? {
        return lookup(defaultLanguage, name)?.let { messages ->
            resolve(defaultLanguage, messages.random(), *args)
        }
    }

    /** Takes a message and expands the placeholders.
     * @param message Message to expand.
     * @param args Arguments for the message.
     * @return the expanded message.
     */
    fun expand(
        message: String,
        vararg args: Any
    ): String {
        return resolve(defaultLanguage, message, *args)
    }

    private fun lookup(
        language: Language,
        name: String
    ): List<String>? {
        val messageMap = messages.getValue(language)

        return if (messageMap.containsKey(name)) {
            messageMap.getValue(name)
        } else {
            parent?.lookup(language, name)
        }
    }

    private fun resolve(
        language: Language,
        message: String,
        vararg args: Any
    ): String {
        val messageMap: MessageMap = messages.getValue(language)

        val expanded: String = messageMap.keys.fold(message) { resolved, name ->
            val values: List<String> = messageMap.getValue(name)
            resolved.replace("{$name}", values.random())
        }

        return args.foldIndexed(expanded) { index, resolved, current ->
            val value = resolve(language, current.toString())
            resolved.replace("{$index}", value)
        }
    }
}
