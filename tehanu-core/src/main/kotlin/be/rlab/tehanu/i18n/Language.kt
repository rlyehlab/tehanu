package be.rlab.tehanu.i18n

import be.rlab.nlp.model.Language as LuceneLanguage

/** List of supported languages in Kotlin Search.
 * This is a copy of [be.rlab.nlp.model.Language] to mark the kotlin-search dependency as provided.
 */
enum class Language(val code: String) {
    ARABIC("ar"),
    ARMENIAN("hy"),
    BASQUE("eu"),
    BENGALI("bn"),
    BRAZILIAN("pt"),
    BULGARIAN("bg"),
    CATALAN("ca"),
    CHINESE("zh"),
    CZECH("cs"),
    DANISH("da"),
    ENGLISH("en"),
    ESTONIAN(""),
    FINNISH("fi"),
    FRENCH("fr"),
    GALICIAN("gl"),
    GERMAN("de"),
    GREEK("el"),
    HINDI("hi"),
    HUNGARIAN("hu"),
    INDONESIAN("id"),
    IRISH("ga"),
    ITALIAN("it"),
    LATVIAN("lv"),
    LITHUANIAN("lt"),
    NORWEGIAN("no"),
    PERSIAN("fa"),
    POLISH("pl"),
    PORTUGUESE("pt"),
    ROMANIAN("ro"),
    RUSSIAN("ru"),
    SORANI("ku"),
    SPANISH("es"),
    SWEDISH("sv"),
    THAI("th"),
    TURKISH("tr");

    fun toLuceneLanguage(): LuceneLanguage {
        return LuceneLanguage.valueOf(name)
    }
}
