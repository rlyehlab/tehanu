package be.rlab.tehanu.i18n

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.typesafe.config.ConfigValueType

class MessageSourceFactory(
    globalLanguage: Language? = null
) {

    private data class MessageSourceConfig(
        val namespace: String,
        val language: Language,
        val messages: MessageMap
    )

    companion object {
        private const val DEFAULT_NAMESPACE: String = "default"
    }

    private lateinit var messageSources: Map<String, MessageSource>
    var defaultLanguage: Language = globalLanguage ?: Language.SPANISH

    fun initialize() {
        messageSources = loadMessages()
    }

    fun resolve(namespace: String): MessageSource {
        val defaultMessageSource: MessageSource = messageSources
            .getOrDefault(DEFAULT_NAMESPACE, MessageSource.empty())
        return messageSources.getOrDefault(namespace, defaultMessageSource)
    }

    private fun loadMessages(): Map<String, MessageSource> {
        val messageSources: Map<String, MessageSource> = Language.entries.flatMap { language ->
            loadMessages("messages.${language.name}.conf", language) +
            loadMessages("messages.${language.code}.conf", language)
        }.groupBy {
            it.namespace
        }.map { (namespace, configs) ->
            val messages = configs.associate { config ->
                config.language to config.messages
            }

            namespace to MessageSource(
                parent = null,
                messages = messages,
                defaultLanguage = defaultLanguage
            )
        }.toMap()

        // Sets the default message source as parent of all other message sources.
        return messageSources[DEFAULT_NAMESPACE]?.let { defaultMessageSource ->
            messageSources.filterKeys { namespace ->
                namespace != DEFAULT_NAMESPACE
            }.map { (namespace, messageSource) ->
                namespace to messageSource.withParent(defaultMessageSource)
            } + (DEFAULT_NAMESPACE to defaultMessageSource)
        }?.toMap() ?: messageSources
    }

    private fun loadMessages(
        messagesFile: String,
        language: Language
    ): List<MessageSourceConfig> {
        val messagesConfig: Config = ConfigFactory.load(messagesFile.lowercase()).resolve()

        return if (messagesConfig.hasPath("messages")) {
            val root = messagesConfig.getConfig("messages").root()

            root.entries.map { entry ->
                entry.key
            }.map { namespace ->
                MessageSourceConfig(
                    namespace,
                    language,
                    resolve(messagesConfig.getConfig("messages.$namespace"))
                )
            }
        } else {
            emptyList()
        }
    }

    private fun resolve(messagesConfig: Config): MessageMap {
        return messagesConfig.root().entries.associate { (name, value) ->
            val values: List<String> = when (value.valueType()) {
                ConfigValueType.LIST -> messagesConfig.getStringList(name)
                else -> listOf(messagesConfig.getString(name))
            }

            name to values
        }
    }
}