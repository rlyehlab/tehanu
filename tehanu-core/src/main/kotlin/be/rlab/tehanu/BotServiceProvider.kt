package be.rlab.tehanu

import kotlin.reflect.KClass

/** Tehanu implements several services for different purposes, this class is
 * an abstraction to access all services.
 *
 * The goal of this class is keep backward compatibility for Clients and other interfaces
 * that require access to internal services. Instead of breaking the contract to add new services,
 * a Client can compose the [BotServiceProvider] to access the required services.
 */
abstract class BotServiceProvider {
    /** Resolves and return a service of the required type.
     * @param type Required service type.
     * @return the required service.
     * @throws IllegalArgumentException if the service cannot be resolved or there is more than one
     * registered service of the same type.
     */
    abstract fun<T : Any> getService(type: KClass<T>): T

    /** Resolves and returns all registered services of the required type.
     * @param type Required services type.
     * @return the required services, or an empty list if there is no registered service of the required type.
     */
    abstract fun<T : Any> getServices(type: KClass<T>): List<T>
}
