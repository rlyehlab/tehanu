package be.rlab.tehanu.clients.telegram.view.ui

import be.rlab.tehanu.clients.telegram.InboundMessageFactory.Companion.CONTENT_FIELD
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators.assertTrue
import be.rlab.tehanu.view.ui.Control
import be.rlab.tehanu.view.ui.custom
import com.github.kotlintelegrambot.entities.Contact

fun UserInput.contact(
    description: MessageBuilder,
    callback: (Control.() -> Unit) = {}
) = custom(description) {

    resolveValue {
        setValue(incomingMessage[CONTENT_FIELD])
    }

    validator {
        assertTrue(
            incomingMessage.content[CONTENT_FIELD] is Contact,
            "Tenés que mandarme un contacto de tu agenda de contactos."
        )
    }

    callback(this)
}
