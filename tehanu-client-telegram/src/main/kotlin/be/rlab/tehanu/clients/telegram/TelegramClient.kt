package be.rlab.tehanu.clients.telegram

import be.rlab.tehanu.BotServiceProvider
import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.clients.*
import be.rlab.tehanu.clients.model.*
import be.rlab.tehanu.clients.telegram.extensions.*
import be.rlab.tehanu.clients.telegram.inlinequery.InlineQueryManager
import be.rlab.tehanu.clients.telegram.inlinequery.model.InlineQueryResponse
import be.rlab.tehanu.clients.telegram.model.OutboundMessage
import be.rlab.tehanu.clients.telegram.sender.*
import be.rlab.tehanu.clients.telegram.view.ui.toInlineKeyboard
import be.rlab.tehanu.i18n.MessageSourceFactory
import be.rlab.tehanu.media.MediaManager
import be.rlab.tehanu.media.model.MediaFile
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.*
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.ui.Button
import be.rlab.tehanu.view.ui.Control
import be.rlab.tehanu.view.ui.Keyboard
import com.github.kotlintelegrambot.Bot
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import com.github.kotlintelegrambot.entities.Chat as TelegramChat
import com.github.kotlintelegrambot.entities.Message as TelegramMessage
import com.github.kotlintelegrambot.entities.Update as TelegramUpdate
import com.github.kotlintelegrambot.entities.User as TelegramUser
import com.github.kotlintelegrambot.entities.files.File as TelegramFile

/** Messaging client for Telegram.
 */
class TelegramClient(
    override val name: String,
    override val serviceProvider: BotServiceProvider,
    private val inboundMessageFactory: InboundMessageFactory,
    botFactory: TelegramBotFactory
) : Client {

    companion object {
        const val SKIP_UPDATE_FIELD = "skipUpdate"
    }

    private val logger: Logger = LoggerFactory.getLogger(TelegramClient::class.java)
    private val accessControl: AccessControl by lazy { serviceProvider.getService(AccessControl::class) }
    private val dispatcher: UpdateDispatcher by lazy { serviceProvider.getService(UpdateDispatcher::class) }
    private val mediaManager: FileManager by lazy { serviceProvider.getService(MediaManager::class) as FileManager }
    private val inlineQueryManager: InlineQueryManager = InlineQueryManager(this)
    private val bot: Bot = botFactory.createBot(::handleUpdate)
    private val senders: List<MessageSender> = listOf(
        MediaSender(bot),
        TextSender(bot, serviceProvider.getService(MessageSourceFactory::class)),
        VoiceSender(bot),
        ContactSender(bot),
        LocationSender(bot),
        AlbumSender(bot),
        InlineQueryResultSender(bot)
    )

    override fun start() {
        mediaManager.client = this
        logger.info("start polling for updates")
        bot.startPolling()
    }

    override fun sendMessage(context: UpdateContext, message: MessageBuilder): List<Message> {
        return if (message.response is InlineQueryResponse) {
            inlineQueryManager.sendMessage(context, message)
        } else {
            val clientId: Long? = when {
                message.userId != null -> accessControl.findUserById(message.userId!!)?.id
                    ?: throw RuntimeException("User not found: ${message.userId}")

                message.chatId != null ->
                    accessControl.findChatById(message.chatId!!)?.clientId
                        ?: throw RuntimeException("Chat not found: ${message.chatId}")

                else -> null
            }

            doSendMessage(message, clientId)
        }
    }

    override fun editMessage(
        chatId: UUID,
        messageId: Long,
        newMessage: MessageBuilder
    ): Message {
        val chat: Chat = accessControl.findChatById(chatId)
            ?: throw RuntimeException("Chat not found: $chatId")
        val outboundMessage: OutboundMessage = createOutboundMessage(newMessage)
        val sender: MessageSender = senders.find { sender -> sender.supports(outboundMessage) }
            ?: throw RuntimeException("Cannot send message, message sender not found: $outboundMessage")
        return inboundMessageFactory.createMessage(sender.edit(chat.clientId, messageId, outboundMessage))
    }

    override fun renderControl(
        context: UpdateContext,
        control: Control,
        messageId: Long?
    ) {
        if (inlineQueryManager.renderControl(context, control)) {
            return
        }

        logger.debug("rendering control: ${control.description}")
        val responseMessage = when (control) {
            // TODO(seykron): support standalone buttons.
            is Button -> control.description?.replyMarkup((control.parent as Keyboard).toInlineKeyboard())
            is Keyboard -> control.description?.replyMarkup(control.toInlineKeyboard())
            else -> control.description
        }
        require(responseMessage != null) { "the response message cannot be null" }

        if (messageId == null) {
            sendMessage(context, responseMessage.forChat(context.chat.id))
        } else {
            try {
                editMessage(context.chat.id, messageId, responseMessage.forChat(context.chat.id))
            } catch (cause: Throwable) {
                // TODO(seykron: Sometimes editing the reply markup fails. Telegram throws an error if the reply markup
                // did not change, but it's a valid scenario for us since we're redrawing controls every time
                // there is user interaction. It should be smarter and do not send reply markup if it didn't change.
                logger.debug("error editing reply markup: ${cause.message}")
            }
        }
    }

    /** Handles a Telegram [TelegramUpdate]s.
     * @param update Telegram update to handle.
     */
    private fun handleUpdate(update: TelegramUpdate) {
        val resolvedUpdate = inlineQueryManager.resolveUpdate(update) ?: resolveChatUpdate(update)
        if (resolvedUpdate.attributes.containsKey(SKIP_UPDATE_FIELD)) {
            return
        }

        var state: State? = null

        try {
            val nextState: State? = dispatcher.dispatch(this, resolvedUpdate)
            state = inlineQueryManager.postProcessUpdate(resolvedUpdate, nextState)
        } catch (cause: Exception) {
            logger.error("error processing update: $resolvedUpdate")
            throw cause
        } finally {
            // Notifies Telegram to stop waiting for a callback button answer.
            resolvedUpdate.action?.let { action ->
                replyAction(action, state?.currentInput)
            }
        }
    }

    /** Handles a [TelegramUpdate] from a [Chat].
     * @param update Telegram update to handle.
     * @param attributes Custom attributes attached to the Update.
     */
    internal fun resolveChatUpdate(
        update: TelegramUpdate,
        attributes: Map<String, Any> = emptyMap()
    ): Update {
        val message: TelegramMessage = update.message
            ?: update.callbackQuery?.message
            ?: update.editedMessage
            ?: throw RuntimeException("Message not found")

        val botMessage: Message = inboundMessageFactory.createMessage(message)
        val chat: Chat = resolveChat(message.chat)
        val action: Action? = update.callbackQuery?.let { callbackQuery ->
            Action(
                id = callbackQuery.id,
                data = callbackQuery.data
            )
        }
        val telegramUser: TelegramUser = update.message?.from
                ?: update.callbackQuery?.from
                ?: throw RuntimeException("User not found")

        return Update.new(
            updateId = update.updateId,
            chat = chat,
            user = resolveUser(telegramUser, chat),
            message = botMessage,
            action = action,
            attributes = attributes
        )
    }

    internal fun resolveChat(chat: TelegramChat): Chat {
        return accessControl.addChatIfRequired(
            Chat.new(
                clientName = name,
                clientId = chat.id,
                type = ChatType.from(chat.type),
                title = chat.title,
                description = chat.description
            )
        )
    }

    internal fun resolveUser(
        user: TelegramUser,
        chat: Chat? = null
    ): User {
        val resolvedUser = User(
            id = user.id,
            userName = user.username,
            firstName = user.firstName,
            lastName = user.lastName
        )
        return chat?.let {
            accessControl.addUserIfRequired(chat, resolvedUser)
        } ?: accessControl.addUserIfRequired(resolvedUser)
    }

    internal fun doSendMessage(
        messageBuilder: MessageBuilder,
        chatId: Long? = null
    ): List<Message> {
        val outboundMessage: OutboundMessage = createOutboundMessage(messageBuilder)
        val sender: MessageSender = senders.find { sender -> sender.supports(outboundMessage) }
            ?: throw RuntimeException("Cannot send message, message sender not found: $outboundMessage")
        return sender.send(chatId, outboundMessage).map(inboundMessageFactory::createMessage)
    }

    private fun createOutboundMessage(messageBuilder: MessageBuilder): OutboundMessage {
        return OutboundMessage.new(
            response = messageBuilder.response,
            disableNotification = messageBuilder.option(DISABLE_NOTIFICATION) ?: false,
            disableLinkPreview = messageBuilder.option(DISABLE_LINK_PREVIEW) ?: false,
            parseMode = messageBuilder.option(PARSE_MODE),
            replyMarkup = messageBuilder.option(REPLY_MARKUP),
            replyToMessageId = messageBuilder.replyTo?.messageId
        )
    }

    /** Replies to a user action if required.
     *
     * @param action Action to reply.
     * @param input [UserInput] that contains the required information to reply the action.
     */
    private fun replyAction(
        action: Action,
        input: UserInput?
    ) {
        // Notifies Telegram to stop waiting for a callback button answer.
        input?.let {
            bot.answerCallbackQuery(
                callbackQueryId = action.id,
                text = input.attributes[FEEDBACK_MESSAGE] as String?,
                showAlert = input.attributes[IS_ALERT] as Boolean?
            )
        } ?: bot.answerCallbackQuery(action.id)
    }

    internal fun getFile(mediaFile: MediaFile): TelegramFile {
        return bot.getFile(mediaFile.clientId).first
            ?.body()
            ?.result
            ?: throw RuntimeException("file not found: ${mediaFile.clientId}")
    }
}
