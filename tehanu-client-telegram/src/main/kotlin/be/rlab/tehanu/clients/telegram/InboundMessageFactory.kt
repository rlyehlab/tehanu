package be.rlab.tehanu.clients.telegram

import be.rlab.tehanu.clients.telegram.model.EventNames
import be.rlab.tehanu.media.model.MediaFile
import be.rlab.tehanu.messages.model.*
import com.github.kotlintelegrambot.entities.files.*
import com.github.kotlintelegrambot.entities.stickers.Sticker
import org.joda.time.DateTime
import com.github.kotlintelegrambot.entities.Message as TelegramMessage
import com.github.kotlintelegrambot.entities.MessageEntity.Type as TelegramEntityType

class InboundMessageFactory {

    companion object {
        private const val DEFAULT_AUDIO: String = "audio/mpeg"
        private const val DEFAULT_PHOTO: String = "image/jpeg"
        private const val DEFAULT_VIDEO: String = "video/mp4"
        private const val DEFAULT_DOCUMENT: String = "application/octet-stream"
        private const val DEFAULT_ANIMATION: String = "image/gif"
        const val CONTENT_FIELD: String = "additionalContent"
        const val VIA_BOT_FIELD: String = "viaBot"
    }

    fun createMessage(message: TelegramMessage): Message {
        val chatEvent: ChatEvent? = when {
            message.deleteChatPhoto == true -> ChatEvent(EventNames.deleteChatPhoto)
            message.groupChatCreated == true -> ChatEvent(EventNames.groupChatCreated)
            message.channelChatCreated == true -> ChatEvent(EventNames.channelChatCreated)
            message.newChatTitle != null -> ChatEvent(EventNames.newChatTitle, message.newChatTitle)
            message.newChatPhoto != null -> ChatEvent(EventNames.newChatPhoto, message.newChatPhoto)
            message.newChatMembers != null -> ChatEvent(EventNames.newChatMembers, message.newChatMembers)
            message.leftChatMember != null -> ChatEvent(EventNames.leftChatMember, message.leftChatMember)
            else -> null
        }
        val files: List<MediaFile> = resolveMediaFiles(message)

        return Message.new(
            messageId = message.messageId,
            date = resolveDate(message),
            isEdit = message.editDate != null,
            isForward = message.forwardDate != null,
            text = message.text,
            entities = extractTextEntities(message),
            event = chatEvent,
            files = files,
            content = mapOf(
                CONTENT_FIELD to (message.contact
                    ?: message.dice
                    ?: message.game
                    ?: message.sticker
                    ?: message.poll
                    ?: message.venue
                    ?: message.audio
                    ?: message.video
                    ?: message.photo
                    ?: message.voice
                    ?: message.animation
                    ?: message.document
                    ?: message.videoNote
                    ?: message.location
                ),
                VIA_BOT_FIELD to message.viaBot
            )
        )
    }

    private fun resolveDate(message: TelegramMessage): DateTime {
        val resolvedDate: Long = message.editDate?.toLong()
            ?: message.forwardDate?.toLong()
            ?: message.date
        return DateTime(resolvedDate * 1000)
    }

    private fun extractTextEntities(message: TelegramMessage): List<MessageEntity> {
        return message.entities?.map { messageEntity ->
            val type = when(messageEntity.type) {
                TelegramEntityType.MENTION, TelegramEntityType.TEXT_MENTION -> EntityType.MENTION
                TelegramEntityType.HASHTAG, TelegramEntityType.CASHTAG -> EntityType.HASHTAG
                TelegramEntityType.BOT_COMMAND -> EntityType.BOT_COMMAND
                TelegramEntityType.URL, TelegramEntityType.TEXT_LINK -> EntityType.URL
                TelegramEntityType.EMAIL -> EntityType.EMAIL
                TelegramEntityType.PHONE_NUMBER -> EntityType.PHONE_NUMBER
                TelegramEntityType.BOLD, TelegramEntityType.ITALIC, TelegramEntityType.UNDERLINE,
                TelegramEntityType.STRIKETHROUGH, TelegramEntityType.CODE, TelegramEntityType.PRE ->
                    EntityType.RICH_TEXT
                else -> EntityType.UNKNOWN
            }

            MessageEntity(
                type = type,
                value = messageEntity.user?.id?.toString()
                    ?: messageEntity.url
                    ?: message.text?.substring(messageEntity.offset until (messageEntity.offset + messageEntity.length))
                    ?: "",
                data = messageEntity
            )
        } ?: emptyList()
    }

    private fun resolveMediaFiles(message: TelegramMessage): List<MediaFile> {
        val files: MutableList<MediaFile> = mutableListOf()
        with(message) {
            photo?.let { files += it.map(::createMediaPhoto)  }
            animation?.let { files += createMediaAnimation(it) }
            audio?.let { files += createMediaAudio(it) }
            video?.let { files += createMediaVideo(it) }
            document?.let { files += createMediaDocument(it) }
            videoNote?.let { files += createMediaVideoNote(it) }
            sticker?.let { files += createMediaSticker(it) }
            voice?.let { files += createMediaVoice(it) }
        }
        return files
    }

    private fun createMediaPhoto(photo: PhotoSize): MediaFile {
        return MediaFile.new(
            clientId = photo.fileId,
            size = photo.fileSize?.toLong() ?: 0,
            mimeType = DEFAULT_PHOTO,
            metadata = mapOf(
                "height" to photo.height,
                "width" to photo.width
            )
        )
    }

    private fun createMediaAnimation(animation: Animation): MediaFile {
        return MediaFile.new(
            clientId = animation.fileId,
            size = animation.fileSize ?: 0,
            mimeType = animation.mimeType ?: DEFAULT_ANIMATION,
            metadata = mapOf(
                "height" to animation.height,
                "width" to animation.width,
                "duration" to animation.duration
            )
        )
    }

    private fun createMediaAudio(audio: Audio): MediaFile {
        return MediaFile.new(
            clientId = audio.fileId,
            size = audio.fileSize?.toLong() ?: 0,
            mimeType = audio.mimeType ?: DEFAULT_AUDIO,
            metadata = mapOf(
                "performer" to audio.performer,
                "duration" to audio.duration,
                "title" to audio.title
            )
        )
    }

    private fun createMediaVideo(video: Video): MediaFile {
        return MediaFile.new(
            clientId = video.fileId,
            size = video.fileSize?.toLong() ?: 0,
            mimeType = video.mimeType ?: DEFAULT_VIDEO,
            metadata = mapOf(
                "duration" to video.duration,
                "height" to video.height,
                "width" to video.width
            )
        )
    }

    private fun createMediaDocument(document: Document): MediaFile {
        return MediaFile.new(
            clientId = document.fileId,
            size = document.fileSize?.toLong() ?: 0,
            mimeType = document.mimeType ?: DEFAULT_DOCUMENT,
            metadata = emptyMap()
        )
    }

    private fun createMediaVideoNote(videoNote: VideoNote): MediaFile {
        return MediaFile.new(
            clientId = videoNote.fileId,
            size = videoNote.fileSize?.toLong() ?: 0,
            mimeType = DEFAULT_VIDEO,
            metadata = mapOf(
                "duration" to videoNote.duration,
                "length" to videoNote.length
            )
        )
    }

    private fun createMediaSticker(sticker: Sticker): MediaFile {
        return MediaFile.new(
            clientId = sticker.fileId,
            size = sticker.fileSize?.toLong() ?: 0,
            mimeType = DEFAULT_PHOTO,
            metadata = mapOf(
                "height" to sticker.height,
                "width" to sticker.width,
                "emoji" to sticker.emoji,
                "isAnimated" to sticker.isAnimated,
                "maskPosition" to sticker.maskPosition,
                "setName" to sticker.setName
            )
        )
    }

    private fun createMediaVoice(voice: Voice): MediaFile {
        return MediaFile.new(
            clientId = voice.fileId,
            size = voice.fileSize?.toLong() ?: 0,
            mimeType = voice.mimeType ?: DEFAULT_AUDIO,
            metadata = mapOf(
                "duration" to voice.duration,
            )
        )
    }
}
