package be.rlab.tehanu.clients.telegram.view.ui

import be.rlab.tehanu.view.ui.Keyboard
import com.github.kotlintelegrambot.entities.InlineKeyboardMarkup
import com.github.kotlintelegrambot.entities.keyboard.InlineKeyboardButton

/** Converts this keyboard to a Telegram Inline Keyboard.
 * @return A valid Telegram keyboard.
 */
fun Keyboard.toInlineKeyboard(): InlineKeyboardMarkup {
    val buttons = buttons().chunked(lineLength) { line ->
        line.map { button ->
            InlineKeyboardButton.CallbackData(
                text = button.title(),
                callbackData = button.id.toString()
            )
        }
    }
    return InlineKeyboardMarkup.create(buttons)
}
