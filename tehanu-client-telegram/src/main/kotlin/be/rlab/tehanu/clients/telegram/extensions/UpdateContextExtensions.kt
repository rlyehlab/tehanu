package be.rlab.tehanu.clients.telegram.extensions

import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.telegram.inlinequery.InlineQueryManager.Companion.INLINE_QUERY_FIELD
import be.rlab.tehanu.clients.telegram.inlinequery.InlineQueryManager.Companion.INLINE_QUERY_RESULT_FIELD
import be.rlab.tehanu.clients.telegram.model.InlineQuery

fun UpdateContext.inlineQuery(): InlineQuery {
    require(update.attributes.containsKey(INLINE_QUERY_FIELD)) { "this operation is only supported for inline queries" }
    return update.attributes.getValue(INLINE_QUERY_FIELD) as InlineQuery
}

fun UpdateContext.inlineQueryResultId(): String {
    require(update.attributes.containsKey(INLINE_QUERY_RESULT_FIELD)) { "this operation is only supported for inline queries" }
    return update.attributes.getValue(INLINE_QUERY_RESULT_FIELD) as String
}
