package be.rlab.tehanu.clients.telegram.inlinequery.model

enum class InlineQueryFlow {
    /** The user is searching for results in the inline query. */
    SEARCH_QUERY,
    /** The user selected a result and Telegram displayed the result as a message. */
    CALLBACK_MESSAGE,
    /** The user interacts with the message. */
    USER_ACTION,
    /** Telegram reports the result chosen by the user as a special Update. */
    CHOSEN_RESULT,
    /** Not an inline query flow. */
    INVALID_FLOW
}
