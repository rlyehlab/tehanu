package be.rlab.tehanu.clients.telegram.sender

import be.rlab.tehanu.clients.telegram.MessageSender
import be.rlab.tehanu.clients.telegram.ResponseUtils.resolveMessage
import be.rlab.tehanu.clients.telegram.model.InputMediaResponse
import be.rlab.tehanu.clients.telegram.model.OutboundMessage
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.TelegramFile
import com.github.kotlintelegrambot.entities.TelegramFile.*
import com.github.kotlintelegrambot.entities.inputmedia.*
import com.github.kotlintelegrambot.entities.Message as TelegramMessage
import com.github.kotlintelegrambot.network.Response as BotResponse
import retrofit2.Response as HttpResponse

typealias MediaSenderOp = (Long, OutboundMessage) -> List<TelegramMessage>

/** Sends messages with media content.
 */
class MediaSender(
    /** Bot that sends the message. */
    private val bot: Bot
) : MessageSender {

    override fun supports(outboundMessage: OutboundMessage): Boolean {
        return outboundMessage.response is InputMediaResponse
    }

    override fun send(
        chatId: Long?,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val senders: Map<String, MediaSenderOp> = mapOf(
            InputMediaTypes.ANIMATION to ::sendAnimation,
            InputMediaTypes.AUDIO to ::sendAudio,
            InputMediaTypes.DOCUMENT to ::sendDocument,
            InputMediaTypes.PHOTO to ::sendPhoto,
            InputMediaTypes.VIDEO to ::sendVideo
        )
        val response: InputMediaResponse = outboundMessage.response()
        val sender: MediaSenderOp = requireNotNull(senders[response.type])
        return sender(chatId!!, outboundMessage)
    }

    override fun edit(
        chatId: Long,
        messageId: Long,
        outboundMessage: OutboundMessage
    ): TelegramMessage {
        val response: InputMediaResponse = outboundMessage.response()

        return unwrapResult(
            bot.editMessageMedia(ChatId.fromId(chatId), messageId, null, response.data, outboundMessage.replyMarkup)
        ).first()
    }

    private fun sendAudio(
        chatId: Long,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val response: InputMediaResponse = outboundMessage.response()
        val audio: InputMediaAudio = response.data as InputMediaAudio
        return unwrapResult(when (val media: TelegramFile = audio.media) {
            is ByFile -> bot.sendAudio(
                ChatId.fromId(chatId), ByFile(media.file), audio.duration, audio.performer, audio.title,
                outboundMessage.disableNotification, outboundMessage.replyToMessageId,
                allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is ByFileId -> bot.sendAudio(
                ChatId.fromId(chatId), ByFileId(media.fileId), audio.duration, audio.performer, audio.title,
                outboundMessage.disableNotification, outboundMessage.replyToMessageId,
                allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is ByUrl -> bot.sendAudio(
                ChatId.fromId(chatId), ByUrl(media.url), audio.duration, audio.performer, audio.title,
                outboundMessage.disableNotification, outboundMessage.replyToMessageId,
                allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            else -> throw RuntimeException("unknown media type")
        })
    }

    private fun sendPhoto(
        chatId: Long,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val response: InputMediaResponse = outboundMessage.response()
        val photo: InputMediaPhoto = response.data as InputMediaPhoto
        return unwrapResult(
            bot.sendPhoto(
                ChatId.fromId(chatId), photo.media, photo.caption, outboundMessage.parseMode,
                outboundMessage.disableNotification, outboundMessage.replyToMessageId,
                allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
        )
    }

    private fun sendVideo(
        chatId: Long,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val response: InputMediaResponse = outboundMessage.response()
        val video: InputMediaVideo = response.data as InputMediaVideo
        return unwrapResult(when (val media: TelegramFile = video.media) {
            is ByFile -> bot.sendVideo(
                ChatId.fromId(chatId), ByFile(media.file), video.duration, video.width, video.height, video.caption,
                outboundMessage.disableNotification, outboundMessage.replyToMessageId,
                allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is ByFileId -> bot.sendVideo(
                ChatId.fromId(chatId), ByFileId(media.fileId), video.duration, video.width, video.height, video.caption,
                outboundMessage.disableNotification, outboundMessage.replyToMessageId,
                allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is ByUrl -> bot.sendVideo(
                ChatId.fromId(chatId), ByUrl(media.url), video.duration, video.width, video.height, video.caption,
                outboundMessage.disableNotification, outboundMessage.replyToMessageId,
                allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            else -> throw RuntimeException("unknown media type")
        })
    }

    private fun sendAnimation(
        chatId: Long,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val response: InputMediaResponse = outboundMessage.response()
        val animation: InputMediaAnimation = response.data as InputMediaAnimation
        return unwrapResult(when (val media: TelegramFile = animation.media) {
            is ByFile -> bot.sendAnimation(
                ChatId.fromId(chatId), ByFile(media.file), animation.duration, animation.width, animation.height,
                animation.caption, outboundMessage.parseMode, outboundMessage.disableNotification,
                outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is ByFileId -> bot.sendAnimation(
                ChatId.fromId(chatId), ByFileId(media.fileId), animation.duration, animation.width, animation.height,
                animation.caption, outboundMessage.parseMode, outboundMessage.disableNotification,
                outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is ByUrl -> bot.sendAnimation(
                ChatId.fromId(chatId), ByUrl(media.url), animation.duration, animation.width, animation.height,
                animation.caption, outboundMessage.parseMode, outboundMessage.disableNotification,
                outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            else -> throw RuntimeException("unknown media type")
        })
    }

    private fun sendDocument(
        chatId: Long,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val response: InputMediaResponse = outboundMessage.response()
        val document: InputMediaDocument = response.data as InputMediaDocument
        return unwrapResult(when (val media: TelegramFile = document.media) {
            is ByFile -> bot.sendDocument(
                ChatId.fromId(chatId), ByFile(media.file), document.caption, outboundMessage.parseMode,
                disableContentTypeDetection = false, outboundMessage.disableNotification,
                outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is ByFileId -> bot.sendDocument(
                ChatId.fromId(chatId), ByFileId(media.fileId), document.caption, outboundMessage.parseMode,
                disableContentTypeDetection = false, outboundMessage.disableNotification,
                outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is ByUrl -> bot.sendDocument(
                ChatId.fromId(chatId), ByUrl(media.url), document.caption, outboundMessage.parseMode,
                disableContentTypeDetection = false, outboundMessage.disableNotification,
                outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            else -> throw RuntimeException("unknown media type")
        })
    }

    private fun unwrapResult(
        result: Pair<HttpResponse<BotResponse<TelegramMessage>?>?, Exception?>
    ): List<TelegramMessage> {
        return listOf(resolveMessage(result))
    }
}
