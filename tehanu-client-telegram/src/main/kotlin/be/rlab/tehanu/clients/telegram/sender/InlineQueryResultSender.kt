package be.rlab.tehanu.clients.telegram.sender

import be.rlab.tehanu.clients.telegram.MessageSender
import be.rlab.tehanu.clients.telegram.inlinequery.model.InlineQueryResponse
import be.rlab.tehanu.clients.telegram.model.OutboundMessage
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.Message as TelegramMessage

/** Sends an inline query result.
 * @param bot Bot that sends the message.
 */
class InlineQueryResultSender(
    private val bot: Bot
) : MessageSender {

    override fun supports(outboundMessage: OutboundMessage): Boolean {
        return outboundMessage.response is InlineQueryResponse
    }

    override fun edit(
        chatId: Long,
        messageId: Long,
        outboundMessage: OutboundMessage
    ): TelegramMessage {
        throw RuntimeException("edit not supported for inline query messages")
    }

    override fun send(
        chatId: Long?,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val response: InlineQueryResponse = outboundMessage.response()

        bot.answerInlineQuery(
            inlineQueryId = requireNotNull(response.inlineContext).inlineQueryId,
            inlineQueryResults = response.data.results,
            nextOffset = response.data.nextOffset,
            isPersonal = response.data.personal,
        )

        return emptyList()
    }
}
