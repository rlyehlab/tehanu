package be.rlab.tehanu.clients.telegram.sender

import be.rlab.tehanu.clients.telegram.MessageSender
import be.rlab.tehanu.clients.telegram.ResponseUtils.resolveMessage
import be.rlab.tehanu.clients.telegram.extensions.MESSAGE_TYPE_CONTACT
import be.rlab.tehanu.clients.telegram.model.ContactEntity
import be.rlab.tehanu.clients.telegram.model.OutboundMessage
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.Message as TelegramMessage

/** Sends a contact from the address book.
 * @param bot Bot that sends the message.
 */
class ContactSender(
    private val bot: Bot
) : MessageSender {

    override fun supports(outboundMessage: OutboundMessage): Boolean {
        return outboundMessage.response.type == MESSAGE_TYPE_CONTACT
    }

    override fun edit(
        chatId: Long,
        messageId: Long,
        outboundMessage: OutboundMessage
    ): TelegramMessage {
        throw RuntimeException("edit not supported for contact")
    }

    override fun send(
        chatId: Long?,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val entity: ContactEntity = requireNotNull(outboundMessage.response.data) as ContactEntity
        // TODO(seykron): send vcard information.
        val result = bot.sendContact(
            chatId = ChatId.fromId(chatId!!),
            phoneNumber = entity.phoneNumber,
            firstName = entity.firstName,
            lastName = entity.lastName,
            disableNotification = outboundMessage.disableNotification,
            replyToMessageId = outboundMessage.replyToMessageId,
            allowSendingWithoutReply = true,
            replyMarkup = outboundMessage.replyMarkup
        )
        return listOf(resolveMessage(result))
    }
}
