package be.rlab.tehanu.clients.telegram.extensions

import be.rlab.tehanu.clients.telegram.InboundMessageFactory.Companion.CONTENT_FIELD
import be.rlab.tehanu.messages.model.EntityType
import be.rlab.tehanu.messages.model.Message
import com.github.kotlintelegrambot.entities.Contact
import com.github.kotlintelegrambot.entities.Game
import com.github.kotlintelegrambot.entities.Venue
import com.github.kotlintelegrambot.entities.dice.Dice
import com.github.kotlintelegrambot.entities.files.*
import com.github.kotlintelegrambot.entities.polls.Poll
import com.github.kotlintelegrambot.entities.stickers.Sticker

/** Returns the command. The command is in the format /command@param. This method returns
 * the command without the parameter (i.e.: /command).
 * @return the command name, including the slash.
 */
fun Message.command(): String {
    return entities.find { entity -> entity.type == EntityType.BOT_COMMAND }?.let { command ->
        command.value.substringBefore("@").trim()
    } ?: ""
}

/** Returns the command parameter. The command is in the format /command@param. This method returns
 * the parameter without the command name (i.e.: param).
 * @return the command parameter.
 */
fun Message.commandParam(): String {
    return entities.find { entity -> entity.type == EntityType.BOT_COMMAND }?.let { command ->
        command.value.substringAfter("@").trim()
    } ?: ""
}

fun Message.audio(): Audio? {
    return content.getValue(CONTENT_FIELD) as Audio?
}

fun Message.video(): Video? {
    return content.getValue(CONTENT_FIELD) as Video?
}

fun Message.animation(): Animation? {
    return content.getValue(CONTENT_FIELD) as Animation?
}

fun Message.videoNote(): VideoNote? {
    return content.getValue(CONTENT_FIELD) as VideoNote?
}

@Suppress("UNCHECKED_CAST")
fun Message.photos(): List<PhotoSize> {
    val value = content.getValue(CONTENT_FIELD)
    return when {
        value !is List<*> -> emptyList()
        value.first() !is PhotoSize -> emptyList()
        else -> value as List<PhotoSize>
    }
}

fun Message.document(): Document? {
    return content.getValue(CONTENT_FIELD) as Document?
}

fun Message.contact(): Contact? {
    return content.getValue(CONTENT_FIELD) as Contact?
}

fun Message.dice(): Dice? {
    return content.getValue(CONTENT_FIELD) as Dice?
}

fun Message.game(): Game? {
    return content.getValue(CONTENT_FIELD) as Game?
}

fun Message.sticker(): Sticker? {
    return content.getValue(CONTENT_FIELD) as Sticker?
}

fun Message.poll(): Poll? {
    return content.getValue(CONTENT_FIELD) as Poll?
}

fun Message.venue(): Venue? {
    return content.getValue(CONTENT_FIELD) as Venue?
}

fun Message.voice(): Voice? {
    return content.getValue(CONTENT_FIELD) as Voice?
}
