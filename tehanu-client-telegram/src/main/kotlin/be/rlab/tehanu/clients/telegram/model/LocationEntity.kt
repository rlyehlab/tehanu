package be.rlab.tehanu.clients.telegram.model

/** Represents a Location entity in a message.
 */
data class LocationEntity(
    /** Latitude of the location. */
    val latitude: Float,
    /** Longitude of the location */
    val longitude: Float,
    /** The radius of uncertainty for the location, measured in meters; 0-1500. */
    val horizontalAccuracy: Float?,
    /** Period in seconds for which the location will be updated.
     * See [Live Locations](https://telegram.org/blog/live-locations), should be between 60 and 86400.
     */
    val livePeriod: Int?,
    /** For live locations, a direction in which the user is moving, in degrees.
     * Must be between 1 and 360 if specified.
     */
    val heading: Int?,
    /** For live locations, a maximum distance for proximity alerts about approaching
     * another chat member, in meters. Must be between 1 and 100000 if specified.
     */
    val proximityAlertRadius: Int?
)
