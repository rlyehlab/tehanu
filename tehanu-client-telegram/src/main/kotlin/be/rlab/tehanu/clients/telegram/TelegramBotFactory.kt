package be.rlab.tehanu.clients.telegram

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.handlers.Handler
import com.github.kotlintelegrambot.entities.Update
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class TelegramBotFactory(
    private val accessToken: String
) {
    private val logger: Logger = LoggerFactory.getLogger(TelegramBotFactory::class.java)

    fun createBot(handleUpdate: (Update) -> Unit): Bot = bot {
        logger.debug("Creating bot with access token $accessToken")

        token = accessToken

        dispatch {
            addHandler (object : Handler {
                override fun checkUpdate(update: Update): Boolean {
                    return true
                }

                override suspend fun handleUpdate(bot: Bot, update: Update) {
                    try {
                        handleUpdate(update)
                    } catch (cause: Throwable) {
                        logger.error("Error handling update $update", cause)
                    }
                }
            })
        }
    }
}
