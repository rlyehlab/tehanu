package be.rlab.tehanu.clients.telegram

import com.github.kotlintelegrambot.entities.Message
import com.github.kotlintelegrambot.types.TelegramBotResult
import com.github.kotlintelegrambot.network.Response as TelegramResponse
import retrofit2.Response as HttpResponse

object ResponseUtils {
    fun resolveMessage(
        result: TelegramBotResult<Message>
    ): Message {
        return if (result.isSuccess) {
            result.getOrNull()
                ?: throw RuntimeException("Cannot retrieve response message")
        } else {
            throw RuntimeException("Telegram response error")
        }
    }

    fun resolveMessage(
        result: Pair<HttpResponse<TelegramResponse<Message>?>?, Exception?>
    ): Message {
        return if (result.first?.isSuccessful == true) {
            result.first
                ?.body()
                ?.result
                ?: throw RuntimeException("Cannot retrieve response message")
        } else {
            throw RuntimeException(result.first?.errorBody()?.string())
        }
    }
}
