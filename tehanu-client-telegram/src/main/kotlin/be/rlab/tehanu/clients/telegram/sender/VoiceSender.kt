package be.rlab.tehanu.clients.telegram.sender

import be.rlab.tehanu.clients.telegram.MessageSender
import be.rlab.tehanu.clients.telegram.ResponseUtils.resolveMessage
import be.rlab.tehanu.clients.telegram.model.OutboundMessage
import be.rlab.tehanu.clients.telegram.model.VoiceResponse
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.TelegramFile
import com.github.kotlintelegrambot.entities.Message as TelegramMessage

/** Sends a voice message.
 * @param bot Bot that sends the message.
 */
class VoiceSender(
    private val bot: Bot
) : MessageSender {

    override fun supports(outboundMessage: OutboundMessage): Boolean {
        return outboundMessage.response is VoiceResponse
    }

    override fun edit(
        chatId: Long,
        messageId: Long,
        outboundMessage: OutboundMessage
    ): TelegramMessage {
        throw RuntimeException("edit not supported for voice messages")
    }

    override fun send(
        chatId: Long?,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val response: VoiceResponse = outboundMessage.response()

        // TODO(seykron): send caption entities.
        val result = when (val media: TelegramFile = response.data) {
            is TelegramFile.ByFile -> bot.sendVoice(
                ChatId.fromId(chatId!!), TelegramFile.ByFile(media.file), response.caption, outboundMessage.parseMode,
                null, response.duration, outboundMessage.disableNotification,
                outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is TelegramFile.ByFileId -> bot.sendVoice(
                ChatId.fromId(chatId!!), TelegramFile.ByFileId(media.fileId), response.caption,
                outboundMessage.parseMode, null,
                response.duration, outboundMessage.disableNotification,
                outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            is TelegramFile.ByUrl -> bot.sendVoice(
                ChatId.fromId(chatId!!), TelegramFile.ByUrl(media.url), response.caption,
                outboundMessage.parseMode, null,
                response.duration, outboundMessage.disableNotification,
                outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
            )
            else -> throw RuntimeException("unknown media type")
        }

        return listOf(resolveMessage(result))
    }
}
