package be.rlab.tehanu.clients.telegram.model

import be.rlab.tehanu.clients.telegram.extensions.MESSAGE_TYPE_ALBUM
import be.rlab.tehanu.clients.telegram.extensions.MESSAGE_TYPE_CONTACT
import be.rlab.tehanu.clients.telegram.extensions.MESSAGE_TYPE_LOCATION
import be.rlab.tehanu.messages.model.Response
import be.rlab.tehanu.messages.model.TextResponse
import com.github.kotlintelegrambot.entities.ParseMode
import com.github.kotlintelegrambot.entities.ReplyMarkup
import com.github.kotlintelegrambot.entities.inputmedia.InputMediaTypes

data class OutboundMessage(
    val response: Response<*>,
    val parseMode: ParseMode?,
    val disableNotification: Boolean,
    val disableLinkPreview: Boolean,
    val replyMarkup: ReplyMarkup?,
    val replyToMessageId: Long?
) {
    companion object {
        fun new(
            response: Response<*>,
            disableNotification: Boolean,
            disableLinkPreview: Boolean,
            parseMode: ParseMode? = null,
            replyMarkup: ReplyMarkup? = null,
            replyToMessageId: Long? = null
        ): OutboundMessage = OutboundMessage(
            response = response,
            parseMode = parseMode,
            disableNotification = disableNotification,
            disableLinkPreview = disableLinkPreview,
            replyMarkup = replyMarkup,
            replyToMessageId = replyToMessageId
        )
    }

    val isText: Boolean = response.type == TextResponse.TYPE
    val isAudio: Boolean = response.type == InputMediaTypes.AUDIO
    val isPhoto: Boolean = response.type == InputMediaTypes.PHOTO
    val isVideo: Boolean = response.type == InputMediaTypes.VIDEO
    val isDocument: Boolean = response.type == InputMediaTypes.DOCUMENT
    val isVoice: Boolean = response.type == VoiceResponse.TYPE
    val isAnimation: Boolean = response.type == InputMediaTypes.ANIMATION
    val isContact: Boolean = response.type == MESSAGE_TYPE_CONTACT
    val isAlbum: Boolean = response.type == MESSAGE_TYPE_ALBUM
    val isLocation: Boolean = response.type == MESSAGE_TYPE_LOCATION

    @Suppress("UNCHECKED_CAST")
    fun<T> response(): T = response as T
}
