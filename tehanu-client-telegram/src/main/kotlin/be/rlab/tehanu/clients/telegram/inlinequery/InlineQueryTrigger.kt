package be.rlab.tehanu.clients.telegram.inlinequery

import be.rlab.tehanu.clients.Trigger
import be.rlab.tehanu.clients.Update
import be.rlab.tehanu.clients.telegram.inlinequery.ModelUtils.getInlineQueryFlow
import be.rlab.tehanu.clients.telegram.inlinequery.model.InlineQueryFlow
import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.support.TriggerSupport
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import be.rlab.tehanu.annotations.Trigger as TriggerAnnotation

/** Triggers handlers for Telegram inline queries.
 */
class InlineQueryTrigger(override val name: String) : Trigger {

    companion object : TriggerSupport() {
        private val logger: Logger = LoggerFactory.getLogger(InlineQueryTrigger::class.java)

        override fun new(
            name: String,
            triggerConfig: TriggerAnnotation
        ): Trigger {
            return InlineQueryTrigger(name = name)
        }
    }

    override fun applies(
        update: Update,
        language: Language
    ): Boolean {
        logger.debug("checking if the update is part of the inline query flow")
        return getInlineQueryFlow(update) == InlineQueryFlow.SEARCH_QUERY
    }
}
