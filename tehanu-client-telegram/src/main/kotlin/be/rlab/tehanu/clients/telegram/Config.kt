package be.rlab.tehanu.clients.telegram

import be.rlab.tehanu.config.ClientConfigBuilder
import be.rlab.tehanu.media.MediaManager

const val TELEGRAM_CLIENT_NAME: String = "TelegramClient"

fun ClientConfigBuilder.telegram(accessToken: String) {
    addClient {
        serviceProvider.registerService(MediaManager::class) { FileManager(accessToken) }
        TelegramClient(
            name = TELEGRAM_CLIENT_NAME,
            serviceProvider = serviceProvider,
            inboundMessageFactory = InboundMessageFactory(),
            botFactory = TelegramBotFactory(accessToken)
        )
    }
}
