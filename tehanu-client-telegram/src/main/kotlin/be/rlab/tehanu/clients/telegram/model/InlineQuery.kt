package be.rlab.tehanu.clients.telegram.model

/** An inline query represents a user input that produces a dynamic result.
 * It is like a typeahead control that let users ask for contextual information.
 */
data class InlineQuery(
    /** Unique id for this query assigned by a [Client]. */
    val id: String,
    /** Query introduced by the user. */
    val query: String,
    /** Offset set by a previous inline query. */
    val offset: String
)
