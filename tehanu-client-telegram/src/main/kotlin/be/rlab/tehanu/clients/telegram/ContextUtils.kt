package be.rlab.tehanu.clients.telegram

import be.rlab.tehanu.clients.UpdateContext

/** Escapes markdown and html entities from a text.
 */
fun UpdateContext.escape(text: String): String {
    val replacements: Map<String, String> = mapOf(
        "_" to "\\_",
        "*" to "\\*",
        "~" to "\\~",
        "[" to "\\[",
        "]" to "\\]",
        "`" to "\\`",
    )
    return replacements.entries.fold(text) { result, entry ->
        result.replace(entry.key, entry.value)
    }
}
