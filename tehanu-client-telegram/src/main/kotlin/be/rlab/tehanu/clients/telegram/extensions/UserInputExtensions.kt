package be.rlab.tehanu.clients.telegram.extensions

import be.rlab.tehanu.view.UserInput

const val FEEDBACK_MESSAGE = "feedbackMessage"
const val IS_ALERT = "isAlert"

/** Displays an alert message in the Telegram client.
 * @param message Message to display.
 */
fun UserInput.alert(message: String): UserInput = apply {
    attributes[FEEDBACK_MESSAGE] = message
    attributes[IS_ALERT] = true
}

/** Displays a notification message at the top of the Telegram chat.
 * @param message Message to display.
 */
fun UserInput.notify(message: String): UserInput = apply {
    attributes[FEEDBACK_MESSAGE] = message
    attributes[IS_ALERT] = false
}
