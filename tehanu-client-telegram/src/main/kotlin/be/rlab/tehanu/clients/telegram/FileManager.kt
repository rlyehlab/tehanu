package be.rlab.tehanu.clients.telegram

import be.rlab.tehanu.media.MediaManager
import be.rlab.tehanu.media.model.MediaFile
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.InputStream
import java.net.URL

class FileManager(
    private val accessToken: String
) : MediaManager() {
    companion object {
        const val DOWNLOAD_URL: String = "https://api.telegram.org/file/bot<token>/<file_path>"
    }
    lateinit var client: TelegramClient
    private val httpClient = OkHttpClient()

    override fun findById(fileId: String): MediaFile? {
        TODO("Not yet implemented")
    }

    override fun readContent(mediaFile: MediaFile): InputStream {
        val file = client.getFile(mediaFile)
        val url = URL(DOWNLOAD_URL
            .replace("<token>", accessToken)
            .replace("<file_path>", file.filePath!!)
        )
        val request = Request.Builder()
            .url(url)
            .get()
            .build()
        return httpClient.newCall(request).execute().body()?.byteStream()
            ?: throw RuntimeException("cannot download file: ${mediaFile.id}")
    }

    override fun store(
        mediaFile: MediaFile,
        content: InputStream
    ): MediaFile {
        TODO("Not yet implemented")
    }

    override fun update(mediaFile: MediaFile): MediaFile {
        TODO("Not yet implemented")
    }
}
