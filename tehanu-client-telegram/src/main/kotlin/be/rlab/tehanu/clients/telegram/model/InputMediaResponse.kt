package be.rlab.tehanu.clients.telegram.model

import be.rlab.tehanu.messages.model.Response
import com.github.kotlintelegrambot.entities.inputmedia.InputMedia
import com.github.kotlintelegrambot.entities.inputmedia.InputMediaTypes

/** Represents animation files (GIF or H.264/MPEG-4 AVC video without sound). Bots can currently send
 * animation files of up to 50 MB in size, this limit may be changed in the future.
 */
data class InputMediaResponse(
    /** The input media content. */
    override val data: InputMedia,
    /** Response type, one of the [InputMediaTypes]. */
    override val type: String
) : Response<InputMedia>
