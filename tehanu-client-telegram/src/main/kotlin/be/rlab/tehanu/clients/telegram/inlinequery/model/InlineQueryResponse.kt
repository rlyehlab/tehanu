package be.rlab.tehanu.clients.telegram.inlinequery.model

import be.rlab.tehanu.clients.telegram.inlinequery.InlineQueryContext
import be.rlab.tehanu.messages.model.Response
import be.rlab.tehanu.view.ui.Keyboard
import be.rlab.tehanu.view.ui.KeyboardMode

/** Represents a response to an inline query.
 */
data class InlineQueryResponse(
    /** The inline query response. */
    override val data: InlineQueryResponseData,
    var keyboard: Keyboard?,
    var inlineContext: InlineQueryContext?
) : Response<InlineQueryResponseData> {
    companion object {
        const val TYPE: String = "inlineQuery"

        fun new(data: InlineQueryResponseData): InlineQueryResponse = InlineQueryResponse(
            data = data,
            keyboard = null,
            inlineContext = null
        )
    }

    override val type: String = TYPE

    fun withInlineContext(inlineContext: InlineQueryContext): InlineQueryResponse = copy(
        inlineContext = inlineContext
    )

    /** Attaches a keyboard to the inline query response.
     * @param callback Callback to build the keyboard.
     * @return this field.
     */
    fun keyboard(
        mode: KeyboardMode = KeyboardMode.SINGLE_SELECTION,
        callback: Keyboard.() -> Unit
    ): InlineQueryResponse = apply {
        keyboard = Keyboard.new(mode, null, renderCallback = callback)
    }
}
