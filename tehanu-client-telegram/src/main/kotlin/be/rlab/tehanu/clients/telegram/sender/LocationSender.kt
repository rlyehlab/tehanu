package be.rlab.tehanu.clients.telegram.sender

import be.rlab.tehanu.clients.telegram.MessageSender
import be.rlab.tehanu.clients.telegram.ResponseUtils.resolveMessage
import be.rlab.tehanu.clients.telegram.extensions.MESSAGE_TYPE_LOCATION
import be.rlab.tehanu.clients.telegram.model.LocationEntity
import be.rlab.tehanu.clients.telegram.model.OutboundMessage
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.Message as TelegramMessage

/** Sends a location in a map.
 * @param bot Bot that sends the message.
 */
class LocationSender(
    private val bot: Bot
) : MessageSender {

    override fun supports(outboundMessage: OutboundMessage): Boolean {
        return outboundMessage.response.type == MESSAGE_TYPE_LOCATION
    }

    override fun edit(
        chatId: Long,
        messageId: Long,
        outboundMessage: OutboundMessage
    ): TelegramMessage {
        val entity: LocationEntity = requireNotNull(outboundMessage.response.data) as LocationEntity
        return bot.editMessageLiveLocation(
            ChatId.fromId(chatId), messageId, null,
            entity.latitude, entity.longitude, outboundMessage.replyMarkup
        ).first
            ?.body()
            ?.result
            ?: throw RuntimeException("Cannot retrieve message response")
    }

    override fun send(
        chatId: Long?,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val entity: LocationEntity = requireNotNull(outboundMessage.response.data) as LocationEntity
        // TODO(seykron): send missing fields.
        val result = bot.sendLocation(
            ChatId.fromId(chatId!!), entity.latitude, entity.longitude, entity.livePeriod,
            outboundMessage.disableNotification, outboundMessage.replyToMessageId,
            allowSendingWithoutReply = true, outboundMessage.replyMarkup
        )
        return listOf(resolveMessage(result))
    }
}
