package be.rlab.tehanu.clients.telegram.view.ui

import be.rlab.tehanu.clients.telegram.InboundMessageFactory.Companion.CONTENT_FIELD
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators.assertTrue
import be.rlab.tehanu.view.ui.Input
import be.rlab.tehanu.view.ui.input
import com.github.kotlintelegrambot.entities.stickers.Sticker

fun UserInput.sticker(
    description: MessageBuilder,
    callback: (Input.() -> Unit) = {}
) = input(description) {

    resolveValue {
        setValue(incomingMessage[CONTENT_FIELD])
    }

    validator {
        assertTrue(incomingMessage.content[CONTENT_FIELD] is Sticker, "No es un sticker.")
    }

    callback(this)
}
