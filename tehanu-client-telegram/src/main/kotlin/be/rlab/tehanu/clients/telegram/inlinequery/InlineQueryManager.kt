package be.rlab.tehanu.clients.telegram.inlinequery

import be.rlab.tehanu.clients.*
import be.rlab.tehanu.clients.model.Action
import be.rlab.tehanu.clients.telegram.TelegramClient
import be.rlab.tehanu.clients.telegram.inlinequery.ModelUtils.configureInlineQueryMarkup
import be.rlab.tehanu.clients.telegram.inlinequery.ModelUtils.getCallbackQueryData
import be.rlab.tehanu.clients.telegram.inlinequery.ModelUtils.getInlineQueryFlow
import be.rlab.tehanu.clients.telegram.inlinequery.ModelUtils.getInlineQueryId
import be.rlab.tehanu.clients.telegram.inlinequery.model.InlineQueryFlow
import be.rlab.tehanu.clients.telegram.inlinequery.model.InlineQueryResponse
import be.rlab.tehanu.clients.telegram.model.InlineQuery
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.ui.Control
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import com.github.kotlintelegrambot.entities.Update as TelegramUpdate

class InlineQueryManager(
    private val client: TelegramClient
) {
    companion object {
        const val INLINE_QUERY_FLOW_FIELD = "inlineQueryFlow"
        const val INLINE_QUERY_FIELD = "inlineQuery"
        const val INLINE_QUERY_ID_FIELD = "inlineQueryId"
        const val INLINE_QUERY_RESULT_FIELD = "inlineQueryResult"
        const val INLINE_QUERY_STATE_ATTRIBUTE = "isInlineQuery"
    }

    private val logger: Logger = LoggerFactory.getLogger(InlineQueryManager::class.java)
    private val stateManager: StateManager by lazy { client.serviceProvider.getService(StateManager::class) }
    private val inlineContextCache: MutableMap<String, InlineQueryContext> = mutableMapOf()

    /** Handles a [TelegramUpdate] from an [InlineQuery].
     * @param update Telegram update to handle.
     */
    fun resolveUpdate(update: TelegramUpdate): Update? {
        val inlineQueryId = getInlineQueryId(update) ?: return null
        val newContext = InlineQueryContext.new(inlineQueryId)
        val inlineContext = inlineContextCache.putIfAbsent(inlineQueryId, newContext) ?: newContext

        return when (getInlineQueryFlow(update)) {
            InlineQueryFlow.SEARCH_QUERY -> resolveSearchUpdate(inlineContext, update)
            InlineQueryFlow.CALLBACK_MESSAGE -> resolveCallbackMessageUpdate(inlineContext, update)
            InlineQueryFlow.USER_ACTION -> resolveUserActionUpdate(inlineContext, update)
            InlineQueryFlow.CHOSEN_RESULT -> resolveChosenResultUpdate(inlineContext, update)
            InlineQueryFlow.INVALID_FLOW -> null
        }
    }

    fun postProcessUpdate(update: Update, state: State?): State? {
        return when (getInlineQueryFlow(update)) {
            InlineQueryFlow.SEARCH_QUERY -> state?.let {
                stateManager.updateState(state.id, state.setAttribute(INLINE_QUERY_STATE_ATTRIBUTE, true))
            }

            InlineQueryFlow.CALLBACK_MESSAGE ->
                stateManager.findByAttribute(INLINE_QUERY_STATE_ATTRIBUTE, true)
                    .firstOrNull()
                    ?.updateIfRequired(update.chat, update.user)

            else -> state
        }
    }

    fun sendMessage(context: UpdateContext, message: MessageBuilder): List<Message> {
        require(message.response is InlineQueryResponse) { "message has no inline query response" }
        val response: InlineQueryResponse = message.response as InlineQueryResponse
        val inlineContext: InlineQueryContext = inlineContextCache.getValue(getInlineQueryId(context.update))

        if (response.keyboard != null) {
            // If there is an inline keyboard attached, we don't wait for the regular user input flow.
            // It forces the keyboard rendering to attach the reply markup to the response.
            val userInput = UserInput.new().addChild(
                requireNotNull(response.keyboard).render(context)
            ) as UserInput
            context.addUserInput(userInput)
            updateInlineQueryContext(
                inlineContext.withInput(userInput)
            )
        }
        client.doSendMessage(
            message.custom(
                configureInlineQueryMarkup(inlineContext, response)
            )
        )
        return listOf(inlineContext.inlineQueryMessage)
    }

    fun renderControl(
        context: UpdateContext,
        control: Control
    ): Boolean {
        logger.debug("rendering control: ${control.description}")
        return getInlineQueryFlow(context.update) != InlineQueryFlow.INVALID_FLOW && context.state.final
    }

    private fun resolveSearchUpdate(
        inlineContext: InlineQueryContext,
        update: TelegramUpdate
    ): Update {
        val inlineQuery = requireNotNull(update.inlineQuery)
        val user = client.resolveUser(inlineQuery.from)
        updateInlineQueryContext(
            inlineContext.withUser(user)
        )
        return Update.new(
            updateId = update.updateId,
            user = user,
            attributes = mapOf(
                INLINE_QUERY_FLOW_FIELD to getInlineQueryFlow(update),
                INLINE_QUERY_ID_FIELD to inlineQuery.id,
                INLINE_QUERY_FIELD to InlineQuery(
                    id = inlineQuery.id,
                    query = inlineQuery.query,
                    offset = inlineQuery.offset
                )
            )
        )
    }

    private fun resolveChosenResultUpdate(
        inlineContext: InlineQueryContext,
        update: TelegramUpdate
    ): Update {
        val chosenInlineResult = requireNotNull(update.chosenInlineResult)
        val user = client.resolveUser(chosenInlineResult.from)
        updateInlineQueryContext(
            inlineContext
                .withSelectedResultId(chosenInlineResult.resultId)
                .withUser(user)
        )
        return Update.new(
            updateId = update.updateId,
            attributes = mapOf(
                TelegramClient.SKIP_UPDATE_FIELD to true
            )
        )
    }

    private fun resolveCallbackMessageUpdate(
        inlineContext: InlineQueryContext,
        update: TelegramUpdate
    ): Update {
        val inlineQueryId = inlineContext.inlineQueryId
        val message = requireNotNull(update.message)
        val chat = client.resolveChat(message.chat)
        updateInlineQueryContext(
            inlineContext
                .withChat(chat)
                .withUser(client.resolveUser(message.from!!, chat))
                .withResultMessageId(message.messageId)
        )
        return client.resolveChatUpdate(
            update, mapOf(
                INLINE_QUERY_ID_FIELD to inlineQueryId,
                INLINE_QUERY_FLOW_FIELD to getInlineQueryFlow(update)
            )
        )
    }

    private fun resolveUserActionUpdate(
        inlineContext: InlineQueryContext,
        update: TelegramUpdate
    ): Update {
        val callbackQuery = requireNotNull(update.callbackQuery)
        val inlineQueryId = inlineContext.inlineQueryId
        val action = Action(
            id = callbackQuery.id,
            data = getCallbackQueryData(callbackQuery)
        )
        if (callbackQuery.inlineMessageId != null) {
            inlineContextCache.remove(callbackQuery.inlineMessageId)?.let { inlineContextToMerge ->
                if (inlineContextToMerge.selectedResultId != null) {
                    updateInlineQueryContext(
                        inlineContext.withSelectedResultId(inlineContextToMerge.selectedResultId)
                    )
                }
            }
        }
        return Update.new(
            updateId = update.updateId,
            chat = requireNotNull(inlineContext.chat) {
                "cannot resolve inline query chat: inlineQueryId=$inlineQueryId"
            },
            user = client.resolveUser(callbackQuery.from),
            message = inlineContext.inlineQueryMessage,
            action = action,
            attributes = mapOf(
                INLINE_QUERY_FLOW_FIELD to InlineQueryFlow.USER_ACTION,
                INLINE_QUERY_ID_FIELD to inlineQueryId,
                INLINE_QUERY_RESULT_FIELD to inlineContextCache.getValue(inlineQueryId).selectedResultId!!
            )
        )
    }

    private fun updateInlineQueryContext(inlineQueryContext: InlineQueryContext) {
        inlineContextCache.replace(inlineQueryContext.inlineQueryId, inlineQueryContext)
    }
}
