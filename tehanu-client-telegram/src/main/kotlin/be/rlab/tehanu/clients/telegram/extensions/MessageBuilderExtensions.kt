package be.rlab.tehanu.clients.telegram.extensions

import be.rlab.tehanu.clients.telegram.inlinequery.model.InlineQueryResponse
import be.rlab.tehanu.clients.telegram.inlinequery.model.InlineQueryResponseData
import be.rlab.tehanu.clients.telegram.model.*
import be.rlab.tehanu.media.MediaManager
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.Response
import be.rlab.tehanu.support.Validations
import com.github.kotlintelegrambot.entities.ParseMode
import com.github.kotlintelegrambot.entities.ReplyMarkup
import com.github.kotlintelegrambot.entities.TelegramFile
import com.github.kotlintelegrambot.entities.inputmedia.*
import java.io.InputStream
import kotlin.reflect.KClass

/** An album is a collection of media items. */
const val MESSAGE_TYPE_ALBUM: String = "album"

/** Contact from address book. */
const val MESSAGE_TYPE_CONTACT: String = "contact"

/** Sends a location message.
 * See [LocationEntity] for further information.
 */
const val MESSAGE_TYPE_LOCATION: String = "location"

/** Mode for parsing entities in the caption. */
const val PARSE_MODE: String = "parseMode"

/** Sends the message silently. Users will receive a notification with no sound. */
const val DISABLE_NOTIFICATION: String = "disableNotification"
const val REPLY_MARKUP: String = "replyMarkup"
const val DISABLE_LINK_PREVIEW: String = "disableLinkPreview"

/** Sends a photo.
 * @param photoRef Either a URL or a client-specific file id.
 * @param caption Text displayed with the photo.
 */
fun MessageBuilder.photo(photoRef: String, caption: String? = null): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.PHOTO,
            data = InputMediaPhoto(
                media = createTelegramFile(photoRef),
                caption = caption
            )
        )
    )

/** Sends a photo.
 * @param photo Input stream to read the photo.
 * @param caption Text displayed with the photo.
 */
fun MessageBuilder.photo(photo: InputStream, caption: String? = null): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.PHOTO,
            data = InputMediaPhoto(
                media = createTelegramFile(photo),
                caption = caption
            )
        )
    )

/** Sends an audio file.
 * @param audioRef Either a URL or a client-specific file id.
 * @param caption Text displayed with the audio.
 */
fun MessageBuilder.audio(audioRef: String, caption: String? = null): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.AUDIO,
            data = InputMediaAudio(
                media = createTelegramFile(audioRef),
                caption = caption
            )
        )
    )

/** Sends an audio file.
 * @param audio Input stream to read the audio.
 * @param caption Text displayed with the audio.
 */
fun MessageBuilder.audio(audio: InputStream, caption: String? = null): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.AUDIO,
            data = InputMediaAudio(
                media = createTelegramFile(audio),
                caption = caption
            )
        )
    )

/** Sends an audio file.
 * @param audioRef Either a URL or a client-specific file id.
 * @param duration Duration of the audio in seconds.
 * @param performer Performer name.
 * @param title Track name.
 */
fun MessageBuilder.audio(
    audioRef: String,
    duration: Int? = null,
    performer: String? = null,
    title: String? = null
): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.AUDIO,
            data = InputMediaAudio(
                media = createTelegramFile(audioRef),
                duration = duration,
                performer = performer,
                title = title
            )
        )
    )

/** Sends an audio file.
 * @param audio Input stream to read the audio.
 * @param duration Duration of the audio in seconds.
 * @param performer Performer name.
 * @param title Track name.
 */
fun MessageBuilder.audio(
    audio: InputStream,
    duration: Int? = null,
    performer: String? = null,
    title: String? = null
): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.AUDIO,
            data = InputMediaAudio(
                media = createTelegramFile(audio),
                duration = duration,
                performer = performer,
                title = title
            )
        )
    )

/** Sends a video.
 * @param videoRef Either a URL or a client-specific file id.
 * @param caption Text displayed with the video.
 */
fun MessageBuilder.video(
    videoRef: String,
    caption: String? = null,
    duration: Int? = null,
    width: Int? = null,
    height: Int? = null,
    supportsStreaming: Boolean
): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.VIDEO,
            data = InputMediaVideo(
                media = createTelegramFile(videoRef),
                caption = caption,
                duration = duration,
                width = width,
                height = height,
                supportsStreaming = supportsStreaming
            )
        )
    )

/** Sends a photo.
 * @param video Input stream to read the video.
 * @param caption Text displayed with the video.
 */
fun MessageBuilder.video(
    video: InputStream,
    caption: String? = null,
    duration: Int? = null,
    width: Int? = null,
    height: Int? = null,
    supportsStreaming: Boolean
): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.VIDEO,
            data = InputMediaVideo(
                media = createTelegramFile(video),
                caption = caption,
                duration = duration,
                width = width,
                height = height,
                supportsStreaming = supportsStreaming
            )
        )
    )

/** Sends a document.
 * @param documentRef Either a URL or a client-specific file id.
 * @param caption Text displayed with the document.
 */
fun MessageBuilder.document(documentRef: String, caption: String? = null): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.DOCUMENT,
            data = InputMediaDocument(
                media = createTelegramFile(documentRef),
                caption = caption
            )
        )
    )

/** Sends a document.
 * @param document Input stream to read the document.
 * @param caption Text displayed with the document.
 */
fun MessageBuilder.document(document: InputStream, caption: String? = null): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.DOCUMENT,
            data = InputMediaDocument(
                media = createTelegramFile(document),
                caption = caption
            )
        )
    )

/** Sends animation files.
 * It supports GIF or H.264/MPEG-4 AVC video without sound.
 * Bots can currently send animation files of up to 50 MB in size, this limit may be changed in the future.
 * @param animationRef Either a URL or a client-specific file id.
 * @param caption Text displayed with the voice message.
 */
fun MessageBuilder.animation(
    animationRef: String,
    caption: String? = null,
    duration: Int? = null,
    width: Int? = null,
    height: Int? = null
): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.ANIMATION,
            data = InputMediaAnimation(
                media = createTelegramFile(animationRef),
                caption = caption,
                duration = duration,
                width = width,
                height = height
            )
        )
    )

/** Sends animation files.
 * It supports GIF or H.264/MPEG-4 AVC video without sound.
 * Bots can currently send animation files of up to 50 MB in size, this limit may be changed in the future.
 * @param voice Input stream to read the animation file.
 * @param caption Text displayed with the voice message.
 */
fun MessageBuilder.animation(
    animation: InputStream,
    caption: String? = null,
    duration: Int? = null,
    width: Int? = null,
    height: Int? = null,
): MessageBuilder =
    custom(
        InputMediaResponse(
            type = InputMediaTypes.ANIMATION,
            data = InputMediaAnimation(
                media = createTelegramFile(animation),
                caption = caption,
                duration = duration,
                width = width,
                height = height
            )
        )
    )

/** Sends a voice message.
 * The file constraints and how to display the voice message is client-specific.
 * @param voiceRef Either a URL or a client-specific file id.
 * @param caption Text displayed with the voice message.
 * @param duration Duration of the voice message in seconds.
 */
fun MessageBuilder.voice(
    voiceRef: String,
    caption: String? = null,
    duration: Int? = null
): MessageBuilder =
    custom(
        VoiceResponse(
            data = createTelegramFile(voiceRef),
            caption = caption,
            duration = duration
        )
    )

/** Sends a voice message.
 * The file constraints and how to display the voice message is client-specific.
 * @param voice Input stream to read the audio file.
 * @param caption Text displayed with the voice message.
 * @param duration Duration of the voice message in seconds.
 */
fun MessageBuilder.voice(
    voice: InputStream,
    caption: String? = null,
    duration: Int? = null
): MessageBuilder =
    custom(
        VoiceResponse(
            data = createTelegramFile(voice),
            caption = caption,
            duration = duration
        )
    )

/** Sends a group of photos as an album.
 *
 * @param itemsRefs List of URLs or a client-specific ids of the media to send.
 */
fun MessageBuilder.photoAlbum(vararg itemsRefs: String): MessageBuilder =
    custom(albumResponse(itemsRefs.toList(), InputMediaPhoto::class))

/** Sends a group of photos as an album.
 *
 * @param items List of streams to read the media items.
 */
fun MessageBuilder.photoAlbum(vararg items: InputStream): MessageBuilder =
    custom(albumResponse(items.toList(), InputMediaPhoto::class))

/** Sends a group of videos as an album.
 *
 * @param itemsRefs List of URLs or a client-specific ids of the media to send.
 */
fun MessageBuilder.videoAlbum(vararg itemsRefs: String): MessageBuilder =
    custom(albumResponse(itemsRefs.toList(), InputMediaPhoto::class))

/** Sends a group of videos as an album.
 *
 * @param items List of streams to read the media items.
 */
fun MessageBuilder.videoAlbum(vararg items: InputStream): MessageBuilder =
    custom(albumResponse(items.toList(), InputMediaPhoto::class))

/** Sends a contact from the address book.
 *
 * @param firstName Contact first name.
 * @param lastName Contact last name.
 * @param phoneNumber Contact phone number.
 * @param vCard Additional vcard information.
 */
fun MessageBuilder.contact(
    phoneNumber: String,
    firstName: String,
    lastName: String?,
    vCard: String? = null
): MessageBuilder =
    custom(object : Response<ContactEntity> {
        override val type: String = MESSAGE_TYPE_CONTACT
        override val data: ContactEntity = ContactEntity(
            phoneNumber = phoneNumber,
            firstName = firstName,
            lastName = lastName,
            vCard = vCard
        )
    })

/** Sends a location.
 *
 * @param latitude Latitude of the location.
 * @param longitude Longitude of the location.
 * @param horizontalAccuracy The radius of uncertainty for the location, measured in meters; 0-1500.
 * @param livePeriod Period in seconds for which the location will be updated, should be between 60 and 86400.
 * @param heading For live locations, a maximum distance for proximity alerts about approaching
 * another chat member, in meters. Must be between 1 and 100000 if specified.
 * @param proximityAlertRadius For live locations, a maximum distance for proximity alerts about approaching
 * another chat member, in meters. Must be between 1 and 100000 if specified.
 */
fun MessageBuilder.location(
    latitude: Float,
    longitude: Float,
    horizontalAccuracy: Float? = null,
    livePeriod: Int? = null,
    heading: Int? = null,
    proximityAlertRadius: Int? = null
): MessageBuilder =
    custom(object : Response<LocationEntity> {
        override val type: String = MESSAGE_TYPE_LOCATION
        override val data: LocationEntity = LocationEntity(
            latitude, longitude, horizontalAccuracy, livePeriod, heading, proximityAlertRadius
        )
    })

/** Mode for parsing entities in the animation caption.
 * See [formatting options](https://core.telegram.org/bots/api#formatting-options) for more details.
 * @param parseMode Parse mode to use for parsing the message.
 */
fun MessageBuilder.parseMode(parseMode: ParseMode): MessageBuilder =
    options(PARSE_MODE to parseMode)

/** Sends the message silently. Users will receive a notification with no sound.
 */
fun MessageBuilder.disableNotification(): MessageBuilder =
    options(DISABLE_NOTIFICATION to true)

/** Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard,
 * instructions to remove reply keyboard or to force a reply from the user.
 * @param replyMarkup One of the supported reply markups.
 */
fun MessageBuilder.replyMarkup(replyMarkup: ReplyMarkup): MessageBuilder =
    options(REPLY_MARKUP to replyMarkup)

/** Disables link previews for links in this message.
 */
fun MessageBuilder.disableLinkPreview(): MessageBuilder =
    options(DISABLE_LINK_PREVIEW to true)

private fun MessageBuilder.createTelegramFile(content: Any): TelegramFile {
    return when {
        content is InputStream -> TelegramFile.ByFile(MediaManager.writeToFile(content))
        content is String && Validations.isValidUrl(content) -> TelegramFile.ByUrl(content)
        content is String -> TelegramFile.ByFileId(content)
        else -> throw RuntimeException("invalid content type, only String and InputStream supported")
    }
}

private fun MessageBuilder.albumResponse(
    itemsRefs: List<Any>,
    mediaType: KClass<*>
): Response<List<InputMedia>> {
    return object : Response<List<InputMedia>> {
        override val type: String = MESSAGE_TYPE_ALBUM
        override val data: List<InputMedia> = itemsRefs.map { itemRef ->
            when (mediaType) {
                InputMediaPhoto::class -> InputMediaPhoto(media = createTelegramFile(itemRef))
                InputMediaVideo::class -> InputMediaVideo(media = createTelegramFile(itemRef))
                else -> throw RuntimeException("only photos and video albums supported")
            }
        }
    }
}

/** Sends an inline query result.
 * @param data Inline query response data.
 * @param callback Callback to configure the response.
 */
fun MessageBuilder.inlineQueryResponse(
    data: InlineQueryResponseData,
    callback: InlineQueryResponse.() -> Unit = {}
): MessageBuilder = custom(
    InlineQueryResponse.new(data).apply(callback)
)
