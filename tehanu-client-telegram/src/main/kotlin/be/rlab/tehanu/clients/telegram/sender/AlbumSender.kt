package be.rlab.tehanu.clients.telegram.sender

import be.rlab.tehanu.clients.telegram.MessageSender
import be.rlab.tehanu.clients.telegram.extensions.MESSAGE_TYPE_ALBUM
import be.rlab.tehanu.clients.telegram.model.OutboundMessage
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.Message as TelegramMessage

/** Sends an album of media items.
 * @param bot Bot that sends the message.
 */
class AlbumSender(
    private val bot: Bot
) : MessageSender {

    override fun supports(outboundMessage: OutboundMessage): Boolean {
        return outboundMessage.response.type == MESSAGE_TYPE_ALBUM
    }

    override fun edit(
        chatId: Long,
        messageId: Long,
        outboundMessage: OutboundMessage
    ): TelegramMessage {
        throw RuntimeException("edit not supported for albums")
    }

    override fun send(
        chatId: Long?,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        TODO("not yet implemented")
    }
}
