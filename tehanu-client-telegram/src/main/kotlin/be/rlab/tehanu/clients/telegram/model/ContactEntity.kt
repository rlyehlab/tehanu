package be.rlab.tehanu.clients.telegram.model

/** Represents a contact in the address book.
 */
data class ContactEntity(
    val phoneNumber: String,
    val firstName: String,
    val lastName: String?,
    val vCard: String?
)
