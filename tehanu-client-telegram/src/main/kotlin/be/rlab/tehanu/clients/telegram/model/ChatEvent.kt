package be.rlab.tehanu.clients.telegram.model

import be.rlab.tehanu.messages.model.ChatEvent

object EventNames {
    const val deleteChatPhoto = "deleteChatPhoto"
    const val groupChatCreated = "groupChatCreated"
    const val channelChatCreated = "channelChatCreated"
    const val newChatTitle = "newChatTitle"
    const val newChatPhoto = "newChatPhoto"
    const val newChatMembers = "newChatMembers"
    const val leftChatMember = "leftChatMember"
}

fun ChatEvent.deleteChatPhoto(): Boolean = name == EventNames.deleteChatPhoto
fun ChatEvent.groupChatCreated(): Boolean = name == EventNames.groupChatCreated
fun ChatEvent.channelChatCreated(): Boolean = name == EventNames.channelChatCreated
fun ChatEvent.newChatTitle(): Boolean = name == EventNames.newChatTitle
fun ChatEvent.newChatPhoto(): Boolean = name == EventNames.newChatPhoto
fun ChatEvent.newChatMembers(): Boolean = name == EventNames.newChatMembers
fun ChatEvent.leftChatMember(): Boolean = name == EventNames.leftChatMember
