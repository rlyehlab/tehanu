package be.rlab.tehanu.clients.telegram.inlinequery

import be.rlab.tehanu.clients.Update
import be.rlab.tehanu.clients.telegram.inlinequery.InlineQueryManager.Companion.INLINE_QUERY_FLOW_FIELD
import be.rlab.tehanu.clients.telegram.inlinequery.InlineQueryManager.Companion.INLINE_QUERY_ID_FIELD
import be.rlab.tehanu.clients.telegram.inlinequery.model.InlineQueryFlow
import be.rlab.tehanu.clients.telegram.inlinequery.model.InlineQueryResponse
import be.rlab.tehanu.clients.telegram.view.ui.toInlineKeyboard
import com.github.kotlintelegrambot.entities.CallbackQuery
import com.github.kotlintelegrambot.entities.InlineKeyboardMarkup
import com.github.kotlintelegrambot.entities.keyboard.InlineKeyboardButton
import com.github.kotlintelegrambot.entities.Update as TelegramUpdate

object ModelUtils {
    fun getInlineQueryId(update: TelegramUpdate): String? {
        return when (getInlineQueryFlow(update)) {
            InlineQueryFlow.CALLBACK_MESSAGE -> {
                val replyMarkup = update.message?.replyMarkup as InlineKeyboardMarkup
                val button = replyMarkup.inlineKeyboard.flatten().first { button ->
                    button is InlineKeyboardButton.CallbackData
                    && button.callbackData.startsWith(InlineQueryContext.INLINE_QUERY_SIGNATURE)
                }
                require(button is InlineKeyboardButton.CallbackData)
                button.callbackData.substringAfter(InlineQueryContext.INLINE_QUERY_SIGNATURE).substringBefore("!!!")
            }
            InlineQueryFlow.SEARCH_QUERY -> update.inlineQuery!!.id
            InlineQueryFlow.USER_ACTION -> {
                val callbackQuery = requireNotNull(update.callbackQuery)
                callbackQuery.data.substringAfter(InlineQueryContext.INLINE_QUERY_SIGNATURE).substringBefore("!!!")
            }
            InlineQueryFlow.CHOSEN_RESULT -> update.chosenInlineResult!!.inlineMessageId!!
            InlineQueryFlow.INVALID_FLOW -> null
        }
    }

    fun getInlineQueryId(update: Update): String {
        require(update.attributes.containsKey(INLINE_QUERY_ID_FIELD)) {
            "this operation is only supported for the inline query flow"
        }
        return update.attributes.getValue(INLINE_QUERY_ID_FIELD) as String
    }

    fun getInlineQueryFlow(update: TelegramUpdate): InlineQueryFlow {
        return if (isInlineQueryMessage(update)) {
            InlineQueryFlow.CALLBACK_MESSAGE
        } else if (update.callbackQuery?.inlineMessageId != null) {
            InlineQueryFlow.USER_ACTION
        } else if (update.inlineQuery != null) {
            InlineQueryFlow.SEARCH_QUERY
        } else if (update.chosenInlineResult != null) {
            InlineQueryFlow.CHOSEN_RESULT
        } else {
            InlineQueryFlow.INVALID_FLOW
        }
    }

    fun getInlineQueryFlow(update: Update): InlineQueryFlow {
        return if (update.attributes.containsKey(INLINE_QUERY_FLOW_FIELD)) {
            update.attributes.getValue(INLINE_QUERY_FLOW_FIELD) as InlineQueryFlow
        } else {
            InlineQueryFlow.INVALID_FLOW
        }
    }

    fun getCallbackQueryData(callbackQuery: CallbackQuery): String {
        return callbackQuery.data.substringAfter(InlineQueryContext.INLINE_QUERY_SIGNATURE).substringAfterLast("!!!")
    }

    fun configureInlineQueryMarkup(
        inlineContext: InlineQueryContext,
        response: InlineQueryResponse
    ): InlineQueryResponse = response.copy(
        data = response.keyboard?.let { keyboard ->
            response.data.withInlineKeyboard(
                forInlineQuery(inlineContext.inlineQueryId, keyboard.toInlineKeyboard())
            )
        } ?: response.data
    ).withInlineContext(inlineContext)

    private fun isInlineQueryMessage(update: TelegramUpdate): Boolean {
        val replyMarkup = update.message?.replyMarkup

        return if (replyMarkup is InlineKeyboardMarkup) {
            replyMarkup.inlineKeyboard.any { row ->
                row.any { button ->
                    (button is InlineKeyboardButton.CallbackData) &&
                    button.callbackData.startsWith(InlineQueryContext.INLINE_QUERY_SIGNATURE)
                }
            }
        } else {
            false
        }
    }

    private fun forInlineQuery(
        inlineQueryId: String,
        keyboard: InlineKeyboardMarkup
    ): InlineKeyboardMarkup = keyboard.copy(
        inlineKeyboard = keyboard.inlineKeyboard.map { line ->
            line.map { button ->
                require(button is InlineKeyboardButton.CallbackData)
                button.copy(
                    callbackData = "${InlineQueryContext.INLINE_QUERY_SIGNATURE}${inlineQueryId}!!!${button.callbackData}"
                )
            }
        }
    )
}
