package be.rlab.tehanu.clients.telegram

import be.rlab.tehanu.clients.telegram.model.OutboundMessage
import com.github.kotlintelegrambot.entities.Message as TelegramMessage

/** Must be implemented by strategies to send different type of messages.
 */
interface MessageSender {

    /** Checks if this sender supports sending the specific outbound message.
     * @param outboundMessage Message to verify.
     * @return true if this sender can send the message, false otherwise.
     */
    fun supports(outboundMessage: OutboundMessage): Boolean

    /** Sends a message.
     * @param chatId Id of the chat to send the message to.
     * @param outboundMessage Message to send.
     * @return returns the list of created messages.
     */
    fun send(
        chatId: Long?,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage>

    /** Edits an existing message.
     * @param chatId Id of the chat to send the message to.
     * @param messageId Id of the message to edit.
     * @param outboundMessage Message to send.
     * @return returns the edited message.
     */
    fun edit(
        chatId: Long,
        messageId: Long,
        outboundMessage: OutboundMessage
    ): TelegramMessage
}