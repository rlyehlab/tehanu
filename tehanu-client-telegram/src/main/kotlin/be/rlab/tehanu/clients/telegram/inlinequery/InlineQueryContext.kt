package be.rlab.tehanu.clients.telegram.inlinequery

import be.rlab.tehanu.clients.model.Chat
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.view.UserInput
import org.joda.time.DateTime

data class InlineQueryContext(
    val inlineQueryId: String,
    val chat: Chat?,
    val user: User?,
    val resultMessageId: Long?,
    val selectedResultId: String?,
    val input: UserInput?,
    val createdAt: DateTime,
    val updatedAt: DateTime
) {
    companion object {
        const val INLINE_QUERY_SIGNATURE = "TIQ:"

        fun new(inlineQueryId: String): InlineQueryContext = InlineQueryContext(
            inlineQueryId = inlineQueryId,
            chat = null,
            user = null,
            resultMessageId = null,
            selectedResultId = null,
            input = null,
            createdAt = DateTime.now(),
            updatedAt = DateTime.now()
        )
    }

    val inlineQueryMessage: Message by lazy {
        Message.new(messageId = inlineQueryId.toLong(), date = DateTime.now())
    }

    fun withChat(chat: Chat): InlineQueryContext = copy(
        chat = chat,
        updatedAt = DateTime.now()
    )

    fun withUser(user: User): InlineQueryContext = copy(
        user = user,
        updatedAt = DateTime.now()
    )

    fun withResultMessageId(messageId: Long): InlineQueryContext = copy(
        resultMessageId = messageId,
        updatedAt = DateTime.now()
    )

    fun withSelectedResultId(selectedResultId: String): InlineQueryContext = copy(
        selectedResultId = selectedResultId,
        updatedAt = DateTime.now()
    )

    fun withInput(input: UserInput): InlineQueryContext {
        return copy(
            input = input
        )
    }
}
