package be.rlab.tehanu.clients.telegram.inlinequery.model

import com.github.kotlintelegrambot.entities.InlineKeyboardMarkup
import com.github.kotlintelegrambot.entities.inlinequeryresults.InlineQueryResult

data class InlineQueryResponseData(
    val nextOffset: String,
    val results: List<InlineQueryResult>,
    /** True if results may be cached on the server side only for the user that sent the query.
     *  By default, results may be returned to any user who sends the same query.
     */
    val personal: Boolean
) {
    companion object {
        fun new(
            nextOffset: String,
            results: List<InlineQueryResult>,
            personal: Boolean = false
        ): InlineQueryResponseData = InlineQueryResponseData(
            nextOffset = nextOffset,
            results = results,
            personal = personal
        )
    }

    fun withInlineKeyboard(inlineKeyboard: InlineKeyboardMarkup): InlineQueryResponseData {
        return copy(
            results = results.map { result ->
                when (result) {
                    is InlineQueryResult.Article -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Gif -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Mpeg4Gif -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Video -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Audio -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Voice -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Document -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Location -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Venue -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Contact -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.Game -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.CachedAudio -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.CachedDocument -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.CachedGif -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.CachedMpeg4Gif -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.CachedPhoto -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.CachedSticker -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.CachedVideo -> result.copy(replyMarkup = inlineKeyboard)
                    is InlineQueryResult.CachedVoice -> result.copy(replyMarkup = inlineKeyboard)
                    else -> throw RuntimeException("reply markup not supported for type: ${result::class}")
                }
            }
        )
    }
}
