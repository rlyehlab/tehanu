package be.rlab.tehanu.clients.telegram.view.ui

import be.rlab.tehanu.clients.telegram.InboundMessageFactory.Companion.CONTENT_FIELD
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.Validators.assertRequired
import be.rlab.tehanu.view.ui.Input
import be.rlab.tehanu.view.ui.input
import com.github.kotlintelegrambot.entities.Location

/** Field to ask user for media files.
 */
fun UserInput.location(
    description: MessageBuilder,
    callback: (Input.() -> Unit) = {}
) = input(description) {

    resolveValue {
        setValue(incomingMessage[CONTENT_FIELD])
    }

    validator {
        assertRequired(
            incomingMessage.content[CONTENT_FIELD] is Location,
            translations["view-fields-location-invalid"]
        )
    }

    callback(this)
}
