package be.rlab.tehanu.clients.telegram.model

import be.rlab.tehanu.messages.model.Response
import com.github.kotlintelegrambot.entities.TelegramFile

/** Represent audio files displayed as a playable voice message.
 * For this to work, your audio must be in an .OGG file encoded with OPUS (other formats may be sent as Audio or
 * Document). Bots can currently send voice messages of up to 50 MB in size, this limit may be changed in the future.
 */
data class VoiceResponse(
    override val data: TelegramFile,
    /** Duration of the voice message in seconds. */
    val duration: Int?,
    /** Message caption. */
    val caption: String?
): Response<TelegramFile> {
    companion object {
        const val TYPE: String = "voice"
    }
    override val type: String = TYPE
}
