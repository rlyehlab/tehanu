package be.rlab.tehanu.clients.telegram.sender

import be.rlab.tehanu.clients.telegram.MessageSender
import be.rlab.tehanu.clients.telegram.ResponseUtils.resolveMessage
import be.rlab.tehanu.clients.telegram.model.OutboundMessage
import be.rlab.tehanu.i18n.MessageSource
import be.rlab.tehanu.i18n.MessageSourceFactory
import be.rlab.tehanu.messages.model.TextResponse
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.Message as TelegramMessage

/** Sends a text message.
 * @param bot Bot that sends the message.
 */
class TextSender(
    private val bot: Bot,
    messageSourceFactory: MessageSourceFactory
) : MessageSender {

    // TODO(seykron): resolve the message source for the underlying handler.
    private val translations: MessageSource = messageSourceFactory.resolve("default")

    override fun supports(outboundMessage: OutboundMessage): Boolean {
        return outboundMessage.response is TextResponse
    }

    override fun edit(
        chatId: Long,
        messageId: Long,
        outboundMessage: OutboundMessage
    ): TelegramMessage {
        val response: TextResponse = outboundMessage.response()

        return bot.editMessageText(
            ChatId.fromId(chatId), messageId, null, response.data, outboundMessage.parseMode,
            outboundMessage.disableLinkPreview, outboundMessage.replyMarkup
        ).first
            ?.body()
            ?.result
            ?: throw RuntimeException("Cannot retrieve message response")
    }

    override fun send(
        chatId: Long?,
        outboundMessage: OutboundMessage
    ): List<TelegramMessage> {
        val result = bot.sendMessage(
            ChatId.fromId(chatId!!), resolve(outboundMessage.response()), outboundMessage.parseMode,
            outboundMessage.disableLinkPreview, outboundMessage.disableNotification,
            outboundMessage.replyToMessageId, allowSendingWithoutReply = true, outboundMessage.replyMarkup
        )
        return listOf(resolveMessage(result))
    }

    private fun resolve(response: TextResponse): String {
        return translations.expand(response.data, *response.args.toTypedArray())
    }
}
