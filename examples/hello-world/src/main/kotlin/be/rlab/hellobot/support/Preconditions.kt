package be.rlab.hellobot.support

import be.rlab.hellobot.domain.ProfileRepository.Companion.PROFILES_SLOT
import be.rlab.hellobot.domain.model.Profile
import be.rlab.hellobot.handlers.HandlerNames
import be.rlab.tehanu.view.UserInput

fun UserInput.profileMustExist() = precondition {
    val profiles: List<Profile> by memory.slot(PROFILES_SLOT, emptyList<Profile>())

    evaluate {
        val exists: Boolean = profiles.any { profile -> profile.user.id == context.user.id }
        if (!exists) {
            context.talk("You need a profile, please register a profile before creating tasks")
            reject()
        } else {
            resolve()
        }
    }
    resolve { transitionTo(HandlerNames.REGISTER_PROFILE) }
}
