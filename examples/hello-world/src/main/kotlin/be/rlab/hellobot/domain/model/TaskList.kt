package be.rlab.hellobot.domain.model

import be.rlab.hellobot.support.ShortHash
import be.rlab.tehanu.clients.model.Chat
import org.joda.time.DateTime
import java.util.*

/** Represents a User's _TODO_ list in a [Chat].
 */
data class TaskList(
    val id: UUID,
    val chatId: UUID,
    val name: String,
    val description: String,
    val createdAt: DateTime
) {
    companion object {
        fun new(
            chatId: UUID,
            name: String,
            description: String
        ): TaskList = TaskList(
            id = UUID.randomUUID(),
            chatId,
            name,
            description,
            createdAt = DateTime.now()
        )
    }

    fun shortId(): String = ShortHash.encode(id)
}
