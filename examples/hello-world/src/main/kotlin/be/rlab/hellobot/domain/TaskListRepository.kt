package be.rlab.hellobot.domain

import be.rlab.hellobot.domain.model.TaskList
import be.rlab.hellobot.support.ShortHash.encode
import be.rlab.hellobot.support.addOrUpdate
import be.rlab.tehanu.store.Memory
import java.util.*

class TaskListRepository(
    memory: Memory,
    private val taskRepository: TaskRepository
) {
    companion object {
        const val DEAD_TASKS_LIST_NAME: String = "::DEAD-TASKS::"
        const val TASK_LIST_SLOT: String = "tasks_lists"
    }

    private var tasksLists: List<TaskList> by memory.slot(TASK_LIST_SLOT, emptyList<TaskList>())

    fun findById(taskListId: UUID): TaskList? {
        return tasksLists.find { taskList -> taskList.id == taskListId }
    }

    fun findByChat(chatId: UUID): List<TaskList> {
        return tasksLists.filter { taskList ->
            taskList.chatId == chatId && taskList.name != DEAD_TASKS_LIST_NAME
        }
    }

    fun findByShortId(shortId: String): TaskList? {
        return tasksLists.find { taskList -> encode(taskList.id) == shortId }
    }

    fun delete(id: UUID) {
        val taskList: TaskList = findById(id)
            ?: throw RuntimeException("Task list with id $id does not exist")
        taskRepository.findByList(id)
            .map { task ->
                if (task.taskListIds.size == 1) {
                    task.removeFrom(id).addTo(deadTasksList(taskList.chatId).id)
                } else {
                    task.removeFrom(id)
                }
            }
            .forEach(taskRepository::saveOrUpdate)

        tasksLists = tasksLists.filter { existingTaskList ->
            existingTaskList.id != id
        }
    }

    fun saveOrUpdate(taskList: TaskList): TaskList {
        tasksLists = tasksLists.addOrUpdate(taskList) { oldItem, newItem ->
            oldItem.id == newItem.id
        }
        return taskList
    }

    private fun deadTasksList(chatId: UUID): TaskList {
        return tasksLists.find { taskList ->
            taskList.chatId == chatId && taskList.name == DEAD_TASKS_LIST_NAME
        } ?: saveOrUpdate(TaskList.new(chatId, DEAD_TASKS_LIST_NAME, "Dead Tasks List"))
    }
}
