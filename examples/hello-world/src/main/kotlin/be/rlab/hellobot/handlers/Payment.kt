package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.ui.enumeration
import be.rlab.tehanu.view.ui.number
import java.math.BigDecimal

enum class CurrencyUnit(val displayName: String) {
    DOLLAR("u\$s"),
    BITCOIN("btc")
}

data class Currency(
    val unit: CurrencyUnit,
    val amount: BigDecimal
)

fun UserInput.currency(description: String) = fieldGroup {
    val amount: Double by number(description)
    val currencyUnit: CurrencyUnit by enumeration<CurrencyUnit>("select the currency unit")

    resolveValue {
        Currency(currencyUnit, BigDecimal.valueOf(amount))
    }
}

object Payment {
    @Handler(name = "/pay")
    fun pay(context: UpdateContext) = context.userInput {
        form {
            val value: Currency by currency("how much it cost?")

            onSubmit {
                answer("got ${value.unit.displayName}${value.amount} from you, thanks!")
            }
        }
    }
}
