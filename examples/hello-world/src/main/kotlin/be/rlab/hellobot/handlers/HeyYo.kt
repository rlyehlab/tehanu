package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.Trigger
import be.rlab.tehanu.annotations.TriggerParam
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.messages.TextTrigger

object HeyYo {
    @Handler(name = "/yarrr")
    @Trigger(TextTrigger::class, TriggerParam(TextTrigger.STARTS_WITH, "/yarrr"))
    fun yarrr(context: UpdateContext) = context.apply {
        answer("yarrr!")
    }
}
