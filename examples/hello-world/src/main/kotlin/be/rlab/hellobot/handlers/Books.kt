package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.Trigger
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.telegram.InlineQueryTrigger
import be.rlab.tehanu.clients.telegram.inlineQuery
import be.rlab.tehanu.clients.telegram.inlineQueryResponse
import be.rlab.tehanu.clients.telegram.model.InlineQueryResponse
import com.github.kotlintelegrambot.entities.ParseMode
import com.github.kotlintelegrambot.entities.inlinequeryresults.InlineQueryResult
import com.github.kotlintelegrambot.entities.inlinequeryresults.InputMessageContent
import java.util.*

data class Book(
    val id: UUID = UUID.randomUUID(),
    val author: String,
    val title: String,
    val coverUrl: String,
    val description: String
)

object Books {
    private val books: List<Book> = listOf(
        Book(
            author = "Philip K Dick",
            title = "Ubik",
            coverUrl = "https://images-na.ssl-images-amazon.com/images/I/71F9RxHKu3L._AC_SY679_.jpg",
            description = "The story is set in a future 1992 where psychic powers are utilized in corporate espionage, while cryogenic technology allows recently deceased people to be maintained in a lengthy state of hibernation. It follows Joe Chip, a technician at a psychic agency who, after an assassination attempt, begins to experience strange alterations in reality that can be temporarily reversed by a mysterious store-bought substance called Ubik"
        ),
        Book(
            author = "Ursula K Le Guin",
            title = "The Dispossessed",
            coverUrl = "https://images-na.ssl-images-amazon.com/images/I/81Ry5hSi3tL.jpg",
            description = "The Dispossessed: An Ambiguous Utopia is a 1974 utopian science fiction novel by American writer Ursula K. Le Guin, set in the fictional universe of the seven novels of the Hainish Cycle."
        ),
        Book(
            author = "Ray Bradbury",
            title =  "Fahrenheit 451",
            coverUrl = "https://images-na.ssl-images-amazon.com/images/I/71bH9Q-7yeS.jpg",
            description = "Fahrenheit 451 is a 1953 dystopian novel by American writer Ray Bradbury. Often regarded as one of his best works, the novel presents a future American society where books are outlawed and \"firemen\" burn any that are found. The book's tagline explains the title as \"'the temperature at which book paper catches fire, and burns\": the autoignition temperature of paper. The lead character, Guy Montag, is a fireman who becomes disillusioned with his role of censoring literature and destroying knowledge, eventually quitting his job and committing himself to the preservation of literary and cultural writings."
        ),
        Book(
            author = "Neil Gaiman",
            title =  "American Gods",
            coverUrl = "https://1.bp.blogspot.com/-WUQfscuAOuU/W5fMc8x779I/AAAAAAAACp8/b_xdSMiZ46MPyQNjleS5XOX9ccLYzt26QCLcBGAs/s1600/American%2BGods.jpg",
            description = "American Gods (2001) is a fantasy novel by British author Neil Gaiman. The novel is a blend of Americana, fantasy, and various strands of ancient and modern mythology, all centering on the mysterious and taciturn Shadow."
        )
    )

    @Handler(name = "/books")
    @Trigger(type = InlineQueryTrigger::class)
    fun search(context: UpdateContext) = context.apply {
        val inlineQuery = inlineQuery()
        val filteredBooks = books.filter { book ->
            book.author.toLowerCase().contains(inlineQuery.query.toLowerCase()) ||
            book.title.toLowerCase().contains(inlineQuery.query.toLowerCase())
        }
        sendMessage {
            inlineQueryResponse(
                id = inlineQuery.id,
                response = InlineQueryResponse.new(
                    inlineQuery.offset,
                    filteredBooks.map { book ->
                        InlineQueryResult.Article(
                            id = book.id.toString(),
                            title = book.title,
                            description = book.description,
                            inputMessageContent = InputMessageContent.Text(
                                "*${book.title}*\n_${book.author}_\n\n${book.description}",
                                parseMode = ParseMode.MARKDOWN
                            ),
                            thumbUrl = book.coverUrl
                        )
                    }
                )
            )
        }
    }
}
