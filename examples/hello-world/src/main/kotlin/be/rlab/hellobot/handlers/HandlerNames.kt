package be.rlab.hellobot.handlers

object HandlerNames {
    const val REGISTER_PROFILE: String = "/register_profile"

    const val CREATE_TASK_LIST: String = "/list_create"
    const val SHOW_TASK_LISTS: String = "/lists"
    const val DELETE_TASK_LIST: String = "/list_delete"

    const val CREATE_TASK: String = "/task_create"
    const val SHOW_TASKS: String = "/tasks"
}
