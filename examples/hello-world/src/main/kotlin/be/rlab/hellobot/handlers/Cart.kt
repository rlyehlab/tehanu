package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.store.Memory

class Cart(memory: Memory) {
    private var products: List<String> by memory.slot("PRODUCTS", emptyList<String>())

    @Handler(name = "/cart_add")
    fun add(context: UpdateContext) = context.userInput {
        form {
            val productName: String by field("which product would you like to add to the cart?")

            onSubmit {
                products = products + productName
                answer("we added the product to the cart, thanks!")
            }
        }
    }

    @Handler(name = "/cart_list")
    fun list(context: UpdateContext) = context.apply {
        talk("List of products:\n${products.joinToString("\n")}")
    }
}
