package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.requireNotNull
import be.rlab.tehanu.messages.model.Message

object Math {
    private val sumExpr: Regex = Regex("(\\d+)\\s*\\+\\s*(\\d+)")
    private val subExpr: Regex = Regex("(\\d+)\\s*-\\s*(\\d+)")

    @Handler(name = "/math_sum")
    fun sum(
        context: UpdateContext,
        message: Message
    ) = context.apply {
        val operands = requireNotNull(
            sumExpr.find(message.text.substringAfter(" "))?.groupValues,
            "you must specify two operands, like in: 3+5"
        )
        val result: Long = operands[1].toLong() + operands[2].toLong()
        answer(result.toString())
    }

    @Handler(name = "/math_sub")
    fun subtract(
        context: UpdateContext,
        message: Message
    ) = context.apply {
        val operands = requireNotNull(
            subExpr.find(message.text.substringAfter(" "))?.groupValues,
            "you must specify two operands, like in: 9-2"
        )
        val result: Long = operands[1].toLong() - operands[2].toLong()
        answer(result.toString())
    }
}
