package be.rlab.hellobot.support

/** Adds or updates an existing item in this list.
 *
 * If the equality function returns true for any list element, it is replaced by the new item.
 * If the equality function returns false for all elements, it means the new item does not exist,
 * so it adds the new item to the list.
 *
 * @param newItem Item to add or update.
 * @param equals Equality function, it takes the old item and the new item as parameters. Must return true to update
 * the existing item.
 * @return the updated list.
 */
fun<T> List<T>.addOrUpdate(
    newItem: T,
    equals: (T, T) -> Boolean
): List<T> {
    var updated = false
    val results: List<T> = map { oldItem ->
        if (equals(oldItem, newItem)) {
            updated = true
            newItem
        } else {
            oldItem
        }
    }
    return if (updated) {
        results
    } else {
        results + newItem
    }
}
