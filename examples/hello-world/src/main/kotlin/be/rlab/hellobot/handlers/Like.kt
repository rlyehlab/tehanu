package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.Trigger
import be.rlab.tehanu.annotations.TriggerParam
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.telegram.photo
import be.rlab.tehanu.messages.MessageContentTrigger
import be.rlab.tehanu.messages.model.Message

object Like {
    @Handler(name = "/like")
    @Trigger(MessageContentTrigger::class, TriggerParam(MessageContentTrigger.HAS_MEDIA))
    fun like(
        context: UpdateContext,
        message: Message
    ) = context.apply {
        if (message.files.last().mimeType == "image/jpeg") {
            sendMessage {
                photo(readContent(message.files.last()), "<3")
            }
        }
    }
}
