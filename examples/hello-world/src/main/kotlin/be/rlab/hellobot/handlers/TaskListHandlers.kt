package be.rlab.hellobot.handlers

import be.rlab.hellobot.domain.TaskListRepository
import be.rlab.hellobot.domain.model.TaskList
import be.rlab.hellobot.handlers.HandlerNames.CREATE_TASK_LIST
import be.rlab.hellobot.handlers.HandlerNames.DELETE_TASK_LIST
import be.rlab.hellobot.handlers.HandlerNames.SHOW_TASK_LISTS
import be.rlab.hellobot.view.taskList
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.telegram.parseMode
import com.github.kotlintelegrambot.entities.ParseMode

object TaskListHandlers {

    @Handler(name = CREATE_TASK_LIST)
    fun create(
        taskListRepository: TaskListRepository,
        context: UpdateContext
    ) = context.userInput {
        val name: String by field("Tell me the list name")
        val description: String by field("Tell me a brief description for this list")

        onSubmit {
            val taskList: TaskList = TaskList.new(chat.id, name, description)
            taskListRepository.saveOrUpdate(taskList)
            answer("I created the list '$name', now you can add tasks")
        }
    }

    @Handler(name = SHOW_TASK_LISTS)
    fun list(context: UpdateContext) = context.userInput {
        taskList("Select a task list to show additional information") { context, taskList ->
            context.sendMessage {
                text("""
                    *${taskList.name}*
                    ${taskList.description}
                """.trimIndent())
                parseMode(ParseMode.MARKDOWN_V2)
            }
        }
    }

    @Handler(name = DELETE_TASK_LIST)
    fun delete(
        taskListRepository: TaskListRepository,
        context: UpdateContext
    ) = context.userInput {
        val taskList: TaskList by taskList("Select the list you want to delete, or type /cancel to abort")

        onSubmit {
            taskListRepository.delete(taskList.id)
            answer("I deleted the task list: ${taskList.name}")
        }
    }
}
