package be.rlab.hellobot.support

import org.hashids.Hashids
import java.util.*
import kotlin.math.absoluteValue

object ShortHash {
    private val hashids = Hashids("TODOBot")

    fun encode(uuid: UUID): String {
        return hashids.encode((uuid.mostSignificantBits % Hashids.MAX_NUMBER).absoluteValue)
    }

    fun encode(vararg numbers: Long): String {
        return hashids.encode(*numbers)
    }

    fun decode(hash: String): List<Long> {
        return hashids.decode(hash).toList()
    }
}