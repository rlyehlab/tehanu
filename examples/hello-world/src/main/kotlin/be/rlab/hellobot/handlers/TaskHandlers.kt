package be.rlab.hellobot.handlers

import be.rlab.hellobot.domain.ProfileRepository
import be.rlab.hellobot.domain.TaskRepository
import be.rlab.hellobot.domain.model.Task
import be.rlab.hellobot.domain.model.TaskList
import be.rlab.hellobot.handlers.HandlerNames.CREATE_TASK
import be.rlab.hellobot.handlers.HandlerNames.SHOW_TASKS
import be.rlab.hellobot.support.profileMustExist
import be.rlab.hellobot.view.taskList
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.view.ui.KeyboardMode
import be.rlab.tehanu.view.ui.keyboard

object TaskHandlers {
    @Handler(name = CREATE_TASK)
    fun create(
        profileRepository: ProfileRepository,
        taskRepository: TaskRepository,
        context: UpdateContext
    ) = context.userInput {
        preconditions {
            profileMustExist()
        }

        form {
            val taskLists: List<TaskList> by taskList(
                "Select the task lists to add this task to",
                multipleSelection = true
            )
            val title: String by field("Tell me a title for the task")
            val description: String by field("Tell me a description for this task")

            onSubmit {
                val task: Task = taskLists.fold(Task.new(title, description)) { task, taskList ->
                    task.addTo(taskList.id)
                }.assign(profileRepository.findByUser(user)!!)
                taskRepository.saveOrUpdate(task)
                answer("Done! I created the task")
            }
        }
    }

    @Handler(name = SHOW_TASKS)
    fun list(
        taskRepository: TaskRepository,
        context: UpdateContext
    ) = context.userInput {
        val taskList: TaskList by taskList("Select the task list to show")
        val tasks: List<Task> by field("Select a task to see more details") {
            keyboard(mode = KeyboardMode.MULTIPLE_SELECTION) {
                val tasks = taskRepository.findByList(taskList.id)
                tasks.forEach { task ->
                    val assignees = task.assignees.joinToString(", ") { profile ->
                        profile.displayName
                    }
                    checkbox("${task.title} ($assignees)", task)
                }
                submitButton("Show information")
            }
        }
        onSubmit {
            val tasksDescription: String = tasks.joinToString("\n\n") { task ->
                val assignees = task.assignees.joinToString(", ") { profile ->
                    profile.displayName
                }
                """
                    # ${task.title}
                    Description: ${task.description}
                    Assignees: $assignees
                    Status: ${task.status}
                    Tasks Lists:
                """.trimIndent()
            }
            talk(tasksDescription)
        }
    }
}
