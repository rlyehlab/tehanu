package be.rlab.hellobot.view

import be.rlab.hellobot.domain.TaskListRepository.Companion.TASK_LIST_SLOT
import be.rlab.hellobot.domain.model.TaskList
import be.rlab.hellobot.handlers.HandlerNames
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.view.UserInput
import be.rlab.tehanu.view.ui.Button
import be.rlab.tehanu.view.ui.Field
import be.rlab.tehanu.view.ui.KeyboardMode
import be.rlab.tehanu.view.ui.keyboard

/** Shows a list of [TaskList]s and ask the user to select one.
 */
fun UserInput.taskList(
    helpMessage: String = "Select a task list",
    multipleSelection: Boolean = false,
    onTaskListClick: ((UpdateContext, TaskList) -> Unit)? = null
): Field = field(helpMessage) {

    fun registerOnClickIfRequired(button: Button, taskList: TaskList) {
        onTaskListClick?.let {
            button.onClick { context, _ -> onTaskListClick(context, taskList) }
        }
    }

    val tasksLists: List<TaskList> by memory.slot(TASK_LIST_SLOT, emptyList<TaskList>())

    precondition {
        evaluate {
            val tasksListForChat: List<TaskList> = tasksLists.filter { taskList -> taskList.chatId == context.chat.id }
            if (tasksListForChat.isEmpty()) {
                context.talk("You don't have tasks lists, please create one to continue")
                reject()
            } else {
                resolve()
            }
        }

        resolve { transitionTo(HandlerNames.CREATE_TASK_LIST) }
    }

    keyboard(
        mode = if (multipleSelection) KeyboardMode.MULTIPLE_SELECTION else KeyboardMode.SINGLE_SELECTION
    ) {
        tasksLists.filter {
            taskList -> taskList.chatId == context.chat.id
        }.forEach { taskList ->
            if (multipleSelection) {
                checkbox(taskList.name, taskList) {
                    registerOnClickIfRequired(this, taskList)
                }
            } else {
                button(taskList.name, taskList) {
                    registerOnClickIfRequired(this, taskList)
                }
            }
        }
        if (multipleSelection) {
            submitButton("Done")
        }
    }

    validator { value ->
        require(value is TaskList || (value is List<*> && value.isNotEmpty())) { "You must select at least one list." }
    }
}
