package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.telegram.photo
import be.rlab.tehanu.media.model.MediaFile
import be.rlab.tehanu.view.ui.keyboard
import be.rlab.tehanu.view.ui.media

object SelectPet {
    @Handler(name = "/select_pet")
    fun selectPet(context: UpdateContext) = context.userInput {
        form {
            val pet: String by field("what's your favorite animal?") {
                keyboard {
                    radio("Cats", "\uD83D\uDE3A")
                    radio("Dogs", "\uD83D\uDC36")
                    submitButton("Awww")
                }
            }
            val photos: List<MediaFile> by media("please, send me the photo of your favorite animal")

            onSubmit {
                sendMessage {
                    photo(readContent(photos.last()), "aww, I like your $pet")
                }
            }
        }
    }
}
