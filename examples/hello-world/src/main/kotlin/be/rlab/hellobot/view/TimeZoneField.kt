package be.rlab.hellobot.view

import be.rlab.tehanu.view.UserInput
import java.util.*

/** Ask the user for a valid time zone name, and resolves a valid [TimeZone].
 */
fun UserInput.timeZone(
    helpMessage: String =
        "Please, tell me your time zone name. You can check the valid names in" +
        " this link: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones"
) = field(helpMessage) {
    validator {  value ->
        require(TimeZone.getAvailableIDs().contains(value)) {
            "The time zone name is not valid, please look at the valid names in this link: " +
            "https://en.wikipedia.org/wiki/List_of_tz_database_time_zones"
        }
    }
    resolveValue { values ->
        require(values[0] is String)
        TimeZone.getTimeZone(values[0] as String)
    }
}
