package be.rlab.hellobot

import be.rlab.hellobot.domain.ProfileRepository
import be.rlab.hellobot.domain.TaskListRepository
import be.rlab.hellobot.domain.TaskRepository
import be.rlab.hellobot.handlers.*
import be.rlab.tehanu.Tehanu
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.telegram.telegram

@Handler(name = "/say_hello")
fun sayHello(context: UpdateContext) = context.apply {
    answer("hello!")
}

@Handler(name = "/start")
fun start(context: UpdateContext) = context.apply {
    talk(
        "hello! Thanks for using the TODO bot. " +
        "This is an full functional example project to show how Tehanu works. " +
        "Visit the [wiki](https://git.rlab.be/seykron/tehanu/-/wikis/home) to read the full documentation."
    )
}

fun main(args: Array<String>) {
    Tehanu.configure {
        persistence {
            dataSource {
                jdbcUrl = "jdbc:h2:file:/tmp/tehanu-db"
                username = "sa"
                password = ""
                driverClassName = "org.h2.Driver"
                connectionTimeout = 5000
                logStatements = true
            }
        }

        services {
            register { ProfileRepository(ref()) }
            register { TaskRepository(ref()) }
            register { TaskListRepository(ref(), ref()) }
        }

        handlers {
            register(::sayHello)
            register(::start)
            register(HeyYo)
            register(Echo)
            register(Math)
            register(MyLocation)
            register(::yourName)
            register(Register)
            register(SelectPet)
            register(Payment)
            register(Cart(memory()))
            register(Books)
            register(Like)
            register(ProfileHandlers::registerProfile)
            register(TaskListHandlers::create, TaskListHandlers::list, TaskListHandlers::delete)
            register(TaskHandlers::create, TaskHandlers::list)
        }

        security {
            admins("46718328")
            roles(
                role("USER", listOf("READ_CHAT", "READ_CALENDAR"))
            )
        }

        clients {
            telegram("YOUR_BOT_TOKEN")
        }
    }.start()
}
