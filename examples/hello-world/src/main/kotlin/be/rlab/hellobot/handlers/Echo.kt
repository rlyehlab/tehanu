package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.model.ChatType
import be.rlab.tehanu.messages.model.Message

object Echo {
    @Handler(name = "/ping", scope = [ChatType.PRIVATE, ChatType.GROUP])
    fun pong(context: UpdateContext, message: Message) = context.apply {
        answer(message.text.substringAfter("/ping"))
    }
}
