package be.rlab.hellobot.domain

import be.rlab.hellobot.domain.model.Profile
import be.rlab.hellobot.support.addOrUpdate
import be.rlab.tehanu.clients.model.User
import be.rlab.tehanu.store.Memory

class ProfileRepository(memory: Memory) {
    companion object {
        const val PROFILES_SLOT: String = "profiles"
    }

    private var profiles: List<Profile> by memory.slot(PROFILES_SLOT, emptyList<Profile>())

    fun findByUser(user: User): Profile? {
        return profiles.find { profile -> profile.user.id == user.id }
    }

    fun saveOrUpdate(profile: Profile) {
        profiles = profiles.addOrUpdate(profile) { oldItem, newItem ->
            oldItem.id == newItem.id
        }
    }
}
