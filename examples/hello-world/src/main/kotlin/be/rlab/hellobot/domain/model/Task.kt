package be.rlab.hellobot.domain.model

import be.rlab.hellobot.support.ShortHash
import org.joda.time.DateTime
import java.util.*

data class Task(
    val id: UUID,
    val taskListIds: List<UUID>,
    val title: String,
    val description: String,
    val status: TaskStatus,
    val assignees: List<Profile>,
    val createdAt: DateTime
) {
    companion object {
        fun new(
            title: String,
            description: String
        ): Task = Task(
            id = UUID.randomUUID(),
            taskListIds = emptyList(),
            title,
            description,
            status = TaskStatus.PENDING,
            assignees = emptyList(),
            createdAt = DateTime.now()
        )
    }

    fun assign(profile: Profile): Task = copy(
        assignees = assignees + profile
    )

    fun addTo(taskListId: UUID): Task = copy(
        taskListIds = taskListIds + taskListId
    )

    fun removeFrom(taskListId: UUID): Task = copy(
        taskListIds = taskListIds.filter { candidateTaskListId -> candidateTaskListId != taskListId }
    )

    fun shortId(): String = ShortHash.encode(id)
}
