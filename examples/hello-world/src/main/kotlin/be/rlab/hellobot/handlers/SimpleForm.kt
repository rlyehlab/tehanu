package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.view.Validators.assertRequired
import be.rlab.tehanu.view.ui.Field

@Handler(name = "/form")
fun yourName(context: UpdateContext) = context.userInput {
    fun Field.nameValidator(value: Any) {
        assertRequired(
            (65..90).contains(value.toString().codePointAt(0)),
            "your name must start with uppercase!"
        )
    }
    val firstName: String by field("Tell me your first name") {
        validator(Field::nameValidator)
    }
    val lastName: String by field("Tell me your last name") {
        validator(Field::nameValidator)
    }

    onSubmit {
        answer("Hi $firstName $lastName!")
    }
}
