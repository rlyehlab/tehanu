package be.rlab.hellobot.domain

import be.rlab.hellobot.domain.model.Task
import be.rlab.hellobot.support.addOrUpdate
import be.rlab.tehanu.store.Memory
import java.util.*

class TaskRepository(memory: Memory) {
    private var tasks: List<Task> by memory.slot("tasks", emptyList<Task>())

    fun findByList(taskListId: UUID): List<Task> {
        return tasks.filter { task -> task.taskListIds.contains(taskListId) }
    }

    fun delete(id: UUID) {
        tasks = tasks.filter { task ->
            task.id != id
        }
    }

    fun saveOrUpdate(task: Task) {
        tasks = tasks.addOrUpdate(task) { oldItem, newItem ->
            oldItem.id == newItem.id
        }
    }
}
