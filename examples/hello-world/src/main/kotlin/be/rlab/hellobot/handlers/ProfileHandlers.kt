package be.rlab.hellobot.handlers

import be.rlab.hellobot.domain.ProfileRepository
import be.rlab.hellobot.domain.model.Profile
import be.rlab.hellobot.handlers.HandlerNames.REGISTER_PROFILE
import be.rlab.hellobot.view.timeZone
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import java.util.*

object ProfileHandlers {
    @Handler(name = REGISTER_PROFILE)
    fun registerProfile(
        profileRepository: ProfileRepository,
        context: UpdateContext
    ) = context.userInput {
        val existingProfile: Profile? = profileRepository.findByUser(context.user)
        val displayName: String by field(
            existingProfile?.let { "Hi ${existingProfile.displayName}, tell me your new name or nick" }
                ?: "Tell me your name or nick"
        )
        val timeZone: TimeZone by timeZone()

        onSubmit {
            val profile: Profile = existingProfile?.update(displayName, timeZone)
                ?: Profile.new(user, displayName, timeZone)
            profileRepository.saveOrUpdate(profile)
            answer("Thanks $displayName! You can create Tasks Lists now")
        }
    }
}
