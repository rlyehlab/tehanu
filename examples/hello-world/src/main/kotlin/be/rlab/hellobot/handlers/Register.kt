package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.telegram.parseMode
import be.rlab.tehanu.clients.telegram.photo
import be.rlab.tehanu.media.model.MediaFile
import be.rlab.tehanu.view.Validators.assertNumber
import be.rlab.tehanu.view.Validators.assertRequired
import be.rlab.tehanu.view.ui.media
import com.github.kotlintelegrambot.entities.ParseMode

object Register {
    @Handler(name = "/register_new_user")
    fun register(context: UpdateContext) = context.userInput {
        val age: Int by field("Tell me your age") {
            validator { value ->
                assertNumber(value.toString())
                assertRequired(value.toString().toInt() > 5, "you must be older!")
            }
        }
        val firstName: String by field("Tell me your first name")
        val lastName: String by field("Tell me your last name")
        val photos: List<MediaFile> by media("Send me your profile picture")

        onSubmit {
            sendMessage {
                parseMode(ParseMode.MARKDOWN_V2)
                photo(readContent(photos.last()), """
                    *This is your Profile*
                    First name: $firstName
                    Last name: $lastName
                    Age: $age
                """.trimIndent())
            }
        }
    }
}
