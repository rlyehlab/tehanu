package be.rlab.hellobot.domain.model

/** Enumeration of all the supported statuses for [Task]s.
 */
enum class TaskStatus {
    /** The task is still pending, no work started. */
    PENDING,
    /** The task is started and still in progress. */
    IN_PROGRESS,
    /** The task is completed. */
    COMPLETED
}
