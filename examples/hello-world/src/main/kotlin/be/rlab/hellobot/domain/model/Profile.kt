package be.rlab.hellobot.domain.model

import be.rlab.tehanu.clients.model.User
import java.util.*

data class Profile(
    val id: UUID,
    val user: User,
    val displayName: String,
    val timeZone: TimeZone
) {
    companion object {
        fun new(
            user: User,
            displayName: String,
            timeZone: TimeZone
        ): Profile = Profile(
            id = UUID.randomUUID(),
            user,
            displayName,
            timeZone
        )
    }

    fun update(
        displayName: String,
        timeZone: TimeZone
    ): Profile = copy(
        displayName = displayName,
        timeZone = timeZone
    )
}
