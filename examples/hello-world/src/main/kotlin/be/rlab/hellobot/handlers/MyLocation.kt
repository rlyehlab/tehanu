package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.Trigger
import be.rlab.tehanu.annotations.TriggerParam
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.messages.MessageContentTrigger
import be.rlab.tehanu.messages.model.Message

object MyLocation {
    @Handler(name = "/my_location")
    @Trigger(MessageContentTrigger::class, TriggerParam(MessageContentTrigger.HAS_LOCATION))
    fun show(
        context: UpdateContext,
        message: Message
    ) = context.apply {
        answer("${message.location.latitude}, ${message.location.longitude}")
    }
}
