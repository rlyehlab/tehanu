package be.rlab.hellobot.handlers

import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.annotations.Security
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.messages.model.Message
import be.rlab.tehanu.store.Memory

class Calendar(memory: Memory) {
    private var events: List<String> by memory.slot("EVENTS", emptyList<String>())

    @Handler("/calendar_add")
    @Security(requiredPermissions = ["ADD_EVENT"])
    fun addEvent(
        context: UpdateContext,
        message: Message
    ) = context.apply {
        events = events + message.text.substringAfter(" ")
        answer("event added!")
    }

    @Handler("/calendar_list")
    @Security(requiredPermissions = ["READ_CALENDAR"])
    fun list(context: UpdateContext) = context.apply {
        talk(events.joinToString("\n"))
    }
}
