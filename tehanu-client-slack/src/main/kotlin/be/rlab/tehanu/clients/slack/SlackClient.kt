package be.rlab.tehanu.clients.slack

import be.rlab.tehanu.BotServiceProvider
import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.clients.Client
import be.rlab.tehanu.clients.Update
import be.rlab.tehanu.clients.UpdateContext
import be.rlab.tehanu.clients.UpdateDispatcher
import be.rlab.tehanu.clients.model.*
import be.rlab.tehanu.messages.MessageBuilder
import be.rlab.tehanu.messages.model.*
import be.rlab.tehanu.store.Memory
import be.rlab.tehanu.view.ui.Control
import be.rlab.tehanu.view.ui.Field
import be.rlab.tehanu.view.ui.Keyboard
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.slack.api.Slack
import com.slack.api.methods.MethodsClient
import com.slack.api.methods.response.channels.ChannelsSetTopicResponse
import com.slack.api.model.block.ActionsBlock
import com.slack.api.model.block.LayoutBlock
import com.slack.api.model.block.SectionBlock
import com.slack.api.model.block.composition.PlainTextObject
import com.slack.api.model.block.element.ButtonElement
import com.slack.api.model.event.*
import com.slack.api.rtm.RTMClient
import com.slack.api.util.json.GsonFactory
import com.sun.net.httpserver.HttpServer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.InetSocketAddress
import java.net.URLDecoder
import java.util.*
import javax.websocket.CloseReason
import kotlin.concurrent.timer
import com.slack.api.model.Message as SlackMessage
import com.slack.api.model.User as SlackUser

/** Slack messaging client implementation.
 */
class SlackClient(
    override val name: String,
    override val serviceProvider: BotServiceProvider,
    private val accessToken: String,
    private val httpServerPort: Int,
    private val httpServerEndpoint: String,
    private val inboundMessageFactory: InboundMessageFactory
) : Client {

    companion object {
        private const val DEFAULT_LINE_LENGTH: Int = 4
        private const val CLIENT_IDS_SLOT = "SlackClient::clientsId"
        private const val RECONNECT_PERIOD: Long = 5000
    }

    private val logger: Logger = LoggerFactory.getLogger(SlackClient::class.java)
    private val gson: Gson = GsonFactory.createSnakeCase(Slack().httpClient.config)

    // Bot services
    private val accessControl: AccessControl by lazy { serviceProvider.getService(AccessControl::class) }
    private val dispatcher: UpdateDispatcher by lazy { serviceProvider.getService(UpdateDispatcher::class) }
    private val memory: Memory by lazy { serviceProvider.getService(Memory::class) }

    private var clientIds: Map<String, Long> by memory.slot(CLIENT_IDS_SLOT, emptyMap<String, Long>())
    private var reconnect: Boolean = false
    private lateinit var client: RTMClient

    override fun start() {
        logger.info("initializing slack client")
        initSlackHandler()
        initHttpServer()
        initReconnectMonitor()
        logger.info("slack client ready")
    }

    override fun sendMessage(context: UpdateContext, message: MessageBuilder): List<Message> {
        val clientId: String = when {
            message.userId != null -> api().conversationsOpen { req ->
                req.users(listOf(reverseId(message.userId!!)))
            }.channel.id
            message.chatId != null -> {
                val chat: Chat = accessControl.findChatById(message.chatId!!)
                    ?: throw RuntimeException("chat not found: ${message.chatId}")
                reverseId(chat.clientId)
            }
            else -> throw RuntimeException("message without chat or user not supported.")
        }
        return doSendMessage(clientId, message)
    }

    override fun editMessage(
        chatId: UUID,
        messageId: Long,
        newMessage: MessageBuilder
    ): Message {
        val chat: Chat = accessControl.findChatById(chatId)
            ?: throw RuntimeException("chat not found: $chatId")

        require(newMessage.response is TextResponse) { "only text messages supported" }
        val response: TextResponse = newMessage.response as TextResponse

        val message: SlackMessage = api().chatUpdate { req ->
            req.channel(reverseId(chat.clientId))
                .ts(reverseId(messageId))
                .asUser(true)
                .text(response.data)
                .blocks(newMessage.option(BLOCKS))
        }.message

        return inboundMessageFactory.createMessage(messageId, message)
    }

    override fun renderControl(
        context: UpdateContext,
        control: Control,
        messageId: Long?
    ) {
        require(control is Field)
        logger.debug("rendering user input field: ${control.description}")
        val helpMessage = control.description?.forChat(context.chat.id) ?: context.newMessage()

        toBlocks(control)?.let { attachments ->
            if (messageId == null) {
                sendMessage(context, helpMessage.blocks(attachments))
            } else {
                editMessage(context.chat.id, messageId, helpMessage.blocks(attachments))
            }
        } ?: context.sendMessage(helpMessage).first()
    }

    private fun processUpdate(jsonUpdate: String) {
        val update: JsonObject = gson.fromJson(jsonUpdate, JsonObject::class.java)
        val handlers: Map<String, (JsonObject) -> Update> = mapOf(
            "message" to this::handleMessage,
            "block_actions" to this::handleBlockActions,
            "member_joined_channel" to this::handleEvent,
            "member_left_channel" to this::handleEvent,
            "channel_rename" to this::handleEvent,
            "channel_deleted" to this::handleEvent,
            "channel_created" to this::handleEvent
        )

        handlers[update.get("type").asString]?.let { handler ->
            val resolvedUpdate: Update = handler(update)
            dispatcher.dispatch(this, resolvedUpdate)
        }
    }

    private fun handleMessage(jsonUpdate: JsonObject): Update {
        val message: SlackMessage = gson.fromJson(jsonUpdate, SlackMessage::class.java)
        val updateId: Long = System.currentTimeMillis()
        val chat: Chat = resolveChat(message.channel)

        return when(message.subtype) {
            null -> Update.new(
                updateId = updateId,
                chat = chat,
                user = resolveUser(message.user, chat),
                message = inboundMessageFactory.createMessage(id(message.ts), message)
            )
            "channel_topic" -> Update.new(
                updateId = updateId,
                chat = chat,
                user = resolveUser(message.user, chat),
                message = inboundMessageFactory.createMessage(
                    messageId = id(jsonUpdate.get("ts").asString),
                    eventType = message.subtype,
                    data = gson.fromJson(jsonUpdate, ChannelsSetTopicResponse::class.java)
                )
            )
            "bot_message" -> Update.new(
                updateId = updateId,
                chat = chat,
                user = resolveBotUser(message.botId),
                message = inboundMessageFactory.createMessage(id(message.ts), message)
            )
            else ->
                throw RuntimeException("Unhandled message subtype: ${message.subtype}")
        }
    }

    private fun handleBlockActions(jsonUpdate: JsonObject): Update {
        val jsonAction: JsonObject = jsonUpdate.getAsJsonArray("actions")[0].asJsonObject
        val jsonMessage: JsonObject = jsonUpdate.getAsJsonObject("message")
        val message: SlackMessage = gson.fromJson(jsonMessage, SlackMessage::class.java)
        val updateId: Long = System.currentTimeMillis()
        val chat: Chat = resolveChat(jsonUpdate.getAsJsonObject("channel").get("id").asString)

        return Update.new(
            updateId = updateId,
            chat = chat,
            user = resolveUser(jsonUpdate.getAsJsonObject("user").get("id").asString, chat),
            message = inboundMessageFactory.createMessage(id(message.ts), message),
            action = Action(
                jsonAction.get("block_id").asString,
                jsonAction.get("action_id").asString
            )
        )
    }

    private fun handleEvent(jsonUpdate: JsonObject): Update {
        val eventTypes: Map<String, Class<out Event>> = mapOf(
            "member_joined_channel" to MemberJoinedChannelEvent::class.java,
            "member_left_channel" to MemberLeftChannelEvent::class.java,
            "channel_rename" to ChannelRenameEvent::class.java,
            "channel_deleted" to ChannelDeletedEvent::class.java,
            "channel_created" to ChannelCreatedEvent::class.java
        )
        val eventType: String = jsonUpdate.get("type").asString
        val type: Class<out Event> = eventTypes[eventType]
            ?: throw RuntimeException("Unsupported event type: $eventType")
        val updateId: Long = System.currentTimeMillis()

        return when(val event: Event = gson.fromJson(jsonUpdate, type)) {
            is MemberJoinedChannelEvent -> Update.new(
                updateId = updateId,
                chat = resolveChat(event.channel),
                user = resolveUser(event.user, resolveChat(event.channel)),
                message = inboundMessageFactory.createMessage(System.currentTimeMillis(), eventType, event)
            )
            is MemberLeftChannelEvent -> Update.new(
                updateId = updateId,
                chat = resolveChat(event.channel),
                user = resolveUser(event.user, resolveChat(event.channel)),
                message = inboundMessageFactory.createMessage(System.currentTimeMillis(), eventType, event)
            )
            is ChannelRenameEvent -> Update.new(
                updateId = updateId,
                chat = resolveChat(event.channel.id),
                user = null,
                message = inboundMessageFactory.createMessage(System.currentTimeMillis(), eventType, event)
            )
            is ChannelDeletedEvent -> Update.new(
                updateId = updateId,
                chat = resolveChat(event.channel),
                user = null,
                message = inboundMessageFactory.createMessage(System.currentTimeMillis(), eventType, event)
            )
            is ChannelCreatedEvent -> Update.new(
                updateId = updateId,
                chat = resolveChat(event.channel.id),
                user = resolveUser(event.channel.creator, resolveChat(event.channel.id)),
                message = inboundMessageFactory.createMessage(System.currentTimeMillis(), eventType, event)
            )
            else -> throw RuntimeException("Unsupported event type: $eventType")
        }
    }

    private fun api(): MethodsClient {
        return Slack().methods(accessToken)
    }

    private fun userInfo(userId: String): SlackUser {
        return api().usersInfo { req ->
            req.user(userId)
                .includeLocale(true)
        }.user
    }

    private fun resolveUser(
        userId: String,
        chat: Chat?
    ): User {
        val slackUser: SlackUser = userInfo(userId)
        val user = User(
            id = id(slackUser.id),
            userName = slackUser.name,
            firstName = slackUser.realName,
            lastName = null
        )

        return chat?.let {
            accessControl.addUserIfRequired(chat, user)
        } ?: accessControl.addUserIfRequired(user)
    }

    private fun resolveBotUser(botId: String): User {
        val botUser = api().botsInfo { req ->
            req.bot(botId)
        }.bot

        return User(
            id = id(botUser.userId),
            userName = botUser.name,
            firstName = null,
            lastName = null
        )
    }

    private fun resolveChat(channelId: String): Chat {
        val channelResponse = api().channelsInfo { req ->
            req.channel(channelId)
                .includeLocale(true)
        }
        val chat = if (channelResponse.isOk) {
            val channel = channelResponse.channel

            Chat.new(
                clientName = name,
                clientId = id(channelId),
                type = if (channel.isIm)
                    ChatType.PRIVATE
                else
                    ChatType.GROUP,
                title = channel.name,
                description = channel.topic?.value
            )
        } else {
            val conversation = api().conversationsInfo { req ->
                req.channel(channelId)
                    .includeLocale(true)
            }.channel

            Chat.new(
                clientName = name,
                clientId = id(channelId),
                type = if (conversation.isIm)
                    ChatType.PRIVATE
                else
                    ChatType.GROUP,
                title = conversation.name,
                description = conversation.topic?.value
            )
        }
        return accessControl.addChatIfRequired(chat)
    }

    private fun id(slackId: String): Long {
        return clientIds[slackId] ?: run {
            val id: Long = Random(System.currentTimeMillis()).nextLong()
            clientIds = clientIds + (slackId to id)
            id
        }
    }

    private fun reverseId(id: Long): String {
        return clientIds.entries.find { entry ->
            entry.value == id
        }?.key ?: throw RuntimeException("id not found: $id")
    }

    private fun doSendMessage(
        channelId: String,
        messageBuilder: MessageBuilder
    ): List<Message> {
        require(messageBuilder.response is TextResponse) { "only text messages supported" }

        val response: TextResponse = messageBuilder.response as TextResponse
        val message: SlackMessage = api().chatPostMessage { req ->
            req.channel(channelId)
                .text(response.data)
                .mrkdwn(messageBuilder.option(MARKDOWN) ?: true)
                .unfurlLinks(messageBuilder.option<Boolean>(DISABLE_LINK_PREVIEW)?.not() ?: true)
                .blocks(messageBuilder.option(BLOCKS))
        }.message

        return listOf(inboundMessageFactory.createMessage(id(message.ts), message))
    }

    private fun toBlocks(field: Field): List<LayoutBlock>? {
        val keyboard: Keyboard? = field.findByType(Keyboard::class).firstOrNull()
        val lineLength: Int = keyboard?.lineLength ?: DEFAULT_LINE_LENGTH

        return keyboard?.buttons()?.map { button ->
            ButtonElement.builder()
                .actionId(button.id.toString())
                .text(PlainTextObject(button.title(), false))
                .build()
        }?.let { actions ->
            val actionsBlocks = actions.chunked(lineLength).map { actionsRow ->
                ActionsBlock.builder()
                    .blockId(UUID.randomUUID().toString())
                    .elements(actionsRow)
                    .build()
            }
            if (field.description != null) {
                // TODO(seykron): Issue #23: add support to render any kind of message.
                require(field.description?.response is TextResponse) {
                    "SlackClient only supports text responses for fields."
                }
                val message = field.description?.response as TextResponse

                listOf(
                    SectionBlock.builder()
                        .text(PlainTextObject(message.data, false))
                        .build()
                ) + actionsBlocks
            } else {
                actionsBlocks
            }
        }
    }

    /** Initializes the Slack's Real Time Messaging handler.
     */
    private fun initSlackHandler() {
        logger.debug("initializing slack handler")

        client = Slack().rtm(accessToken)
        client.addMessageHandler(this::processUpdate)
        client.addCloseHandler { reason ->
            if (reason.closeCode != CloseReason.CloseCodes.NORMAL_CLOSURE) {
                logger.debug("connection closed, trying to reconnect")
                reconnect = true
            } else {
                logger.debug("connection closed gracefully")
            }
        }

        client.connect()
        logger.debug("slack connected")
    }

    /** Initializes an embedded HTTP server to handle user interactions.
     */
    private fun initHttpServer() {
        logger.debug("initializing interaction HTTP server")
        HttpServer.create(InetSocketAddress(httpServerPort), 0).apply {
            createContext(httpServerEndpoint) { http ->
                val decoded = URLDecoder.decode(http.requestBody.bufferedReader().readText(), "ISO-8859-1")
                val jsonUpdate = decoded.substringAfter("payload=")

                processUpdate(jsonUpdate)

                http.responseHeaders.add("Content-Type", "application/json")
                http.sendResponseHeaders(200, 0)
                http.close()
            }
            start()
        }
    }

    private fun initReconnectMonitor() {
        logger.debug("initializing reconnect monitor")

        timer("Slack Reconnect Monitor", daemon = true, period = RECONNECT_PERIOD) {
            if (reconnect) {
                try {
                    client.reconnect()
                    reconnect = false
                } catch (cause: Exception) {
                    logger.error("cannot reconnect, trying again in $RECONNECT_PERIOD ms", cause)
                }
            }
        }
    }
}
