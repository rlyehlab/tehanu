package be.rlab.tehanu.clients.slack

import be.rlab.tehanu.config.ClientConfigBuilder
import be.rlab.tehanu.media.MediaManager

const val SLACK_CLIENT_NAME: String = "SlackClient"
const val SLACK_DEFAULT_SERVER_PORT: Int = 8945
const val SLACK_DEFAULT_SERVER_ENDPOINT: String = "/slack"

fun ClientConfigBuilder.slack(
    accessToken: String,
    httpServerPort: Int = SLACK_DEFAULT_SERVER_PORT,
    httpServerEndpoint: String = SLACK_DEFAULT_SERVER_ENDPOINT
) {
    addClient {
        serviceProvider.registerService(MediaManager::class) { FileManager() }
        SlackClient(
            name = SLACK_CLIENT_NAME,
            serviceProvider = serviceProvider,
            accessToken = accessToken,
            httpServerPort = httpServerPort,
            httpServerEndpoint = httpServerEndpoint,
            inboundMessageFactory = InboundMessageFactory()
        )
    }
}
