package be.rlab.tehanu.clients.slack

import be.rlab.tehanu.clients.slack.model.EventNames
import be.rlab.tehanu.messages.model.*
import com.slack.api.model.block.RichTextBlock
import com.slack.api.model.block.element.RichTextSectionElement
import org.joda.time.DateTime
import com.slack.api.model.Message as SlackMessage

class InboundMessageFactory {

    fun createMessage(
        messageId: Long,
        message: SlackMessage
    ): Message {
        val entities = message.blocks?.filter { block ->
            block.type == "rich_text"
        }?.flatMap { block ->
            require(block is RichTextBlock)

            block.elements.filter { element ->
                element is RichTextSectionElement
            }.flatMap { element ->
                require(element is RichTextSectionElement)
                element.elements
            }
        }?.map { block ->
            when (block) {
                is RichTextSectionElement.Text ->
                    MessageEntity(EntityType.RICH_TEXT, block.text, block)
                is RichTextSectionElement.Channel ->
                    MessageEntity(EntityType.MENTION, block.channelId, block)
                is RichTextSectionElement.User ->
                    MessageEntity(EntityType.MENTION, block.userId, block)
                is RichTextSectionElement.Emoji ->
                    MessageEntity(EntityType.RICH_TEXT, block.name, block)
                is RichTextSectionElement.Link ->
                    MessageEntity(EntityType.URL, block.url, block)
                is RichTextSectionElement.Team ->
                    MessageEntity(EntityType.MENTION, block.teamId, block)
                is RichTextSectionElement.UserGroup ->
                    MessageEntity(EntityType.MENTION, block.usergroupId, block)
                is RichTextSectionElement.Broadcast ->
                    MessageEntity(EntityType.MENTION, block.range, block)
                is RichTextSectionElement.Date ->
                    MessageEntity(EntityType.RICH_TEXT, block.timestamp, block)
                else ->
                    MessageEntity(EntityType.UNKNOWN, block.type, block)
            }
        }

        return Message.new(
            messageId = messageId,
            date = DateTime.now(),
            isEdit = false,
            isForward = false,
            text = message.text,
            entities = entities ?: emptyList()
        )
    }

    fun createMessage(
        messageId: Long,
        eventType: String,
        data: Any
    ): Message {
        val chatEvent: ChatEvent? = when (eventType) {
            "member_joined_channel" -> ChatEvent(EventNames.memberJoined, data)
            "member_left_channel" -> ChatEvent(EventNames.memberLeft, data)
            "channel_rename" -> ChatEvent(EventNames.channelRenamed, data)
            "channel_deleted" -> ChatEvent(EventNames.channelDeleted, data)
            "channel_created" -> ChatEvent(EventNames.channelCreated, data)
            "channel_topic" -> ChatEvent(EventNames.topicChanged, data)
            else -> null
        }

        return Message.new(
            messageId = messageId,
            date = DateTime.now(),
            isEdit = false,
            isForward = false,
            event = chatEvent
        )
    }
}