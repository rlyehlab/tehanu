package be.rlab.tehanu.clients.slack

import be.rlab.tehanu.messages.MessageBuilder
import com.slack.api.model.block.LayoutBlock

/** Mode for parsing entities in the caption. */
const val MARKDOWN: String = "parseMode"
/** List of layout blocks in the message. */
const val BLOCKS: String = "blocks"
/** Don't render the link preview for urls. */
const val DISABLE_LINK_PREVIEW: String = "disableLinkPreview"

/** Indicates whether to parse message as markdown.
 * @param parseMarkdown true to parse the message as markdown.
 */
fun MessageBuilder.markdown(parseMarkdown: Boolean): MessageBuilder =
    options(MARKDOWN to parseMarkdown)

/** Adds [LayoutBlock]s to the message.
 * @param blocks List of layout blocks in the message.
 */
fun MessageBuilder.blocks(blocks: List<LayoutBlock>): MessageBuilder =
    options(BLOCKS to blocks)

/** Disables link previews for links in this message.
 */
fun MessageBuilder.disableLinkPreview(): MessageBuilder =
    options(DISABLE_LINK_PREVIEW to true)
