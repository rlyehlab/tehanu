package be.rlab.tehanu.clients.slack

import be.rlab.tehanu.media.MediaManager
import be.rlab.tehanu.media.model.MediaFile
import java.io.InputStream

class FileManager : MediaManager() {
    override fun findById(fileId: String): MediaFile? {
        TODO("Not yet implemented")
    }

    override fun readContent(mediaFile: MediaFile): InputStream {
        TODO("Not yet implemented")
    }

    override fun store(
        mediaFile: MediaFile,
        content: InputStream
    ): MediaFile {
        TODO("Not yet implemented")
    }

    override fun update(mediaFile: MediaFile): MediaFile {
        TODO("Not yet implemented")
    }
}