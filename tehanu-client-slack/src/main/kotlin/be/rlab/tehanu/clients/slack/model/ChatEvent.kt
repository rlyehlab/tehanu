package be.rlab.tehanu.clients.slack.model

import be.rlab.tehanu.messages.model.ChatEvent

object EventNames {
    const val memberJoined = "memberJoined"
    const val memberLeft = "memberLeft"
    const val topicChanged = "topicChanged"
    const val channelRenamed = "channelRenamed"
    const val channelCreated = "channelCreated"
    const val channelDeleted = "channelDeleted"
}

fun ChatEvent.memberJoined(): Boolean = name == EventNames.memberJoined
fun ChatEvent.memberLeft(): Boolean = name == EventNames.memberLeft
fun ChatEvent.topicChanged(): Boolean = name == EventNames.topicChanged
fun ChatEvent.channelRenamed(): Boolean = name == EventNames.channelRenamed
fun ChatEvent.channelCreated(): Boolean = name == EventNames.channelCreated
fun ChatEvent.channelDeleted(): Boolean = name == EventNames.channelDeleted
