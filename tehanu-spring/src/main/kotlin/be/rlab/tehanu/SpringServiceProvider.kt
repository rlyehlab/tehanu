package be.rlab.tehanu

import org.springframework.context.ApplicationContext
import kotlin.reflect.KClass

class SpringServiceProvider(
    private val applicationContext: ApplicationContext
) : BotServiceProvider() {
    override fun <T : Any> getService(type: KClass<T>): T {
        return applicationContext.getBean(type.java)
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> getServices(type: KClass<T>): List<T> {
        return applicationContext.getBeansOfType(type::class.java).values.toList() as List<T>
    }
}
