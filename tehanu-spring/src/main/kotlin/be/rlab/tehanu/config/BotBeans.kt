package be.rlab.tehanu.config

import be.rlab.tehanu.BotAware
import be.rlab.tehanu.SpringServiceProvider
import be.rlab.tehanu.Tehanu
import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.acl.model.Role
import be.rlab.tehanu.clients.Client
import be.rlab.tehanu.clients.StateManager
import be.rlab.tehanu.clients.UpdateDispatcher
import be.rlab.tehanu.command.Cancel
import be.rlab.tehanu.command.UserManager
import be.rlab.tehanu.i18n.Language
import be.rlab.tehanu.i18n.MessageSourceFactory
import be.rlab.tehanu.support.ConfigUtils.getOrDefault
import com.typesafe.config.Config
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.support.beans

object BotBeans {
    private val logger: Logger = LoggerFactory.getLogger(BotBeans::class.java)

    fun beans(mainConfig: Config) = beans {
        val botConfig: Config = mainConfig.getConfig("bot")

        // Built-in commands
        bean {
            UserManager(accessControl = ref())
        }
        bean {
            Cancel
        }

        // Bot
        bean<AccessControl>()
        bean<StateManager>()
        bean<UpdateDispatcher>()
        bean<SpringServiceProvider>()

        bean(isPrimary = false) {
            UpdateDispatcherConfigurer()
        }
        bean {
            MessageSourceFactory(
                if (mainConfig.hasPath("bot.default-language")) {
                    Language.valueOf(mainConfig.getString("bot.default-language").uppercase())
                } else {
                    null
                }
            )
        }

        bean {
            Tehanu(
                botListeners = provider<BotAware>().toList(),
                clients = provider<Client>().toList(),
                serviceProvider = ref()
            )
        }

        bean {
            logger.debug("Loading bot configuration")

            val roles: List<Role> = botConfig.getOrDefault(emptyList()) {
                getConfigList("roles")
            }.map { roleConfig ->
                Role(
                    name = roleConfig.getString("name"),
                    title = roleConfig.getString("title"),
                    permissions = roleConfig.getStringList("permissions")
                )
            }

            SecurityConfig(
                admins = botConfig.getOrDefault(emptyList()) { getStringList("admins") },
                roles = roles
            )
        }
    }
}
