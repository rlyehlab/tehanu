package be.rlab.tehanu.config

import be.rlab.tehanu.clients.UpdateDispatcher
import be.rlab.tehanu.acl.AccessControl
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateHandler
import be.rlab.tehanu.i18n.MessageSourceFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.getBean
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import be.rlab.tehanu.annotations.HandlerGroup
import be.rlab.tehanu.support.persistence.TransactionSupport
import org.springframework.beans.factory.getBeansOfType
import kotlin.reflect.KFunction
import kotlin.reflect.full.hasAnnotation

/** Search for all beans annotated with [HandlerGroup] and resolves the underlying [UpdateHandler]s.
 * It adds the message handlers to the [UpdateDispatcher] defined in the context.
 */
open class UpdateDispatcherConfigurer : ApplicationContextAware {

    private val logger: Logger = LoggerFactory.getLogger(UpdateDispatcherConfigurer::class.java)

    override fun setApplicationContext(applicationContext: ApplicationContext) {
        val beans = applicationContext.getBeansWithAnnotation(HandlerGroup::class.java) +
            applicationContext.getBeansOfType<KFunction<*>>().filter { (_, bean) ->
                bean.hasAnnotation<Handler>()
            }
        val configurersCount: Int = applicationContext
            .getBeansOfType(UpdateDispatcherConfigurer::class.java).size

        if (configurersCount == 1 || this::class != UpdateDispatcherConfigurer::class) {
            val updateDispatcher: UpdateDispatcher = applicationContext.getBean()
            val accessControl: AccessControl = applicationContext.getBean()
            val securityConfig: SecurityConfig = applicationContext.getBean()
            val messageSourceFactory = applicationContext.getBean<MessageSourceFactory>().apply {
                initialize()
            }
            applicationContext.getBeansOfType<TransactionSupport>().onEach { (name, bean) ->
                logger.debug("initializing db for $name")
                bean.config = applicationContext.getBean()
                bean.db = applicationContext.getBean()
            }
            val handlers: List<UpdateHandler> = beans.flatMap { (name, target) ->
                logger.debug("registering handlers for $name")
                HandlerAnnotationConfiguration(
                    target,
                    messageSourceFactory,
                    accessControl,
                    securityConfig
                ).getHandlers()
            }
            updateDispatcher.addHandlers(handlers)
        } else {
            logger.debug("UpdateDispatcherConfigurer provided by other bean, skipping initialization.")
        }
    }
}
