package be.rlab.tehanu.config

import be.rlab.tehanu.clients.slack.FileManager
import be.rlab.tehanu.clients.slack.InboundMessageFactory
import be.rlab.tehanu.clients.slack.SlackClient
import com.typesafe.config.Config
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.support.beans

object SlackBeans {
    private val logger: Logger = LoggerFactory.getLogger(SlackBeans::class.java)

    private const val CLIENT_NAME: String = "SlackClient"
    private const val DEFAULT_SERVER_PORT: Int = 8945
    private const val DEFAULT_SERVER_ENDPOINT: String = "/slack"
    private const val CONF_ACCESS_TOKEN: String = "access_token"
    private const val CONF_SERVER_PORT: String = "http_server_port"
    private const val CONF_SERVER_ENDPOINT: String = "http_server_endpoint"

    fun beans(mainConfig: Config) = beans {
        logger.debug("Reading Slack configuration")

        val config: Config = mainConfig.getConfig("bot.clients.slack")

        bean<InboundMessageFactory>()
        bean<FileManager>()

        // Slack client.
        bean {
            SlackClient(
                name = CLIENT_NAME,
                serviceProvider = ref(),
                accessToken = config.getString(CONF_ACCESS_TOKEN),
                httpServerPort =
                    if (config.hasPath(CONF_SERVER_PORT))
                        config.getInt(CONF_SERVER_PORT)
                    else
                        DEFAULT_SERVER_PORT,
                httpServerEndpoint =
                    if (config.hasPath(CONF_SERVER_ENDPOINT))
                        config.getString(CONF_SERVER_ENDPOINT)
                    else
                        DEFAULT_SERVER_ENDPOINT,
                inboundMessageFactory = ref()
            )
        }
    }
}
