package be.rlab.tehanu.config

import com.typesafe.config.Config

object ConfigSupport {
    fun stringOrNull(
        config: Config,
        key: String
    ): String? {
        return if (config.hasPath(key)) {
            config.getString(key)
        } else {
            null
        }
    }

    fun stringOrDefault(
        config: Config,
        key: String,
        defaultValue: String
    ): String {
        return if (config.hasPath(key)) {
            config.getString(key)
        } else {
            defaultValue
        }
    }

    fun stringListOrDefault(
        config: Config,
        key: String
    ): List<String> {
        return if (config.hasPath(key)) {
            config.getStringList(key)
        } else {
            emptyList()
        }
    }

    fun booleanOrDefault(
        config: Config,
        key: String,
        defaultValue: Boolean = true
    ): Boolean {
        return if (config.hasPath(key)) {
            config.getBoolean(key)
        } else {
            defaultValue
        }
    }

    fun floatOrDefault(
        config: Config,
        key: String,
        defaultValue: Float = 0.0F
    ): Float {
        return if (config.hasPath(key)) {
            config.getDouble(key).toFloat()
        } else {
            defaultValue
        }
    }
}