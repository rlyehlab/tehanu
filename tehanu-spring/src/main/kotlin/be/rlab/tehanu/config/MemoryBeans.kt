package be.rlab.tehanu.config

import be.rlab.tehanu.store.DatabaseMemory
import be.rlab.tehanu.store.MemorySlots
import be.rlab.tehanu.store.MemorySlotsDAO
import org.springframework.context.support.beans

object MemoryBeans {
    fun beans() = beans {
        // Memory
        bean<DatabaseMemory>()
        bean<MemorySlotsDAO>()
        bean { MemorySlots }
    }
}
