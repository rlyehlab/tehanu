package be.rlab.tehanu.config

import be.rlab.tehanu.store.DataSourceConfig
import be.rlab.tehanu.support.persistence.DataSourceInitializer
import be.rlab.tehanu.support.ConfigUtils.getOrDefault
import com.typesafe.config.Config
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.springframework.context.support.beans
import javax.sql.DataSource

object DataSourceBeans {
    fun beans(mainConfig: Config) = beans {
        // Database
        bean {
            DataSourceInitializer(
                tables = provider<Table>().toList()
            ).apply {
                config = ref()
            }
        }

        bean {
            val dbConfig: Config = mainConfig.getConfig("db")

            DataSourceConfig(
                url = dbConfig.getString("url"),
                user = dbConfig.getString("user"),
                password = dbConfig.getString("password"),
                driver = dbConfig.getString("driver"),
                logStatements = dbConfig.getOrDefault(false) { getBoolean("log-statements") },
                drop = dbConfig.getOrDefault("no") { getString("drop") }
            )
        }

        bean {
            Database.connect(
                datasource = ref<DataSource>()
            )
        }

        bean {
            val dataSourceConfig: DataSourceConfig = ref()

            HikariDataSource().apply {
                jdbcUrl = dataSourceConfig.url
                username = dataSourceConfig.user
                password = dataSourceConfig.password
                driverClassName = dataSourceConfig.driver
            }
        }
    }
}
