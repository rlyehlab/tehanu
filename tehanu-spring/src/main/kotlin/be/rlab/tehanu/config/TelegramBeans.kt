package be.rlab.tehanu.config

import be.rlab.tehanu.clients.telegram.FileManager
import be.rlab.tehanu.clients.telegram.InboundMessageFactory
import be.rlab.tehanu.clients.telegram.TelegramBotFactory
import be.rlab.tehanu.clients.telegram.TelegramClient
import com.typesafe.config.Config
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.support.beans

object TelegramBeans {
    private val logger: Logger = LoggerFactory.getLogger(TelegramBeans::class.java)
    private const val CLIENT_NAME: String = "TelegramClient"

    fun beans(mainConfig: Config) = beans {
        logger.debug("Reading Telegram configuration")
        val config: Config = mainConfig.getConfig("bot.clients.telegram")

        bean<InboundMessageFactory>()
        bean { FileManager(config.getString("access_token")) }
        bean { TelegramBotFactory(config.getString("access_token")) }

        // Telegram client.
        bean {
            TelegramClient(
                name = CLIENT_NAME,
                serviceProvider = ref(),
                inboundMessageFactory = ref(),
                botFactory = ref()
            )
        }
    }
}
