package be.rlab.tehanu

import be.rlab.tehanu.config.BotBeans
import be.rlab.tehanu.config.DataSourceBeans
import be.rlab.tehanu.config.MemoryBeans
import be.rlab.tehanu.store.MemorySlots
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import org.springframework.beans.factory.getBean
import org.springframework.context.annotation.AnnotationConfigApplicationContext

/** Can be extended to start a standalone application using Spring.
 *
 * It creates a [AnnotationConfigApplicationContext] that can be initialized in the
 * [initialize] phase. After that, the application context is refreshed and once
 * all components are available it invokes the [ready] phase.
 *
 * The application configuration is managed by Type Safe. The [resolveConfig] method
 * can be used to provide a custom [Config]. By default, it looks up the application.conf
 * file at the root of the classpath.
 */
abstract class SpringApplication : BotApplication {

    private val logger: Logger = LoggerFactory.getLogger(SpringApplication::class.java)
    lateinit var applicationContext: AnnotationConfigApplicationContext
        private set

    init {
        // Sets up jul-to-slf4j bridge. I have not idea
        // why the logging.properties strategy doesn't work.
        SLF4JBridgeHandler.removeHandlersForRootLogger()
        SLF4JBridgeHandler.install()
    }

    /** Resolves the TypeSafe main configuration.
     * By default it searches for the default application.conf
     * @return the application main config.
     */
    open fun resolveConfig(): Config {
        return ConfigFactory.defaultApplication().resolve()
    }

    override fun initialize() {
        logger.debug("initializing application context")
    }

    override fun ready() {
        logger.debug("application context is ready")
    }

    override fun start() {
        logger.debug("Bot is waking up")

        applicationContext = AnnotationConfigApplicationContext().apply {
            val mainConfig: Config = resolveConfig()
            DataSourceBeans.beans(mainConfig).initialize(this)
            MemoryBeans.beans().initialize(this)
            BotBeans.beans(mainConfig).initialize(this)
        }
        initialize()
        applicationContext.refresh()

        transaction {
            SchemaUtils.createMissingTablesAndColumns(MemorySlots)
        }

        ready()

        val tehanu: Tehanu = applicationContext.getBean()
        logger.info("Bot is entering Telegram")
        tehanu.start()
    }
}
