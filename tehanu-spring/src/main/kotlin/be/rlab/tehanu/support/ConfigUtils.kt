package be.rlab.tehanu.support

import com.typesafe.config.Config
import com.typesafe.config.ConfigException

object ConfigUtils {
    fun<T> Config.getOrDefault(
        defaultValue: T,
        resolve: Config.() -> T
    ): T {
        return try {
            resolve(this)
        } catch (cause: ConfigException) {
            defaultValue
        }
    }
}