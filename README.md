# Tehanu

Tehanu is a messaging client that provides a single interface for different messaging services like Telegram or Slack.

![Tehanu](tehanu.png)

Tehanu allows you to easily build bots for multiple messaging clients. She has the following features:

* Annotation-based configuration
* Manages different types of messages and content
* Supports multiple messaging clients
* Allows to build complex flows using structured user input
* Provides persistent storage
* Supports role-based access control out of the box
* Internationalization

Tehanu is completely transparent, and she provides all the features for the following messaging clients:

* Telegram
* Slack

## Dependencies

Tehanu is available in maven central. You need to include the core dependency and the messaging client
dependency you want to connect to.

```xml
<dependency>
    <groupId>be.rlab</groupId>
    <artifactId>tehanu-core</artifactId>
    <version>4.0.0</version>
</dependency>
<dependency>
    <groupId>be.rlab</groupId>
    <artifactId>tehanu-client-telegram</artifactId>
    <version>4.0.0</version>
</dependency>
```

### Quick start

The following example configures Tehanu for Telegram, then it defines and registers a very simple Handler. Look at
the [wiki for the complete documentation](https://gitlab.com/rlyehlab/tehanu/-/wikis/home).

```kotlin
import be.rlab.tehanu.Tehanu
import be.rlab.tehanu.annotations.Handler
import be.rlab.tehanu.clients.UpdateContext

@Handler(name = "/say_hello")
fun sayHello(context: UpdateContext) = context.apply {
    answer("hello!")
}

fun main(args: Array<String>) {
    Tehanu.configure {
        handlers {
            register(::sayHello)
        }
        clients {
            telegram("YOUR_BOT_TOKEN")
        }
    }.start()
}

```

### Releases policy

Breaking changes will change the major version. New releases for the same mainline will
change the minor version. Bug fixes will change the revision.

We'll be careful to avoid breaking changes, but if it happens, we'll still deliver bug fixes
to the previous version. New features will not be available in previous versions. Every time
a new major version is released, we'll drop support for older versions and the previous version
will be deprecated and kept in maintenance mode.
